\Subsection{Оценки Чернова}

\begin{definition}
	\slashns

	Пусть $x_1, \ldots, x_n$~--- независимые случайные величины, такие, что $x_i = 1$ с вероятностью $p_i$ и $0$ с вероятностью $1 - p_i$. Схема, в которой нас интересует величина $x = \sum x_i$ называется схемой Пуассона.
\end{definition}

\begin{remark}
	\slashns

	В некотором смысле, это обобщение схемы Бернулли на случай, когда монетки различны, т.е. если все $p_i$ равны некоторому $p$, то получаем обычную схему Бернулли c $n$ подбрасываниями и вероятностью выпадения орла равной $p$. 
\end{remark}

\begin{theorem}
	\slashns
	
	Пусть дана некоторая схема Пуассона и $\delta > 0$.	

	Тогда $Pr[x > (1 + \delta)\mu] < \left(\frac{e^\delta}{(1 + \delta)^{1 + \delta}}\right)^\mu$, где $\mu = E(x)$~--- математическое ожидание результата нашей схемы Пуассона.
\end{theorem}	

\begin{proof}
	\slashns

	Заметим, что $Pr[x > (1 + \delta)\mu] = Pr[e^{xt} > e^{t(1 + \delta)\mu}]$ для любого $t > 0$.
	
	Теперь мы можем оценить вероятность по неравенству Маркова: $Pr[e^{xt} > e^{t(1 + \delta)\mu}] \le \frac{E(e^{xt})}{e^{t(1 + \delta)\mu}}$

	Преобразуем числитель:

	$E(e^{xt}) = E(e^{x_1t + \ldots + x_nt}) = E(\prod e^{x_it}) = \prod(Ee^{x_it}) = \prod(p_ie^t + (1 - p_i)) = \prod(1 + p_i(e^t - 1))$
	
	Оцениваем каждую скобку неравенством $1 + x \le e^x$ и получим:
	
	$\prod(1 + p_i(e^t - 1))\le \prod e^{p_i(e^t - 1)} = e^{(e^t - 1)\sum p_i} = e^{\mu(e^t - 1)}$
		
	Мы получили неравенство $Pr[x > (1 + \delta)\mu] \le \frac{e^{\mu(e^t - 1)}}{e^{t(1 + \delta)\mu}}$ для каждого положительного $t$. 
	
	Подставим в него $t = \ln (1 + \delta)$ и получим требуемое.

\end{proof}

\begin{remark}
	\slashns

	Функция $E(e^{tx})$, которую мы использовали в доказательстве называется производящей функцией моментов (является экспоненциальной производящей функцией). 
	
	Действительно, $E(e^{tx}) = E(\frac{(tx)^0}{0!} + \frac{(tx)^1}{1!} + \ldots) = 1 + tE(x) + \frac{t^2}{2}E(x^2) + \ldots$, что и требовалось.  
\end{remark}

\begin{denotation}
	\slashns

	Обозначим $\left(\frac{e^\delta}{(1 + \delta)^{1 + \delta}}\right)^\mu$ как $F^{+}(\mu, \delta)$.
\end{denotation}

\begin{example}
	\slashns
	
	Пусть вероятность выпадения орла на монетке равна $\frac{1}{3}$. Тогда вероятность выпадения хотя-бы половины орлов будет экспоненциально мала. 
	
	Обозначим количество выпавших орлов за $n$ подбрасываний как $y_n$, тогда $Ey_n = \frac{1}{3}$. Воспользуемся оценкой Чернова:

	$Pr[y_n > \frac{n}{2}] = Pr[y_n > (1 + \frac{1}{2})Ey_n] \le F^+(\frac{n}{3}, \frac{1}{2}) \approx (0.965)^n$
\end{example}	

\begin{theorem}
	\slashns
	
	Пусть дана некоторая схема Пуассона и $\delta > 0$.	
	
	Тогда $Pr[x < (1 - \delta)\mu] < e^{\frac{-\mu \delta^2}{2}}$, где $\mu = E(x)$~--- математическое ожидание результата нашей схемы Пуассона.	
\end{theorem}	

\begin{proof}
	\slashns

	Аналогично, $Pr[x < (1 - \delta)\mu] = Pr[e^{tx} < e^{t(1 - \delta)\mu}] = Pr[e^{-tx} > e^{-t(1 - \delta)\mu}]$

	По неравенству Маркова:
	
	$Pr[e^{-xt} > e^{-t(1 - \delta)\mu}] < \frac{E(e^{-tx})}{e^{-t(1 - \delta)\mu}}$
	
	Оцениваем числитель:	
	
	$E(\prod e^{-tx}) = \prod (E e^{-tx}) = \prod(p_ie^{-t} + (1 - p_i)) = \prod(1 + p_i(e^{-t} - 1))$
	
	Так же оцениваем каждую скобку по неравенству $1 + x \le e^x$ и получаем: 
	
	$\prod(1 + p_i(e^{-t} - 1)) \le \prod e^{p_i(e^{-t} - 1)} = e^{\sum p_i(e^{-t} - 1)} = e^{(e^{-t} - 1)\mu}$

	Подставляем в полученное неравенство $t = \ln \frac{1}{1 - \delta}$ и получаем
	
	$Pr[x < (1 - \delta)\mu] \le \left(\frac{e^{-\delta}}{(1 - \delta)^{1 - \delta}}\right)^\mu \le e^{\frac{-\delta^2}{2}\mu}$

	Последнее неравенство мы не доказываем.
\end{proof}	

\begin{denotation}
	\slashns
	
	Обозначим $e^{\frac{-\delta^2}{2}\mu}$ как $F^-(\mu, \delta)$.
\end{denotation}	

\begin{example}
	\slashns
	
	Пусть вероятность выпадения орла равна $\frac{3}{4}$. Тогда вероятность того, что орел выпадет в менее чем половине случаев экспоненциально мала.
	
	Аналогично, обозначаем количество орлов как $y_n$, $Ey_n = \frac{3}{4}n$.
	
	$Pr[y_n < \frac{n}{2}] = Pr[y_n < (1 - \frac{1}{3})Ey_n] \le F^-(\frac{3}{4}n, \frac{1}{3}) \approx (0,9592)^n$.
\end{example}	

\begin{theorem}
	\slashns
	
	Для $\delta \in [0, u]$, $F^+(\mu, \delta) \le e^{-C(u)\mu\delta^2}$, где $C(u) = \frac{(1 + u)\ln (1 + u) - u}{u^2}$.
\end{theorem}	

\begin{proof}
	\slashns
	
	Без доказательства.
\end{proof}	

\begin{remark}
	\slashns

	Это просто некоторый способ оценки функции $F^+(\mu, \delta)$. Мы никогда им не пользовались. 
\end{remark}

\begin{definition}
	\slashns
	
	Пусть дана система линейных неравенств и линейная функция. Задачей линейного программирования является нахождения значений переменных, такие, что линейная функция принимает минимальное (или максимальное) возможное значение. 

	Задачей целочисленного линейного программирования является аналогичная задача над кольцом целых чисел.
\end{definition}

\begin{remark}
	\slashns

	Задача обычного линейного программирования решается за полиномиальное время. В то же время задача целочисленного линейного программирования является $\mathtt{NP}$-трудной.
\end{remark}

\begin{remark}
	\slashns
	
	Если требуется добавить в систему равенство, то это можно сделать посредством добавления двух разносторонних неравенств.
\end{remark}	

\begin{algorithm}
	\slashns
	
	Некоторый общий метод решения:
	
	\begin{enumerate}
		\item Свести задачу к задаче целочисленного линейного программирования
		
		\item Решить ее как задачу простого линейного программирования.
		
		\item Округлить ответ специальным образом.
	\end{enumerate}		
\end{algorithm}

\begin{task}
	\slashns
	
	Дана доска $\sqrt{n}\times \sqrt{n}$. Несколько пар клеток отмечены одинаковыми цветами. Требуется соединить клетки каждой пары ломаной, проходящей по клеткам таблицы, и имеющей не более одного угла. 
	
	Пусть $c(e)$~--- количество пересечений единичного ребра сетки $e$ проведенными ломаными.

	Требуется провести ломаные таким образом, чтобы $\max c(e)$ по всем $e$ было минимально.
\end{task}	

\begin{algorithm}
	\slashns

	Попробуем решить нашу задачу описанным выше образом. Сделаем три описанных шага:

	\textbf{Шаг 1: Свести задачу к $\mathtt{LIP}$}

	Для каждой пары концов у нас есть два варианта как провести ломаную. Заведем две переменные $x_i$ и $y_i$, так, что если ломаная проведена по верхней и правой стороне квадрата, то $x_i = 0$, $y_i = 1$, а если по левой и нижней, то $x_i = 1$, $y_i = 0$. Таким образом мы имеем равенство $x_i + y_i = 1$. Так же нужно ограничить значения $x_i$ и $y_i$ числами из множества $\{0, 1\}$. Для этого добавляем неравенства $0 \le x_i, y_i \le 1$.

	Далее, для каждого ребра (к примеру для $c$) запишем количество пересекающих его ребер. Это число равно $\sum x_i + \sum y_i$, где суммирование ведется по пересекающим $c$ ломаным. Таким образом мы получили систему неравенств $\sum x_i + \sum y_i \le \omega$. При этом, мы хотим минимизировать значение $\omega$, т.е. мы получили задачу $\mathtt{LIP}$. 
	
	\textbf{Шаг 2: Решить ее как задачу обычного линейного программирования}.

	Тут ничего сложного, просто решаем. Получаем решение вида $\tilde{x_i}$, $\tilde{y_i}$, $\tilde{\omega}$. Где все $x_i, y_i \in [0, 1]$.
	
	\textbf{Шаг 3: Округлить ответ специальным образом}

	Для каждого $x_i$ выберем значение равное $1$ с вероятностью $\tilde{x_i}$ и $0$ с оставшейся вероятностью. Значение для $y_i$ будет получено автоматически. Полученные значения обозначим как $\hat{x_i}$ и $\hat{y_i}$. Соответственно новое значение для $\omega$ обозначим как $\hat{\omega}$. Осталось оценить ошибку.

	\textbf{Оцениваем вероятность ошибки}.
	
	Оценим какую точность можно гарантировать с ошибкой не больше $\varepsilon$.
		
	Значение каждой из $x_i, y_i$ равно $0$ или $1$ с вероятностями $\tilde{x_i}$ и $\tilde{y_i}$ соответственно, при этом одна из переменных равна $0$, а другая $1$, т.е. мы получили схему Пуассона для каждого из неравенств $\sum \hat{x_i} + \sum \hat{y_i}$. Обозначим сумму как $s_i$.

	Мы умеем оценивать вероятность того, что такая сумма больше $(1 + \delta)\mu$, где $\mu$~--- математическое ожидание. Посчитаем $\mu$.
	
	$\mu = Es_i = E(\sum \hat{x_i} + \sum \hat{y_i}) = \sum E\hat{x_i} + \sum E\hat{y_i} = \sum \tilde{x_i} + \sum \tilde{y_i} \le \tilde{\omega}$. 
	
	Оцениваем $Pr[s_i > (1 + \delta)\tilde{\omega}] < Pr[s_i > (1 + \delta)\mu] < F^+(\mu, \delta)$.

	Выбираем такое $\delta$, что $F^+(\mu, \delta) = \frac{\varepsilon}{2n}$.

	Тогда мы получили, что вероятность того, что мы ошиблись в неравенстве с номером $i$ больше чем в $1 + \delta$ раз не более $\frac{\varepsilon}{2n}$. Всего неравенств $2n$, а значит вероятность, ошибки хотя бы в одном не больше чем $2n \cdot \frac{\varepsilon}{2n} = \varepsilon$.
	
	Т.е. с вероятностью ошибки $\varepsilon$ мы можем гарантировать ошибку в $1 + \delta$ раз, где $F^+(\mu, \delta) = \frac{\varepsilon}{2n}$.
\end{algorithm}

\begin{remark}
	\slashns
	
	Почему результат хороший (т.е. $\delta$ достаточно маленькое), выяснять не будем.
\end{remark}	