\Section{Введение}

\Subsection{Сетевая модель ISO/OSI}

Чтобы разные компьютерные системы, сделанные в разных странах, с разными протоколами, могли хорошо взаимодействовать, инженеры договорились, каким образом будут представляться уровни взаимодействия систем. На каждом уровне действуют свои протоколы, используются свои элементы информации, способы кодирования, шифрования, и каждый уровень выполняет свои функции. Чем ниже уровень, тем процессы, происходящие на нем, ближе к физической среде, чем выше -- тем ближе к прикладным программам и к пользователю. В результате получилась семиуровневая модель, которую обычно представляют в виде таблицы.

\begin{center}
  \begin{tabular}{ | c | c | c | c | }
    \hline
    \textbf{№} & \textbf{Название} & \textbf{Тип данных} & \textbf{Функции} \\
    \hline
    7 & \makecell{Прикладной\\Application} & Данные & Доступ к сетевым службам \\
    \hline
    6 & \makecell{Представления\\Presentation} & Данные & Представление и шифрование данных \\
    \hline
    5 & \makecell{Сеансовый\\Session} & Данные & Управление сеансом связи \\
    \hline
    4 & \makecell{Транспортный\\Transport} & Сегменты или датаграммы & \makecell{Прямая связь между конечными\\пунктами и надёжность} \\
    \hline
    3 & \makecell{Сетевой\\Network} & Пакеты & \makecell{Определение маршрута и логическая\\адресация} \\
    \hline
    2 & \makecell{Канальный\\Link} & Биты или кадры & Физическая адресация \\
    \hline
    1 & \makecell{Физический\\Physical} & Биты & \makecell{Работа со средой передачи, сигналами и\\двоичными данными} \\
    \hline
  \end{tabular}
\end{center}

\begin{itemize}
	\item Физический уровень отвечает за физическое кодирование битов. Этот уровень всегда реализуется аппаратно.
	\item На канальном уровне биты объединяются в массивы данных (кадры/фреймы). Этот уровень описывает, каким образом устройства, подключенные к одному сегменту сети, обмениваются между собой данными. В том числе здесь решается задача адресации в пределах одного сетевого сегмента с использованием MAC-адреса. Чаще всего канальный уровень реализуется аппаратно, но иногда может и программно.
	\item Сетевой уровень отвечает за передачу данных между сетями. Обеспечивает адресацию в глобальной сети с использованием логического адреса и передачу данных между двумя сетями, возможно через какие-то другие сети. При этом надежность передачи данных не обеспечивается. Начиная с этого уровня обычно всё реализовано программно, но здесь бывают и аппаратные реализации.
	\item Транспортный уровень отвечает за передачу данных между приложениями с заданным уровнем надежности. Здесь происходит адресация приложений, то есть доставляем информацию не просто на устройство, а передаем конкретной программе. В TCP/IP на транспортном уровне есть два протокола. TCP -- более надежный и UDP -- более быстрый.
	\item Сеансовый уровень отвечает за организацию сеанса связи между двумя узлами сети. Здесь мы создаем, поддерживаем и разрываем соединение.
	\item Уровень представления отвечает за согласование форматов информации на концах соединения. Сюда входит сжатие, шифрование информации, разные кодировки. Протоколы этого уровня нигде не реализованы, и задачи согласования решаются на прикладном уровне.
	\item Прикладной уровень отвечает за всякие прикладные функции, такие как передача файла, передача электронной почты, передача веб-страниц, итп.
\end{itemize}

Протоколы на разных уровнях ISO/OSI вкладываются друг в друга через механизм инкапсуляции, который называется стек протоколов.

Механизм следующий: в поле 'данные' протокола вкладываются данные протокола следующего уровня. В итоге данные устроены так:

\begin{tabular}{|c|c|c|c|c|}
	\hline
	заголовок 2 уровня & заголовок 3 уровня & ... & данные & концовка (есть только в ethernet) \\
	\hline
\end{tabular}

На практике разные стеки протоколов по-разному реализуют эту модель. Какие-то уровни пропускаются и объединяются с другими. В стеке TCP/IP четыре уровня -- прикладной, транспортный, сетевой и канальный.

К архитектуре компьютерных сетей относятся только верхние пять уровней, и мы будем рассматривать только их.

\Subsection{Архитектуры компьютерных сетей}

Существует много разных сетевых архитектур. Вот небольшой список:

\begin{itemize}
	\item DNA (Digital Network Architecture) -- архитектура, разработанная компанией DEC, которая раньше была одой из самых влиятельных на рынке сетей и цифровых устройств.
	\item SNA (Systems Network Architecture) -- архитектура, разработанная компанией IBM для своих компьютеров. Используется до сих пор.
	\item DARPA (TCP/IP, Internet) -- самая известная архитектура, разработанная министерством обороны США.
	\item IPv6 -- разновидность TCP/IP. Это отдельная архитектура, во многом отличающаяся от оригинальной.
	\item Novell Netware -- ранее самая популярная архитектура локальных сетей. Сейчас архитектура осталась, но протоколы почти не используются.
	\item SMB (Server Message Block) -- совместная разработка Microsoft и IBM для ОС Windows и OS/2. Используется для доступа к разделяемым ресурсам -- к серверам, принтерам, итп. 
	\item AppleTalk -- архитектура компании Apple для устройств Apple. Ни с чем остальным не совместима.
	\item XNS -- архитектура компании Xerox. На основе нее была сделана архитектура TCP/IP и некоторые другие.
\end{itemize}

Мы подробно будем говорить только о TCP/IP и IPv6.

\pagebreak

\Subsection{Характеристики архитектур К. С.}

Все сетевые архитектуры обладают следующим характеристикам:

\begin{itemize}
	\item Иерархия протоколов (в основном протоколов третьего уровня и выше)
	\item Соответствие модели ISO
	\item Адресация. В сети находится много сущностей, которые можно адресовать:
	\begin{itemize}
		\item Узлы. Существует несколько видов адресации узлов:
		\begin{itemize}
			\item Индивидуальная -- один адрес соответствует одному узлу (например сетевой карте или маршрутизатору).
			\item Групповая -- один адрес соответствует группе узлов. Например когда хотим что-то вещать для фиксированной группы устройств.
			\item Широковещательная -- когда надо сообщить что-то всем объектам сети.
		\end{itemize}
		В TCP/IP используются все эти виды.
		\item Приложения. На транспортном уровне появляются приложения и сервисы, и их тоже надо адресовать.
	\end{itemize} 
	\item Связь с канальным уровнем. Хоть мы от него и абстрагируемся, нам надо как-то пользоваться его функциями. Механизм связи включает в себя:
	\begin{itemize}
		\item Разрешение адресов, то есть сопоставление адресам сетевого уровня адресов канального уровня.
		\item Фрагментация. Пакеты сетевого уровня могут быть большие, а кадры на канальном уровне маленькие. Поэтому сетевые пакеты приходится разбивать на кусочки. Когда мы передаем пакет между устройствами, он может проходить через несколько промежуточных узлов. При этом канальный уровень между ними может быть разный на разных участках маршрута. Поэтому фрагментация бывает двух видов:
		\begin{itemize}
		 	\item Поузловая -- каждый промежуточный узел сам обеспечивает фрагментацию. Используется в IPv4.
		 	\item На источнике -- делим сразу так, чтобы заведомо прошло весь путь. Используется в IPv6.
		 \end{itemize}
	\end{itemize}
	\item Сетевые протоколы -- это основа архитектуры, за счет протоколов всё и функционирует.
	\item Маршрутизация -- одна из самых сложных функций. Она характеризуется различными атрибутами:
	\begin{itemize}
	 	\item По типу маршрута:
	 	\begin{itemize}
	 		\item Индивидуальная -- строим маршрут для передачи информации одному узлу.
	 		\item Групповая -- строим маршрут для передачи информации группе узлов.
	 	\end{itemize}
	 	\item По адаптивности к изменениям в сети:
	 	\begin{itemize}
	 		\item Статическая -- не зависит от того, что происходит в сети, администратор сам определяет, как идут пакеты.
	 		\item Динамическая -- оптимальный маршрут строится автоматически с помощью динамических протоколов.
	 		\item Предопределенная ("от источника") -- отправляемый пакет сам определяет, через какие узлы ему надо пройти.
	 	\end{itemize}
	 	\item По месту проведения маршрутных вычислений:
	 	\begin{itemize}
	 		\item Централизованная -- один узел рассчитывает маршруты для всей сети.
	 		\item Децентрализованная -- каждый узел решает, как проводить маршрутизацию.
	 		\item Гибридная -- часть вычислений происходит централизованно, часть на местах.
	 	\end{itemize}
	 	\item По числу возможных маршрутов:
	 	\begin{itemize}
	 		\item Однопутевые -- между двумя точками бывает одновременно только один маршрут.
	 		\item Многопутевые -- между двумя точками может быть одновременно несколько маршрутов.
	 	\end{itemize}
	 	\item По характеру информации, необходимой для маршрутизации:
	 	\begin{itemize}
	 		\item Глобальные -- необходимо знать информацию обо всей сети, чтобы построить оптимальный маршрут.
	 		\item Локальные -- достаточно только информации о соседях.
	 		\item Смешанные -- нужна локальная информация и часть глобальной.
	 	\end{itemize}
	\end{itemize}
	\item Транспортные механизмы -- обеспечивают заданный уровень надежности передачи данных между приложениями. Транспортные механизмы бывают:
	\begin{itemize}
		\item Датаграммные -- менее надежные. Позволяют просто доставить пакет от одного приложения к другому.
		\item Потоковые -- обеспечивают уровень надежности и порядок доставки данных.
		\item Многопоточные -- когда необходимо поддерживать сразу несколько каналов связи между двумя приложениями.
	\end{itemize}
	\item Именование ресурсов (в TCP/IP это DNS)
	\begin{itemize}
		\item Одноуровневые.
		\item Двухуровневые.
		\item Иерархические -- используется в TCP/IP.
	\end{itemize}
	\item Прикладные протоколы -- протоколы удаленного терминала, передачи файлов, почты, итп.
	\item Управление.
	\item Защита информации.
\end{itemize}

\Subsection{История создания интернета}

\begin{itemize}
	\item 1957 г. -- создание DARPA (Defense Advances Research Projects Agency). Агентство занималось созданием единой сети для всех военных узлов США.
	\item 1968 г. -- разработка сети ARPANET. Эта сеть не использовала протоколов TCP/IP, так как их тогда не существовало.
	\item 1969 г. -- осуществление первой передачи данных между двумя университетами.
	\item 1974 г. -- разработка TCP/IP.
	\item 1983 г. -- переход ARPANET с NCP на TCP/IP.
	\item 1984 г. -- создание системы DNS.
	\item 1989 г. -- создание WWW -- протокол HTTP, система серверов, использование браузеров.
	\item 1990 г. -- переход на единый термин Internet.
	\item 1993 г. -- первый графический веб-браузер Mosaic. До этого браузеры были текстовыми и использовались из консоли.
\end{itemize}

\Subsection{Стандартизующие организации Internet}

В других стандартах, например Ethernet, обычно прописаны все детали до мелочей. Для Internet такого нет. Он развивался довольно демократично и как таковых стандартов для него не существует. Есть ряд организаций, которые занимаются его развитием:

\begin{itemize}
	\item Internet Society (ISOC) -- сообщество, занимающееся развитием интернета. В ее составе есть организация:
	\begin{itemize}
	 	\item Internet Architecture Board (IAB) -- координирует развитие TCP/IP. Эта организация состоит из департаментов:
	 	\begin{itemize}
	 		\item Internet Engineering Steering Group (IESG) -- рассматривает стандарты и технические работы для IETF.
	 		\item Internet Engineering Task Force (IETF) -- отвечает за разработку стандартов на протоколы и архитектуру Internet.
	 		\item Internet Research Task Force (IRTF) -- занимается развитием технологий, которые могут понадобиться для Internet в будущем.
	 		\item Internet Corporation of Assigned Names and Numbers (ICANN) -- занимается централизованным назначением IP-адресов и всем, что с этим связано.
	 	\end{itemize}
	 \end{itemize} 
\end{itemize}

\Subsection{Координирующие организации Internet}

\begin{itemize}
	\item Network Information Center (NIC) -- организации, ответственные за распределение адресов:
		\begin{itemize}
			\item InterNIC -- на территории США.
			\item Reseaux IP Europeens (RIPE) -- в Европе.
			\item African Network Information Centre (AfriNIC) -- в Африке.
			\item Asia Pacific Network Information Centre (APNIC) -- в Азии и Океании.
			\item Regional Latin-American and Caribbean IP Address Registry (LACNIC) -- в Латинской Америке.
			\item Russian Institute for Public Networks (RIPN) -- в России.
		\end{itemize}
\end{itemize}

\Subsection{Стандарты TCP/IP}

Все стандарты, касающиеся интернета, формируются через RFC (Request for Comments). Все эти документы можно найти по \href{http://www.ietf.org/standards/rfcs/}{по ссылке}.

Часть этих документов являются информационными -- FYI (For Your Information), а остальные стандартами -- STD (Standard).
