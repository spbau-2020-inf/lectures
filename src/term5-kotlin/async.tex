\Section{Асинхронное программирование}

В современных программах большая часть кода чего-то ждет -- получения данных, пользовательского ввода, итд. Рассмотрим простой пример. У нас есть UI, который просит пользователя ввести некий \texttt{token}, затем по этому токену и некоторому объекту \texttt{item} делаем запрос на сервер и как-то обрабатываем ответ.

\begin{lstlisting}
fun postItem(item: Item) {
  val token = requestToken()
  val post = createPost(token, item)
  processPost(post)
}
\end{lstlisting}

\Subsection{Потоки и блокировки}

Одна из возможных реализаций функции \texttt{requestToken()} -- это ручная блокировка, что-то типа спинлока. Показываем пользователю UI с формой и в цикле ждем, пока он не заполнит форму.

\begin{lstlisting}
fun requestToken(): Token {
  // показать UI сформой
  ...

  while (true) {
    if (ready()) {
      return token
    }
  }
}
\end{lstlisting}

Активное ожидание -- это не всегда хорошо, поэтому можно вынести UI в отдельный поток и заджойнить его.

\begin{lstlisting}
fun requestToken(): Token {
  val t = Thread {
    // показать UI с формой в отдельном потоке
  }
  t.start()

  // заблокироваться на поток, пока не получим результат   
  t.join()
  return token
}
\end{lstlisting}

Однако плодить потоки мы тоже не всегда хотим. В частности, каждый поток отбирает порядка 2мб дополнительной памяти. А в некоторых языках, например в JavaScript, потоков нет совсем.

\Subsection{Callbacks}

Следующий вариант -- колбеки. Передаем в функцию \texttt{requestTokenAsync()} лямбду, которая выполнится, когда токен будет получен.

\begin{lstlisting}
fun requestTokenAsync(callback: (Token) -> Unit) { ... }
\end{lstlisting}

Тогда наша основная функция будет выглядеть так.

\begin{lstlisting}
fun postItem(item: Item) {
    requestTokenAsync { token ->
        createPostAsync(token, item) { post ->
            processPost(post)
        }
    }
}
\end{lstlisting}

Видим кучу вложенных лямбд и вспоминаем про Callback hell. Чтобы этого избежать, в других языках есть концепции Future/Promises/Rx, это объекты, которые возвращаются из функций с асинхронной логикой и инкапсулируют в себе колбеки, позволяя писать более простой и понятный код.

\begin{lstlisting}
fun requestTokenAsync(): CompletableFuture<Token> { ... }

fun postItem(item: Item) {
    requestTokenAsync()
        .thenCompose { token -> createPostAsync(token, item) }
        .thenAccept { post -> processPost(post) }
}
\end{lstlisting}

Метод \texttt{thenCompose()} дожидается результата \texttt{CompletableFuture} и вызывает переданный ему колбек. Метод \texttt{thenAccept()} разворачивает все вложенные друг в друга \texttt{CompletableFuture}, дожидаясь их всех, и обрабатывает результат. Методов для работы с \texttt{CompletableFuture} много, их все приходится помнить, а работать с ними сложнее чем с обычным последовательным кодом.

\Subsection{Корутины}

Котлин предлагает альтернативный подход -- корутины. Добавим к сигнатурам наших асинхронных функций волшебное слово \texttt{suspend}. Главную функцию тоже сделаем \texttt{suspend}. Теперь можем писать обычный последовательный код.

\begin{lstlisting}
suspend fun requestTokenAsync(): Token { ... }
suspend fun createPostAsync(token: Token, item: Item): Post { ... }

suspend fun postItem(item: Item) {
  val token = requestTokenAsync()
  val post = createPostAsync(token, item)
  processPost(post)
}
\end{lstlisting}

Посмотрим, во что компилятор трансформирует такую функцию. Ей в параметры добавляется объект типа \texttt{Continuation}. Затем создается конечный автомат, у которого есть метод \texttt{resumeWith()}. Весь код функции разбивается на блоки между \texttt{suspend}-вызовами. В каждом блоке выполняется логика и вызывается вложенная \texttt{suspend} функция, при этом автомат передается в неё. Таким образом, \texttt{Continuation} в данном случае является подобием колбека, который вызывает эту же функцию, с новым состоянием автомата.

\pagebreak

\begin{lstlisting}
fun postItem(item: Item, c: Continuation) {
  val stateMachine = c ?: object : Continuation<Unit> {
    override fun resumeWith(result: Result<Unit>) {
      postItem(null, this)
    }
  }

  when (stateMachine.label) {
    0 -> {
      stateMachine.item = item
      stateMachine.label = 1
      requestTokenAsync(stateMachine)
    }
    1 -> {
      createPostAsync(token, item, stateMachine)
    }
    ...
  }
}
\end{lstlisting}

Но, поскольку функция \texttt{postItem()} теперь \texttt{suspend}, её нельзя вызвать из нормальной функции. Вместо этого можно использовать coroutine builder. Для этого просто передадим наш код в функцию \texttt{launch()} из библиотеки \texttt{kotlinx.coroutines}. Она выполнит код в фоновом потоке.

\begin{lstlisting}
fun postItem(item: Item) {
  runBlocking {
    val job = launch {
      val token = requestTokenAsync()
      val post = createPostAsync(token, item)
      processPost(post)  
    }
    job.join()
  }
}
\end{lstlisting}

Важно заметить, что весь код, в том числе и  обработка результата (\texttt{processPost()}) выполнится в каком-то другом потоке, но иногда мы хотим что-то обрабатывать в конкретном потоке. Например, в android трогать UI можно только из UI-потока. Поток можно задать в параметре \texttt{launch} с помощью \texttt{Dispatcher}.

\begin{lstlisting}
launch(Dispatchers.Main) { ... }
\end{lstlisting}

\pagebreak

\Subsection{Экскурс в C\# -- \texttt{async/await}}

В языке C\# с асинхронными функциями можно работать через механизм \texttt{async/await}. Функция, помеченная ключевым словом \texttt{async}, возвращает таску, в которой будет лежать результат, когда будет готов. Если же перед вызовом такого метода написать слово \texttt{await}, то функция выполнится синхронно, то есть мы дождемся её завершения и получим готовый результат.

\begin{lstlisting}
// C\#
async Task<Token> requestToken() { ... }
async Task<Post> createPost(Token token, Item item) { ... }
void processPost(Post post) { ... }

async task postItem(Item item) {
  var token = await requestToken();
  var post = await createPost();
  processPost();
}
\end{lstlisting}

Если же вызвать \texttt{async} функцию без слова \texttt{await}, то она начнет выполняться в другом потоке, а нам сразу вернется объект типа \texttt{Task}, с помощью которого мы позже сможем так же дождаться результата. Такой подход сохраняет параллелизм. Убедимся в этом на примере с параллельной загрузкой двух картинок.

\begin{lstlisting}
// C\#
async Task<Image> loadImage(String name) { ... }

var task1 = loadImage("img1");
var task2 = loadImage("img2");
// картинки уже начали параллельно загружаться
var image1 = await task1;
var image2 = await task2;
\end{lstlisting}

\Subsection{\texttt{async/await} в котлине}

В котлине \texttt{async} -- это не ключевое слово, а просто функция, принимающая на вход корутину и возвращающая объект типа \texttt{Deferred} (аналог \texttt{Task}). Эта функция находится в библиотеке \texttt{kotlinx.coroutines}. Ключевого слова \texttt{await} в котлине тоже нет, зато у \texttt{Deffered} есть метод \texttt{await()}, который и содержит логику ожидания и получения результата. В итоге, на котлине код из предыдущего примера будет выглядеть так.

\begin{lstlisting}
// Kotlin
fun loadImage(name: String): Deferred<Image> = async { ... }

val deferred1 = loadImage("img1")
val deferred2 = loadImage("img2")

val image1 = deferred1.await()
val image2 = deferred2.await()
\end{lstlisting}

\pagebreak

Более явный способ использования \texttt{async/await} -- сделать \texttt{loadImage()} обычной функцией, а её вызов обернуть в \texttt{async\{ ... \}}.

\begin{lstlisting}
// Kotlin
fun loadImage(name: String): Image { ... }

val deferred1 = async { loadImage("img1") }
val deferred2 = async { loadImage("img2") }

val image1 = deferred1.await()
val image2 = deferred2.await()
\end{lstlisting}

В таком случае мы сможем, когда надо, запускать функцию асинхронно, а обычный вызов будет последовательным. Таким образом мы подчеркиваем, что по-умолчанию у нас всё последовательно, а чтобы запустить что-то асинхронно, надо об этом явно заявить.

\Subsection{Ментальная модель корутин}

О корутинах можно думать, как о легковесных потоках, ведь они занимают гораздо меньше дополнительных ресурсов, чем настоящие java-потоки. Например, мы можем создать сто тысяч корутин и без проблем их запустить.

\begin{lstlisting}
fun foo() {
  runBlocking {
    val jobs = List(100_000) {
      launch {
        delay(1000)
        println("...")
      }
    }

    jobs.forEach { it.join() }
  }
}
\end{lstlisting}

Аналогичный же код с настоящими java-потоками на большинстве компьютеров упадет с ошибкой \texttt{OutOfMemoryError}.

\Subsection{Шаблон CSP}

Communicating Sequential Processes -- это шаблон проектирования распределенных систем. До того как этот шаблон придумали, для взаимодействия между потоками использовался shared state, что вело к куче проблем и сложностей с синхронизацией и эффективностью. Шаблон CSP предлагает вместо общего состояния использовать альтернативные способы общения между потоками.

\pagebreak

\Subsubsection{Channel}

Один из способов межпроцессного взаимодействия -- это каналы. Запустим параллельно две корутины, одна будет писать числа в канал, а другая читать числа из канала и выводить в stdout.

\begin{lstlisting}
fun foo() = runBlocking<Unit> {
    val channel = Channel<Int>()

    launch(coroutineContext) {
        repeat(10) {
            delay(100)
            channel.send(it)
        }
        channel.close()
    }

    launch(coroutineContext) {
        for (x in channel) {
            println(x)
        }
    }
}
\end{lstlisting}

\Subsubsection{Actors}

В классическом CSP мы явно работаем с объектами, через которые происходит взаимодействие (например с каналами). В модели акторов мы делаем по-другому. Актор -- это именованная корутина и канал, с помощью которого она сможет общаться с другими акторами. Когда один актор хочет что-то сообщить другому, он вызывает у того соответствующий метод, в котором сокрыта логика работы с каналами.

\begin{lstlisting}
runBlocking<Unit> {
    val printer = actor<Int>(coroutineContext) {
        for (i in channel) {
            println(i)
        }
    }

    launch(coroutineContext) {
        repeat(10) {
            delay(100)
            printer.send(it)
        }
        printer.close()
    }
}
\end{lstlisting}

\pagebreak

Посмотрим, как можно реализовать подсчет чисел фибоначчи с помощью акторов.

\begin{lstlisting}
runBlocking {
    val fibonacci = produce { // создаем актора
        var a = 1
        var b = 1
        while (true) {
            send(a)
            val temp = a + b
            a = b
            b = temp
        }
    }

    println(fibonacci.receive()) // 1
    println(fibonacci.receive()) // 1
    println(fibonacci.receive()) // 2

    fibonacci.cancel() // прерываем актора
}
\end{lstlisting}

Функция \texttt{produce()} создает актора, а внутри мы вместо \texttt{yield()} используем \texttt{send()} для отправки следующего числа наружу. Важно, что функция \texttt{send()} засыпает до тех пор, пока отправленное значение не прочитают. Поэтому, после того как мы прочитали столько значений, сколько нам нужно, мы прерываем актора методом \texttt{cancel()}.

\Subsection{Язык vs библиотека}

Современные языки стараются делать как можно меньше. Это проще для переносимости и для совместимости с разными платформами. Поэтому в котлине для корутин на уровне языка реализованы только ключевое слово \texttt{suspend} и возможность трансформировать корутину в конечный автомат. Всё остальное -- \texttt{async/await}, каналы, акторы и прочее -- реализовано в виде библиотеки, написанной уже на чистом котлине. Про все возможности библиотеки \texttt{kotlinx.coroutines} можно прочитать \href{https://github.com/Kotlin/kotlinx.coroutines/blob/master/docs/coroutines-guide.md}{здесь}.