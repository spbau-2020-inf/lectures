\Section{Coroutines}

\Subsection{Sequence}

В Java 8 есть \texttt{stream}'ы, которые позволяют делать ленивые вычисления и создавать бесконечные последовательности. В котлине для этого есть интерфейс \texttt{Sequence} и различные функции для работы с ним.

Чтобы создать, например, бесконечную последовательность натуральных степеней двойки, достаточно вызвать функцию \texttt{generateSequence()} и передать в неё начальное значение и функцию получения следующего элемента из предыдущего.

\begin{lstlisting}
val powerOfTwo = generateSequence(seed = 1) { 2 * it }
println(powerOfTwo.take(5).joinToString(" ")) // 1 2 4 8 16
\end{lstlisting}

Рассмотрим как Sequence работает изнутри. Интерфейс \texttt{Sequence} имеет метод \texttt{iterator()}, который возвращает обычный итератор, с помощью которого работают такие функции как \texttt{take(), map(), filter(), } и.т.п.

\begin{lstlisting}
interface Sequence<out T> {
  operator fun iterator(): Iterator<T>
}
\end{lstlisting}

Функция \texttt{generateSequence()} возвращает экземпляр класса \texttt{GeneratorSequence}, который реализует интерфейс \texttt{Sequence}.

\begin{lstlisting}
fun <T : Any> generateSequence(seed: T?, nextFunction: (T) -> T?): Sequence<T> =
  GeneratorSequence(seed, nextFunction)
\end{lstlisting}

Осталось понять, как класс \texttt{GeneratorSequence} реализует реализует итератор.

\begin{lstlisting}
class GeneratorSequence<T : Any>(val seed: T?, val nextValue: (T) -> T?) : Sequence<T> {
  inner class SequenceIterator : Iterator<T> {
    var nextItem: T? = null

    // -2 for initial unknown       
    // -1 for next unknown        
    // 0 for done       
    // 1 for continue
    var nextState: Int = -2
    ...
  }

  override operator fun iterator(): Iterator<T> = SequenceIterator()
}
\end{lstlisting}

У итератора есть несколько состояний.
\begin{itemize}
 	\item[-2] начальное состояние, когда еще ничего не проинициализировано;
 	\item[-1] следующий элемент еще неизвестен;
 	\item[0]  следующий элемент -- \texttt{null}, это значит, что последовательность закончилась;
 	\item[1]  можно продолжать итерироваться.
\end{itemize}

Вычислением следующего элемента занимается метод \texttt{calcNext()}. Если находимся в начальном состоянии, то инициализируемся переданным значением, иначе вызываем переданную функцию от текущего значения. Если текущее значение стало \texttt{null}, переходим в конечное состояние.

\begin{lstlisting}
private fun calcNext() {
  nextItem = if (nextState == -2) getInitialValue() else getNextValue(nextItem!!)
  nextState = if (nextItem == null) 0 else 1
}
\end{lstlisting}

Метод \texttt{next()} проверяет, что мы не в конечном состоянии, получает, если нужно, следующий элемент и возвращает его, если тот не \texttt{null}.

\begin{lstlisting}
override fun next(): T {
  if (nextState < 0) calcNext()

  if (nextState == 0) throw NoSuchElementException()
  
  val result = nextItem as T
  nextState = -1
  return result
}
\end{lstlisting}

Метод \texttt{hasNext()} получает, если нужно, следующий элемент и проверяет, что он действительно есть.

\begin{lstlisting}
override fun hasNext(): Boolean {
  if (nextState < 0) calcNext()
  return nextState == 1
}
\end{lstlisting}

Функции для последовательностей -- \texttt{map(), filter()} и им подобные, должны работать лениво, поэтому они просто оборачивают переданные им последовательности в специальные декораторы, добавляющие итераторам нужную логику.

\begin{lstlisting}
public fun <T, R> Sequence<T>.map(transform: (T) -> R): Sequence<R> {
  return TransformingSequence(this, transform)
}
\end{lstlisting}

\Subsection{Sequence + yield}

Усложним пример. Теперь у нас есть бесконечный цикл, активно генерирующий значения, мы хотим превратить его в ленивый.

\begin{lstlisting}
fun fibonacci() = sequence {
    var a = 0
    var b = 1
    var temp: Int
    while (true) {
        yield(a) // магия, возвращающая значение и продолжающая вычисления
        temp = a
        a = b
        b += temp
    }
}

println(fibonacci().take(8).toList()) // {0, 1, 1, 2, 3, 5, 8, 13}
\end{lstlisting}

Функция \texttt{sequence()} принимает экстеншн функцию для класса \texttt{SequenceScope}, в котором, собственно, и объявлен магический метод \texttt{yield()}. Также видим везде магическое слово \texttt{suspend}.

\begin{lstlisting}
public fun <T> sequence(block: suspend SequenceScope<T>.() -> Unit): Sequence<T>

public abstract class SequenceScope<in T>() {
  public abstract suspend fun yield(value: T)
  ...
}
\end{lstlisting}

Когда лямбда передается с типом \texttt{suspend}, она трансформируется в специальный класс. Посмотрим на него. В нем есть локальные переменные из нашей лямбды, а так же текущее состояние. Вся логика находится в методе \texttt{resume()}. Он проверяет, в каком состоянии находится, и в зависимости от этого выполняет разные части лямбды.

\begin{lstlisting}
class <anonymous_for_state_machine>
extends CoroutineImpl<...> implements Continuation<Object> {
  // Текущее состояние
  int label = 0;

  // локальные переменные из лямбды
  Integer a = null;
  Integer b = null;
  Integer temp = null;

  void resume(Object data) {
    if (label == 0) goto L0;
    if (label == 1) goto L1;
    else throw IllegalStateException();
    ...
  }
}
\end{lstlisting}

В нашем случае с \texttt{fibonacci()} у \texttt{label} будет два состояния, а тело лямбды преобразуется следующим образом.

\begin{lstlisting}
L0:
  a = 0
  b = 1
  temp = 0
  label = 1
LOOP:
  label = 1
  data = yield(a, this)
  if (data == COROUTINE_SUSPENDED) return
L1:
  temp = a;
  a = b;
  b += temp;
  label = -1
  goto LOOP
END:
  label = -1
  return
\end{lstlisting}

Изначально \texttt{label == 0}, поэтому код начнется с начала, войдет в цикл и вызовет метод \texttt{yield()}. После этого проверит, не прервали ли нас и, если прервали, то выйдет. При следующем вызове \texttt{resume()} у нас будет состояние \texttt{label == 1}, поэтому мы пойдем сразу на метку \texttt{L1} и продолжим выполнение цикла, пока снова не попадем на \texttt{yield()}.

Таким образом, если мы вызовем функцию \texttt{fibonacci()}, она нам вернет \texttt{Sequence}. У него мы возьмем итератор, и при каждом вызове \texttt{next()}, будет вызываться сгенерированный метод \texttt{resume()}, доходить до \texttt{yield()}, возвращать следующее значение и прерываться до следующего вызова \texttt{next()}.

Сгенерированный для лямбды класс называется корутина.

Реализация итератора для таких последовательностей похожа на то, что мы видели для \texttt{GeneratorSequence}, только, помимо прочего, он содержит метод \texttt{yield()}, который вызывается из нашей корутины. Он сохраняет переданное значение в качестве следующего элемента, переходит в состояние \texttt{State\_Ready} и возвращает маркер \texttt{COROUTINE\_SUSPENDED}, который говорит корутине, что ей надо прерваться.

\begin{lstlisting}
suspend override fun yield(value: T) {
  nextValue = value
  state = State_Ready
  return suspendCoroutineOrReturn { c ->
    nextStep = c
    COROUTINE_SUSPENDED
  }
}
\end{lstlisting}

\Subsection{Continuation}

В рассмотренном примере контроль управления передается явно через продолжения (Continuation). Этот подход похож на коллбеки и заключается в следующем -- есть интерфейс \texttt{Continuation} с методом \texttt{resume()} и сложная функция, которая что-то вычисляет. Это всё передается в код, который запускает сложную функцию, дожидается её завершения, после чего вызывает \texttt{Continuation.resume()}, который далее что-то делает с результатом сложной функции.

\begin{lstlisting}
interface Continuation<T> {
  fun resume(t: T)
}

fun <T> yield(complexFun: () -> T, c: Continuation<T>) {
    val value = complexFun()
    c.resume(value)
}
\end{lstlisting}

Рассмотрим пример использования такого подхода. Передадим в функцию \texttt{yield()} лямбду, которая просто вызывает \texttt{foo()}, а в качестве коллбека -- \texttt{Continuation}, внутри которого еще раз вызовем \texttt{yield()} с лямбдой, вызывающей \texttt{bar()} и еще одним \texttt{Continuation}, который вызывает \texttt{baz()}.

\begin{lstlisting}
yield({ foo() }, object : Continuation<Int> {
  override fun resume(t1: Int) {
    // Здесь значение уже точно вычислено
    yield({ bar(t1) }, object : Continuation<Int> {
      override fun resume(t2: Int) { baz(t2) }
    })
  }
})
\end{lstlisting}

Таким образом мы построили цепочку из двух коллбеков, которые будут нам гарантировать последовательное выполнение \texttt{t1 = foo()}, \texttt{t2 = bar(t1)} и \texttt{baz(t2)}.

С увеличением вложенности, растет количество скобочек и кода в целом, при этом сильно снижается читаемость кода. Такая проблема называется Callback Hell, она распространена в JavaScript и других похожих языках.

Чтобы избежать коллбек-ада, в котлине введено ключевое слово \texttt{suspend}, которое при трансляции в байт-код берет функцию и разворачивает её в корутину, которая уже работает на коллбеках и \texttt{yield}'ах.

\Subsection{Suspend-функции}

На \texttt{suspend}-функции накладываются ограничения. Так как к таким функциям при компиляции добавляется еще один параметр, который берется из сгенерированного кода, \texttt{suspend}-функции можно вызывать только из других \texttt{suspend}-функций или из \texttt{suspend}-лямбд.

\begin{lstlisting}
suspend fun foo() { ... }

fun usual() { foo() } // Ошибка

suspend fun other() { foo() } // OK

fun func(f: suspend () -> Unit) { ... }
func { foo() } // OK
\end{lstlisting}

Такой код будет превращаться в следующее.

\begin{lstlisting}
suspend fun foo() { ... }
suspend fun other() { foo() }
// превратится в
fun foo(c: Continuation): Any?
fun other(c: Continuation): Any { foo(c) }
\end{lstlisting}

То есть при вложенных вызовах, Continuation будет передаваться во все функции.

\Subsection{Разоблачение магии}

Разберемся, как же работает \texttt{SequenceBuilderIterator}. Для этого вернемся к методу \texttt{yield()}. Он возвращает такую страшную конструкцию.

\begin{lstlisting}
return suspendCoroutineUninterceptedOrReturn { c ->
  nextStep = c
  COROUTINE_SUSPENDED
}
\end{lstlisting}

Suspend-функция \texttt{suspendCoroutineUninterceptedOrReturn()} -- это заглушка, позволяющая вытащить переданный ей неявно \texttt{Continuation}, чтобы в \texttt{yield()} его можно было сохранить в итераторе и вызвать на следующем шаге.

Итак, что же происходит с нашей функцией вычисления фибоначчи при компиляции. Лямбда, переданная в функцию \texttt{sequence()}, превращается в анонимный класс, унаследованный от \texttt{Continuation}. Тело лямбды разбивается на куски между вызовами \texttt{yield()} и попадает в метод \texttt{resume()}. Каждый вызов \texttt{yield(a)} заменяется на вызов \texttt{yield(a, this)}, где \texttt{this} -- это тот же самый объект, который сейчас работает. Таким образом, наша лямбда сказала итератору, чтобы на следующем шаге он вызвал её еще раз. Поэтому, при следующем вызове \texttt{iterator.next()}, снова вызовется наша бывшая лямбда, только уже в новом состоянии, и выполнит еще один шаг вычислений.