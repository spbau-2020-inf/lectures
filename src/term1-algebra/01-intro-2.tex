\Subsection{Бинарные отношения}

Пусть $X$, $Y$~--- множества, а $R \subseteq X \times Y$.

\begin{definition}\label{def:intro-rel0}
  $R$ называется отношением между объектами из $X$ и $Y$. Запись $x R y$ означает, что $(x,\,y) \in R$.
\end{definition}

\begin{remark} Как правило нас будут интересовать интересовать ситуация, когда $X = Y$, т.е. отношение между элементами одного множества. Такие отношения называются отношениями \textbf{на множестве} $X$.
\end{remark}

\begin{example}[1] $X = Y$, отношение равенства.
\end{example}

\begin{example}[2] $X = Y = \R$, отношение $\le$.
\end{example}
  
\begin{example}[3] $X = Y = \N$, отношение кратности $\divby$
\end{example}

\begin{example}[4] \hyperref[def:intro-func2]{График функции} тоже является отношением.
\end{example}

\begin{definition}\label{def:intro-relchar}
  Бинарное отношение на множестве $M$ называется:\slashn
  \begin{itemize}
  \item Рефлексивным, если $\forall x \in M\colon xRx$.
  \item Антирефлексивным, если $\forall x \in M\colon \lnot (xRx)$.
  \item Симметричным, если $\forall x, y \in M\colon xRy \implies yRx$.
  \item Антисимметричным, если $\forall x, y \in M\colon xRy \land yRx \implies x = y$.
  \item Асимметричным, если $\forall x, y \in M\colon xRy \implies \lnot (yRx)$.
  \item Транзитивным, если $\forall x, y, z \in M\colon xRy \land yRz \implies xRz$.
  \end{itemize}
\end{definition}

\begin{remark}
  Отношение асимметрично тогда и только тогда, когда оно антисимметрично и антирефлексивно.
\end{remark}

\begin{definition}\label{def:intro-releq}
  Отношение называется отношением эквивалентности, если оно рефлексивно, транзитивно, симметрично.
\end{definition}

\begin{remark}
  Отношения эквивалентности часто обозначаются через $\sim$.
\end{remark}

\begin{definition}
  Пусть $\sim$~--- отношение эквивалености, классом эквивалентности элемента $x$ называется множество элементов, состоящих с ним в отношении:
  \begin{compactitem}
    \item $\bar{x} = \{y \mid x \sim y\}$
  \end{compactitem}
\end{definition}

\begin{lemma}\slashn
  \begin{enumerate}
  \item Классы экивалентности совпадают или не пересекаются.
  \item Множество распадается на диъюнктное объединение ($\sqcup$) классов эквивалентности.
  \item Всякое разбиение множества $X$ на непересекающиеся подмножества есть разбиение на классы эквивалентности по этому признаку.
  \end{enumerate}
\end{lemma}
\begin{proof}\slashn
  \begin{enumerate}
  \item Пусть $\bar{x} \cap \bar{y} \ne \emptyset \implies \exists z \in \bar{x} \cap \bar{y}$.

    Тогда $z \sim x, z \sim y \implies x \sim y \implies \bar{x} = \bar{y}$.

    Действительно, $t \in \bar{x} \iff t \in \bar{y}$, по определению отношения эквивалентности \ref{def:intro-releq}.

  \item Заметим, что $X = \bigcup\limits_{x \in X} \bar{x}$ (каждый $x$ входит хотя бы в свой класс эквивалентности).

    Но классы эквивалентности либо совпадают, либо не пересекаются, поэтому можно из объединения убрать совпадающие классы, оставив каждый только в одном экземпляре, и тем самым получить требуемое разбиение.

  \item Определим $x ~ y$, когда $x$ лежит в том же подмножестве, что и $y$. Заметим, что:
    \begin{compactitem}
    \item Верна рефлексивность ($x \sim x$)
    \item Верна симметричность ($x \sim y \implies y \sim x$).
    \item Верна транзитивность ($x \sim y, y \sim z \implies x \sim z$). \qedhere
    \end{compactitem}
  \end{enumerate}
\end{proof}

\begin{definition}\label{def:intro-factoreq}
  Пусть $X$~--- множество, $\sim$ ~--- отношение эквивалентности на $X$ (\ref{def:intro-releq}). Тогда $X/{\sim}$~--- множество всех классов эквивалентности.
\end{definition}

\begin{example}[1] Отношение равенства является отношением эквивалентности.
\end{example}

\begin{example}[2] Сравнимость по модулю является отношением эквивалентности:

  Пусть $a, b \in \Z, n \in N$.~~~ $a \sim b := (a - b) \divby n$.

  Проверим определение отношения эквивалентности:
  \begin{compactitem}
  \item Рефлексивность: $x - x = 0 \divby n$.
  \item Симметричность: $(x - y) \divby n \implies (y - x) \divby n$.
  \item Транзитивность: $(x - y) \divby n, (y - z) \divby n \implies (x - y) + (y - z) \divby n \implies (x - z) \divby n$.
  \end{compactitem}

  \medskip

  $\Z/{\sim}$ принято обозначать как $Z/{n\Z}$, подробнее об этом будет рассказано позднее, но уже сейчас можем понять как устроено это множество: $\Z/{n\Z} = \{\overline{0},\,\overline{1},\,\ldots\overline{n - 1}\}$, где:
  \begin{compactitem}
  \item $\overline{0} = \{m \in \Z \mid m \divby n\}$
  \item $\overline{1} = \{m \in \Z \mid (m - 1) \divby n\}$
  \item $\dots$
  \end{compactitem}

  Несложно понять, что эти классы непересекаются и в объединении дают всё множество $\Z$.
\end{example}

\begin{example}[3] Множество точек на плоскости, точки эквивалентны, если находятся на одинаковом расстоянии от точки $(0,\,0)$. Классы эквивалентности~--- все окружности с центром в $O$.

  \medskip
  \begin{tikzpicture}
    \draw[dashed] (0cm,0cm) circle(0.65cm);
    \draw[dashed, thick] (0cm,0cm) circle(1.55cm);
    \draw[dashed] (0cm, 0cm) circle (0.4cm);
    \draw[dashed, thick] (0cm, 0cm) circle (1.2cm);
    \node[below left] at (0cm, 0cm) {$O$};
    
    \draw[very thick] (0.705514cm, 0.970694cm) -- (0cm, 0cm) -- (1.2cm, 0cm);
  \end{tikzpicture}
  
\end{example}
