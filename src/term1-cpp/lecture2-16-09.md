Build
-----

build
- compile
- link
```
main.c → nmain.o ------|
first.c → first.o -----→ main.exe (исполняемый файл)
second.c → second.o ---|
```
Чего не хвататет? Добавки библиотеки.


Ответвление: include guards
---------------------------

1. guard (гард) #include
a)
```
a.h
#include "b.h"

b.h
#include "a.h"
```
b)
```
main.c ← first.h —→↓
	↑← second.h  ←—
```
```
main.c
#include <first.h>
#include <second.h>

first.h
|FIRST|

second.h
#include <first.h>
|SECOND|
```

Как препроцессор будет обрабатывать main.c?
Он должен обойти директывы (строки с #)

```
main.c →	|FIRST|
		#include "second.h"
		#include "first.h"
		|SECOND|
```
В итоге:
```
|FIRST|
|FIRST|
|SECOND|
```
Как можно это предотвратить?
1. С помощью директив (стандарт)
В препроцессоре можно заводить переменные.
```
first.h
#ifndef _FIRST_H_
#define _FIRST_H_
|FIRST|
#endif
```
2. Специфические команды (зависит от компилятора)
```
#pragma once
|FIRST|
```

Библиотеки
----------

архив объектных файлов (.o файлов)
```
1 libc
2 mylib

printf.o
string.o
sort.o

		 __
main.o —————→↓  |  |
first.o ———— →  |  |
second.o  ——→↑  |__|
		|printf|
		|______|
```
**Static way.** Минусы: нужно перекомпилить проект, чтобы использовать новые версии библиотек.
```
.a (от archive)
.lib (от library)
--static — ключ для статической линковки
-l mylib
```

**Dynamic way** (по умолчанию) — пользователь должен заранее иметь библиотеки
```
.so (от shared object)
.dl (от dynamic library)
```
ld — утилита-загрузчик

В RAM загружается отдельно исполняемый файл, отдельно libc и mylib

Debian (also, Ubuntu)
```
1.  myprog.tar.gz
	mylib.so
	mylib.h
	mylib.a

autotools — генерирует
	configure.sh
	Makefile

2. myprog.deb — архив с исполняемым файлом
	dpkg
	apt

3.
mylib.deb
mylib-dev.deb

4. cmake — генерирует
	sln (для MSVS)
	Makefile
```

Указатели
---------

```
целое	байт
------------
char	1
short	2
int	4
long	8

дробное байт
-----------
float	4
double	8 +-10e38

Для дробных:
|знак s | степень e | мантисса M|
(-1)^s * M * 10^e
```

```
int a = 4;
int s1 = sizeof(a);
int s2 = sizeof(int);
```

Целые числа хранятся с дополниельным кодом.

Массивы
-------

```
int a[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
int a[10];
int a[10] = {0};
```
```
|__________|
 0        9
```

Строки. ASCII.
--------------

```
koir8-r — кодировка линукса Я = 240
cp1251 —кодировка вина Я = 235
```
```
char s[] = {'H', 'i'};
char s[2];
char s[] = "Hello"; // размер s —6 элементов
```
Коварность
```
int b = a[3]; // 3
a[10] = c;
d = a[-1];
```
```
#define N 10
int a[N];
a[0] = ...;
int s = 0;
for (int i = 0; i < N; i++)
	s += a[i];
```

Указатели.
----------

```
int b = 3;
int *a = &b;

int c = *a;
```
Адресная арифметика.
```
int a[10];
int *pa = &(a[0])
// int *pa = a
pa = pa + 1
// a[i] → *(a + i)
```
