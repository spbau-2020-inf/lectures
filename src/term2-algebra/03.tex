% 01 марта 2017

\Subsection{Леммы о размерностях}

\begin{remark} Пусть $U$ и $V$~--- векторные пространство, и $\vphi\colon U \to V$~--- линейное отображение между ними.

$\vphi^{-1}(b) = \{\vphi(x) = b\} = \Ker \vphi + a$, где $a$~--- произвольный прообраз.

$\Ker \vphi + a$ является аффинным подпространством.
\end{remark}

\begin{definition} Непустое множество $V$ называется \emph{аффинным подпространством}, если для
любых $v, u \in V$ и чисел $\alpha, \beta \in K$, таких что $\alpha + \beta = 1$, верно $\alpha v + \beta u \in V$
\end{definition}

\begin{remark} В определении выше можно рассматривать любую конечную сумму $\sum \alpha_i v_i$, для $\sum \alpha_i = 1$.
  Несложно показать, что это одно и то же.
\end{remark}

\begin{remark} Определение аффинного подпространство весьма схоже с определением подпространства.
Если убрать ограничение $\alpha + \beta = 1$, то получится в точности необходимое и достаточное условие на подпространство.
\end{remark}

\begin{remark} Афинное подпространство \textbf{не} является подпространством в общем случае
\end{remark}

\begin{theorem}\label{thm:hom-theorem} Пусть $U$, $V$~--- векторные пространства над $K$, $\vphi\colon U \to V$~--- линейное отображение.
  Тогда $\Im \vphi \cong U{/}_{\Ker \vphi}$
\end{theorem}

\begin{proof} Построим отображение слева направо: $f\colon \Im \vphi \to U{/}_{\Ker \vphi}$

  $f(u) := \vphi^{-1}(u) = v + \Ker \vphi$, где $\vphi(v) = u$.
  
  Несложно увидеть, что отображение получилось линейным, сюръекция очевидна, ядро $\Ker f$ тривиально
  (только $\Ker \vphi$ отображается в $0 + \Ker \vphi$).
\end{proof}

\begin{lemma}\label{lm:factor-dimension} Пусть $U$~--- подпространство $V$, $U$ и $V$ имеют конечную размерность.

  Тогда $\dim V{/}_U = \dim V - \dim U$
\end{lemma}

\begin{proof} Пусть $\dim V = n$, $\dim U = m$.

  Пусть $u_1, \ldots, u_m$~--- базис $U$, дополним его до базиса $V$, пусть $u_1, \ldots, u_m, u_{m + 1}, \ldots, u_n$~--- базис $V$.

  Пусть для $x \in U$, $\overline{x} := x + U$~-- класс элемента $x$ в факторе $V{/}_U$.

  Покажем, что $\overline{u_{m + 1}}, \overline{u_{m + 2}}, \ldots, \overline{u_{n}}$~--- базис $V{/}_U$.

  \bigskip
  
  Предположим, что этот набор является линейно независимым: $\sum_{i = m + 1}^{n} \alpha_i \overline{u_i} = 0$

  Тогда $\sum_{i = m + 1}^{n} \alpha_i u_i = t \in U$, потому что в факторе это выражение равно $0$.

  Выразим $t$ в базисе $U$: $t = \sum_{i = 1}^m \beta_i u_i$

  Итого имеем $0 = t - t = \sum_{i = 1}^m \beta_i u_i - \sum_{i = m + 1}^{n} \alpha_i u_i$

  Сумма нетривиальна, если хотя бы одно $\alpha_i$ было не равно $0$.

  Противоречие с линейной независимостью $u_1, \ldots, u_n$.
  
  \bigskip

  Докажем, что любой $\overline{v} \in V{/}_U$ выражается.

  Разложим $v$ в $V$: $v = \sum_{i = 1}^n \alpha_i u_i$

  Разложение в $V{/}_U$ отличается не сильно: $\overline{v} = \sum_{i = 1}^{n} \alpha_i \overline{u_i} = \sum_{i = m + 1}^{n} \alpha_i \overline{u_i}$.

  Пользуемся тем, что фактор~--- линейное отображение, выкидываем младшие $\overline{u_i}$, так как они равны $0$ в факторе.
\end{proof}

\begin{theorem} $\dim U = \dim \Ker \vphi + \dim \Im \vphi$.
\end{theorem}

\begin{proof}
  Применим \hyperref[lm:factor-dimension]{лемму о размерности фактора} и \hyperref[thm:hom-theorem]{аналог теоремы о гомоморфизме}.
\end{proof}

\begin{definition} Определим ранг линейного отображения как размерность его образа.

  $\rank \vphi := \dim \Im \vphi$
\end{definition}

\Subsection{Суммы подпространств, прямые и не очень}

\begin{definition} Пусть $U, W \le V$. Тогда \emph{суммой подпространств} (или \emph{суммой Минковского}) называется множество всех попарных сумм;

  $U + W := \{u + w \mid u \in U, w \in W\}$
\end{definition}

\begin{exerc} $U + W \le V$,~~$U \cap W \le V$
\end{exerc}

\begin{remark} Введённое определение, а также выводы из него соответствуют аналогичному определению в теории идеалов.
  Там тоже множество всех попарных сумм идеалом являлось и пересечение идеалом являлось, а объединение~--- нет.
\end{remark}

\begin{remark} Сумма подпространств является коммутативной операцией: $V + U = U + V$
\end{remark}

\begin{lemma} Пусть $U, W, U_1, \ldots, U_n \le V$, тогда:
  \begin{enumerate}
  \item $\langle \cup\, U_i \rangle = U_1 + U_2 + \ldots + U_n$
  \item $U + W = U \iff W \le U$.
  \end{enumerate}
\end{lemma}

\begin{proof}
  \begin{enumerate}
  \item Более-менее аналогично предыдущим соответствующим теоремам. Можно доказать, показав два включения.

  \item
    Пусть $W \le U$, тогда $U + W \subseteq U$, при чём равенство достигается, например второе слагаемое можно сделать $0$.

    Пусть $U + W = U$, тогда $\forall w \in W$ $w = 0 + w \in U + W = U$. \qedhere
  \end{enumerate}
\end{proof}

\begin{definition} Подпространство $V$ является \emph{внутренней прямой суммой} подпространств $V_1, \ldots, V_m$ ($V = \oplus_i V_i$),
  если любой вектор $v \in V$ раскладывается единственным образом в сумму $v = v_1 + \ldots + v_m$, где $v_i \in V_i$.
\end{definition}

\begin{definition} \emph{Внешней прямой суммой} пространств $V_1, \ldots, V_m$ ($V = \oplus_i V_i$)
  называется множество кортежей $(v_1, \ldots, v_m)$ с операциями $+$ и $\cdot$ определёнными покомпонентно.
\end{definition}

\begin{remark} Хотя заданные операции отличаются по смыслу (например первое больше похоже на прямую сумму в теории групп, а второе на
  прямую сумму в теории колец (прямое произведение в группах), обозначим их одинаково.
\end{remark}

\begin{lemma} Следующие условия эквивалентны:

  \begin{enumerate}
  \item Сумма подпространств $U_1 + \ldots + U_n$ является прямой
  \item $0 = v_1 + \ldots + v_n \implies \forall i\colon v_i = 0$, где $v_i \in U_i$
  \item $\forall i\colon U_i \cap (U_1 + \ldots + U_{i-1} + U_{i + 1} + \ldots + U_{n}) = \{0\}$
  \end{enumerate}
\end{lemma}

\begin{consequence} Сумма $U + V$ является прямой $\iff$ $U \cap V = \{0\}$.
\end{consequence}

\begin{proof}
  \begin{enumerate}
  \item[$(1 \implies 2)$]
    В прямой сумме любой элемент представляется единственным образом.

    В том числе и ноль, при чём одно из представлений нуля мы знаем, значит других нет.
  \item[$(2 \implies 1)$]
    Ноль представляется единственным образом, но пусть у $x$ есть два представления:

    $x = x_1 + \ldots + x_n = y_1 + \ldots + y_n$

    Тогда $(x_1 - y_1) + \ldots + (x_n - y_n) = 0$, и значит каждое слагаемое равно нулю.
  \item[$(1 \implies 3)$]
    Пусть $W_i := U_1 + \ldots + U_{i-1} + U_{i+1} + \ldots + U_n$

    Если $x \in U_i \cap W_i$ и $x \ne 0$, то $x$ можно выразить двумя способами,
    через $U_i$ и через сумму составляющих $W_i$.

    Значит пересечение тривиально.

  \item[$(3 \implies 2)$]

    Пусть $0 = u_1 + \ldots + u_n$, где $u_i \in U_i$

    Пусть не все $u_i$ равны $0$, тогда $\exists k\colon u_{k} \ne 0$

    Тогда $u_k = - \sum\limits_{i \ne k} u_i$

    Тогда $u_k \in U_k \cap W_k$, при чём $u_k \ne 0$, противоречие. \qedhere
  \end{enumerate}
\end{proof}

В следующей теореме мы докажем, что внешняя прямая сумма, в некотором смысле, совпадает с внутренней.

В следующих утверждениях обозначим внутреннюю прямую сумму через $\oplus$, а внешнюю через $\oplus^e$.

\begin{statement} \begin{enumerate}\item Пусть $V_1, V_2, \ldots, V_n$~--- векторные пространства над $K$.

    Тогда отображения $\mu_i \colon V_i \to V_1 \oplus^e \ldots \oplus^e V_n$ ($v_i \mapsto (0,\ldots,0,v_i,0,\ldots,0)$)
    являются мономорфизмами.

  \item Если $V = V_1 \oplus^e \ldots \oplus^e V_n$, то прямая сумма $\mu_1(V_1) \oplus \ldots \oplus \mu_n(V_n)$
    изоморфна внешней (то есть $V$).
    
  \item Если $V = V_1 \oplus \ldots \oplus V_n$, тогда объединение базисов $V_1, \ldots, V_n$ является базисом $V$.

  \item $\dim (V_1 \oplus \ldots \oplus V_n) = \dim V_1 + \ldots + \dim V_n$.
  \end{enumerate}
\end{statement}

\begin{remark} После второго пункта внешняя прямая сумма это то же самое, что внутренняя сумма внутри пространства внешней,
  поэтому пункты \texttt{3} и \texttt{4} применимы как для внутренней суммы, так и для внешней.
\end{remark}

\begin{proof} \begin{enumerate}
  \item Очевидно (есть гомоморфизм, и он инъекция).
  \item Рассмотрим отображение $f\colon V_1 \oplus^e \ldots \oplus^e V_n \to \mu_1(V_1) \oplus \ldots \oplus \mu_n(V_n)$

    $f(v_1, \ldots, v_n) := \mu_1(v_1) + \ldots + \mu_n(v_n) = (v_1, \ldots, v_n)$

    Явно видно, что отображение оказалось тождественным.
  \item Пусть $\dim V_i = n_i$, и $V_i$ имеет базис $v_{i,1}, \ldots, v_{i,n_i}$.

    Проверим линейную независимость:

    $\sum_i (\sum_{j=1}^{n_i} \alpha_{i,j} v_{i,j}) = 0$, но $(\sum_{j=1}^{n_i} \alpha_{i,j} v_{i,j}) \in V_i$.

    Так как $V$~--- это прямая сумма, то это эквивалентно $\sum_{j=1}^{n_i} \alpha_{i,j} v_{i,j} = 0$ (для всех $i$), но это
    базис подпространства, поэтому $\alpha_{i,j} = 0$ для всех $i$, $j$.

    \bigskip

    Кроме того нужно заметить, что каждый элемент выражается, для этого достаточно разложить этот элемент
    в сумму элементов из $V_i$, а каждый из полученных элементов выразить через соответствующий базис.
  \item Следует из предыдущего пункта (мы даже явно построили базис). \qedhere
  \end{enumerate}
\end{proof}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Маркер вычитки

\begin{theorem}[Формула Грассмана] Пусть $U, W \le V$, тогда $\dim (U + W) = \dim U + \dim W - \dim U \cap W$.
\end{theorem}

\begin{remark} Равенство выше в некотором смысле напоминает формулу включений-исключений.
\end{remark}

\begin{proof} Рассмотрим линейное отображение $\vphi\colon U \oplus^e W \to V$ ($(u, w) \mapsto u + w$).

  Заметим, что $\vphi$~--- линейное отображение (убедитесь сами).

  $\Im \vphi = U + W$, $\Ker \vphi = \vphi^{-1}(0) = \{(u, -u) \mid u \in U, -u \in W\} = \{(u, -u) \mid u \in U \cap W\} \cong U \cap W$.

  Изоморфизм почти очевиден: $(u, -u) \mapsto u$

  \bigskip
  
  $\dim U \oplus^e W = \dim \Ker \vphi + \dim \Im \vphi = \dim (U \cap W) + \dim (U + W)$

  Но $\dim U \oplus^e W = \dim U + \dim W$, подставим выше и теорема доказана.
\end{proof}

\begin{remark} Обратите внимание, что мы пользуемся внешней прямой суммой, а не внутренней.

  Так как $U$ и $W$ произвольные пространства, то их внутренней суммы возможно не существует
  (если они, например, нетривиально пересекаются).
\end{remark}

\Subsection{Кольцо матриц}

\begin{definition} Матрица размером $n \times m$~--- это таблица из $n$ строк и $m$ столбцов, состоящая из элементов поля $K$.

  $\begin{pmatrix}
    a_{1,1} & a_{1,2} & \ldots & a_{1,m} \\
    a_{2,1} & a_{2,2} & \ldots & a_{2,m} \\
     \vdots & \vdots  & \ddots &  \vdots \\
    a_{n,1} & a_{n,2} & \ldots  & a_{n,m}
  \end{pmatrix}$
\end{definition}

\begin{definition} Обозначается множество матриц как $K^{n \times m}$ или $\mathrm{Mat}_{n \times m}(K) = \mathrm{Mat}(n, m, K)$.

  В последней записи если $n = m$, то второй параметр можно опустить.
\end{definition}

\begin{remark} Обратите внимание на порядок: сначала идёт число строк, затем число столбцов.
\end{remark}

\begin{definition} Матрицы одинаковых размеров можно складывать и домножать на скаляр:

  % #ЯСТРАШНЫЙ
  
  $$\begin{pmatrix}
    a_{1,1} & a_{1,2} & \ldots & a_{1,m} \\
    a_{2,1} & a_{2,2} & \ldots & a_{2,m} \\
     \vdots & \vdots  & \ddots &  \vdots \\
    a_{n,1} & a_{n,2} & \ldots  & a_{n,m}
  \end{pmatrix} + \begin{pmatrix}
    b_{1,1} & b_{1,2} & \ldots & b_{1,m} \\
    b_{2,1} & b_{2,2} & \ldots & b_{2,m} \\
    \vdots & \vdots  & \ddots &  \vdots \\
    b_{n,1} & b_{n,2} & \ldots  & b_{n,m}
  \end{pmatrix} := \begin{pmatrix}
    a_{1,1} + b_{1,1} & a_{1,2} + b_{1,2} & \ldots & a_{1,m} + b_{1,m} \\
    a_{2,1} + b_{2,1} & a_{2,2} + b_{2,2} & \ldots & a_{2,m} + b_{2,m} \\
     \vdots & \vdots  & \ddots &  \vdots \\
    a_{n,1} + b_{n,1} & a_{n,2} + b_{n,2} & \ldots & a_{n,m} + b_{n,m}

  \end{pmatrix}$$

  $$\alpha  \begin{pmatrix}
    b_{1,1} & b_{1,2} & \ldots & b_{1,m} \\
    b_{2,1} & b_{2,2} & \ldots & b_{2,m} \\
     \vdots & \vdots  & \ddots &  \vdots \\
    b_{n,1} & b_{n,2} & \ldots  & b_{n,m}
  \end{pmatrix} := \begin{pmatrix}
    \alpha b_{1,1} & \alpha b_{1,2} & \ldots & \alpha b_{1,m} \\
    \alpha b_{2,1} & \alpha b_{2,2} & \ldots & \alpha b_{2,m} \\
     \vdots & \vdots  & \ddots &  \vdots \\
    \alpha b_{n,1} & \alpha b_{n,2} & \ldots  & \alpha b_{n,m}
  \end{pmatrix}$$
\end{definition}

\begin{lemma} Множество матриц с размером $n \times m$ является векторным пространством размерности $nm$.
\end{lemma}

\noindent{}Проверка \hyperref[def:vectorspace]{всех условий} векторного пространства остаётся в качестве упражнения.

\begin{definition}
  \emph{Стандартным} базисом назовём множество матриц, в которых ровно один ненулевой элемент~--- единица в некотором месте.

  $A = \sum_{i,j} a_{i, j} e_{i, j}$
\end{definition}

\begin{definition} Пусть $A \in K^{n \times m}$, $B \in K^{m \times k}$

  $AB := C$, где $C \in K^{n \times k}$, а $c_{i,j} = \sum_{l=1}^m a_{i,l} b_{l,j}$   
\end{definition}

\begin{remark} Такое определение умножения матриц может показаться странным на первый взгляд.

  На самом деле матрица $A$ размером $n \times m$ задаёт линейное отображение из $K^m$ в $K^n$:

  $f(x) := Ax$.

  Можно убедиться (расписав формулы), что матрице композиции отображений соответствует матрица, получающаяся именно таким
  умножением, иначе говоря $(BA)x = B(Ax)$ для любого вектора $x$.
\end{remark}

\begin{remark} Если записать вторую матрицу в виде столбцов $B = (b_1 \mid b_2 \mid \ldots \mid b_k)$, ($b_i \in K^m$),
  то $AB = (A b_1 \mid A b_2 \mid \ldots \mid A b_k)$.
\end{remark}

\begin{remark} Умножение матриц ассоциативно, так как операция композиции отображений является ассоциативной.

  Более формально, скомбинируем два предыдущих замечания:

  Мы знаем, что $(BA)x = B(Ax)$, где $x$~--- любой вектор.

  Покажем, что $(BA)C = B(AC)$ для любой матрицы $C$.

  Представим $C = (c_1 \mid \ldots \mid c_k)$ в столбцовой записи и подставим:

  $(BA)(c_1 \mid \ldots \mid c_k) = ((BA)c_1 \mid \ldots \mid (BA)c_k) = (B(A c_1) \mid \ldots \mid B(A c_k)) = B(A c_1 \mid A c_2 \mid \ldots \mid A c_k) = B(AC)$
\end{remark}

\begin{lemma} $K^{n \times n}$~--- кольцо с единицей.
\end{lemma}

\begin{proof}
  \begin{enumerate}
  \item $E_4 := \begin{pmatrix}
    1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1
  \end{pmatrix}$

  Подставьте в определение и убедитесь, аналогично для всех остальных $n$.

  \item Ассоциативность доказана в замечании.
    
  \item $A(B + C) = AB + AC$, так как матрица~--- линейное отображение.

  \item $(A + B)C = AC + BC$, несложно убедиться из определения. \qedhere
  \end{enumerate}
\end{proof}

\begin{remark} В этом кольце (для $n \ge 2$) есть нильпотенты (и тем более делители нуля):

  $\begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix} \begin{pmatrix} 0 & 1 \\ 0 & 0 \end{pmatrix} = \begin{pmatrix} 0 & 0 \\ 0 & 0 \end{pmatrix}$
\end{remark}

\begin{remark} Умножение матриц не коммутативно (композиция отображений не является коммутативной)

  Более того, если матрицы не квадратные, то при существовании произведения $AB$ произведение $BA$ вообще может не существовать.
\end{remark}

\begin{lemma} Пусть $V$~--- векторное пространство над $K$, $e = (e_1, e_2, \ldots, e_n)$~--- базис $V$.

  Введём $\vphi_e \colon V \to K^n$, это отображение переводит элемент $V$ в столбец его коэффициентов при разложении по базису.

  Тогда $\vphi_e$~--- линейное отображение и вообще изоморфизм.
\end{lemma}

\begin{proof} Линейность очевидна, сюръективность тоже (по набору коэффициентов можно построить нужный вектор), также ядро тривиально
  (так как $e$~--- базис, то только тривиальная линейная комбинация равна нулю).
\end{proof}

\begin{consequence} Пусть $V$~--- конечномерное пространство, тогда $V \cong K^{\dim V}$.

  В частности, все $V$ c одинаковой конечным $\dim V$ изоморфны друг другу (так как они все изоморфны $K^{\dim V}$).
\end{consequence}

\begin{remark} Построенное отображение, вообще-то говоря, зависит от выбора базиса $e$.

  Более того, выбирая разные базисы и отображения получатся разные (смотрите далее).
\end{remark}
