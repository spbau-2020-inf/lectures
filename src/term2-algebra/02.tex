\begin{lemma} (О замене)

$B$ -- базис векторного пространства $V$ над полем $K$. (То есть $\langle B \rangle = V$ и $B$ линейно независимо.)

Пусть вектор $u \in B$, вектор $v \in V$, но $v \not\in \langle B \setminus \{u\} \rangle$.

Тогда $B \setminus \{ u \} \cup \{ v \}$ -- базис $V$.

\begin{proof}
  Докажем по определению два пункта.
  \begin{enumerate}
    \item Множество векторов $B \setminus \{ u \} \cup \{ v \}$ является системой образующих пространства $V$. Чтобы доказать это, достаточно показать, что можно выразить вектор $u$ через вектора $B \setminus \{u\} \cup \{v\}$.
    
    $v = \sum \alpha_i b_i + \alpha u$, $b_i \in B \setminus \{u\}$,
    $\alpha, \alpha_i \in K$.
    
    $\alpha \neq 0$, так как $v \not\in \langle B \setminus \{u\} \rangle$.
    
    $u = \frac{1}{\alpha} v - \sum \frac{\alpha_i}{\alpha} b_i$
    $\in B \setminus \{ u \} \cup \{ v \}$.
    
    $V = \langle B \rangle \subset B \setminus \langle \{ u \} \cup \{ v \} \rangle$.
    
    \item $B \setminus \{ u \} \cup \{ v \}$ линейно независимо.
    
    $\beta v + \sum \beta_i v_i = 0$, $v_i \in B \setminus \{u\}$
    
    Подставим значение $v$:
    $\beta \sum \alpha_i b_i + \beta \alpha u + \sum \beta_i v_i = 0$
    
    Коэффициент $\beta \alpha$ перед $u$ должен равняться нулю, так как в двух суммах нет векторов,
    которые могли бы сократиться с $u$. Но $\alpha \neq 0$ $\Rightarrow \beta = 0$.    
    $\Rightarrow \sum \beta_i v_i = 0 \Rightarrow \beta_i = 0 \,\,\, \forall i$.
    Значит, действительно, для получения нуля подходит только тривиальная комбинация. \qedhere
  \end{enumerate}
\end{proof}
\end{lemma}

\begin{theorem} Любые два базиса равномощны.
  \begin{proof}
    Пусть $V$ -- конечномерное пространство. $B, C$ -- базисы пространства $V$, $|B| > |C| = n$.
    
    $B$ -- минимальная система образующих $\Rightarrow$
    $B \setminus \{b_1\}$ -- не является системой образующих.
    
    Если $C \subset \langle B \setminus \{b_1\} \rangle$, значит
    $V = \langle C \rangle \subset \langle B \setminus \{b_1\} \rangle \neq V$. Противоречие.
        
    Поясним. $C \subset \langle B \setminus \{b_1\} \rangle$, значит в $\langle B \setminus \{b_1\} \rangle$
    также лежат все комбинации векторов из $C$, $C$ -- базис $\Rightarrow$ $\langle B \setminus \{b_1\} \rangle = V$,
    то есть $B \setminus \{b_1\}$ -- базис, но тогда $B$ не базис (так как не минимально). Противоречие.
    
    Значит, $C \not\subset \langle B \setminus \{b_1\} \rangle$ $\Rightarrow$
    $\exists c_1 \not\in \langle B \setminus \{b_1\} \rangle$ $\Rightarrow$
    по лемме о замене $B \setminus \{b_1\} \cup \{c_1\}$ -- базис.
    $|B \setminus \{b_1\} \cup \{c_1\}| = |B| > |C|$.
    
    Продолжим выкидывать и добавлять векторы: $B \setminus \{b_1\} \cup \{c_1\} \setminus \{b_2\}$ и т.д.
    
    $B_1 = B \setminus \{b_1\} \cup \{c_1\} ... \setminus \{b_n\} \cup \{ c_n\}$.
    $|B_1| = |B|$, $C \subset B_1$. Однако раз $C$ -- базис и $B_1$ -- базис,
    то в $B_1$ не должны содержаться никакие другие лишние вектора, помимо векторов базиса $C$
    (так как базис -- минимальная система образующих), которые там обязательно
    присутствуют по построению. Значит, $|B_1| = |C|$ $\Rightarrow$ $|B| = |C|$.
    
    \begin{remark}
      Векторы $c_i$ не совпадают, поэтому после каждой итерации мощность $B$ не меняется.
    \end{remark}
  \end{proof}
\end{theorem}

\begin{remark} Теорема верна и в бесконечномерном случае (без доказательства).
\end{remark}

\begin{definition} $\dim_K V$ -- размерность пространства $V$ над полем $K$, то есть мощность базиса. Иногда индекс $K$ опускается.
\end{definition}

\begin{example}
  \begin{enumerate}
    \item $K^n$ -- пространство столбцов длины $n$. $\dim K^n = n$.
    
   \[
   e_1 = \begin{pmatrix}
	  1\\
	  0 \\
	  \vdots \\
	  0
	  \end{pmatrix}, e_2 = \begin{pmatrix}
	  0\\
	  1 \\
	  \vdots \\
	  0
	  \end{pmatrix}, \ldots, e_n = \begin{pmatrix}
	  0\\
	  0 \\
	  \vdots \\
	  1
	  \end{pmatrix}
	  \]

          Назовём столбцы $\{e_1, ..., e_n\}$ стандартным базисом $K^n$ (В столбце $e_i$ на $i$-том месте стоит 1, на остальных местах~--- 0).
          Это действительно базис: 

	  \[\sum_{i=1}^n \alpha_i e_i = \begin{pmatrix}
	  \alpha_1 \\
	  \alpha_2 \\
	  \vdots \\
	  \alpha_n
	  \end{pmatrix}.\]
	
	  \item ${}^n K$ -- пространство строчек длины $n$, $\dim {}^n K = n$.
	  \item Пространство $\CC$ над полем $\R$. $\dim_\R \CC = 2$. Пример базиса: $\{1, i \}$.
	  \item Пространство $\R$ над полем $\Q$. $\dim_\Q \R = \infty$.
	  \item $K[X]$. Базис: $1, x, x^2, x^3$ и т.д. $\dim K[X] = \infty$
	  \item $C[0, 1]$ -- функции, непрерывные на отрезке $[0, 1]$. $\dim C[0, 1] = \infty$.
  \end{enumerate}
\end{example}

\begin{definition} Линейное отображение, или линейный оператор, или гомоморфизм пространств.
  $V, U$ -- векторные поля над $K$.

  $\vphi : V \rightarrow U$ называется линейным отображением, если выполняются следующие два условия:
  \begin{enumerate}
    \item $\vphi(v_1 + v_2) = \vphi(v_1) + \vphi(v_2)$ $\forall v_1, v_2 \in V$.
    \item $\vphi(\alpha v) = \alpha \vphi(v)$ $\forall v \in V, \alpha \in K$.
  \end{enumerate}
  
  \textbf{Свойства.}
  
  \begin{enumerate}
    \item $\vphi(0) = 0$.
    \item $\vphi(-v) = -\vphi(v)$.
    \item $\vphi(u - v) = \vphi(u) - \vphi(v)$.
  \end{enumerate}
  
  \begin{remark} Линейное отображение аналогично гомоморфизму колец, доказательство свойств аналогично.
  \end{remark}
\end{definition}

\begin{example} Линейные отображения.
  \begin{enumerate}
    \item Поворот плоскости на угол $\alpha$, $R^\alpha : \R^2 \rightarrow \R^2$.
    \item Проекция точки на ось $OX$, $f_x : \R^2 \rightarrow \R$, $(a, b) \mapsto a$.
    \item Производная, $D : K[x] \rightarrow K[x]$.
  \end{enumerate}
\end{example}

\begin{definition}
  $\vphi, \psi : V \rightarrow V$ -- эндоморфизмы (линейные отображения из себя в себя).
  
  \begin{remark} Не путать эндоморфизм с автоморфизмом. Авторморфизм -- это биективный эндоморфизм.
  \end{remark}
  
  \textbf{Свойства.}
  \begin{enumerate}
    \item $\vphi \circ \psi$ -- линейное отображение.
    \item $(\vphi + \psi)(v) = \vphi(v) + \psi(v)$.
    \item $(\alpha \psi)(v) = \alpha \psi(v), \alpha \in K$.
  \end{enumerate}
\end{definition}

\begin{definition}
  $V$ -- векторное пространство над K, $V$ -- кольцо относительно той же операции сложения.
  
  $\alpha (ab) = (\alpha a)b \,\,\, \forall \alpha \in K, a,b \in V$.
  
  Тогда $V$ -- алгебра над $K$.
\end{definition}

\begin{definition} $\End V$ -- алгебра эндоморфизмов пространства $V$.
\end{definition}

\begin{definition} $V$ -- векторное пространство над $K$, $U \le V$.

  Фактор-пространство $V{/}_U$ -- векторное пространство, множество элементов фактор-пространства $V{/}_U$ совпадает
  с множеством элементов фактор-группы $V{/}_U$ абелевых групп $(V, +)$ и $(U, +)$.
  
  Элементы фактор-пространства имеют вид $\{x + U\}$.
  Сложение уже определено в фактор-группе ($(a + U) + (b + U) = (a + b) + U$), определим умножение:  
  $\alpha (x + U) = \alpha x + U, \alpha \in K, x \in V$.
\end{definition}

\begin{definition} $\vphi : U \rightarrow V$ -- линейное отображение.

$\Ker \vphi = \{u \in U \mid \vphi(u) = 0\}$

$\Im \vphi = \{v \in V \mid \vphi(u) = v, u \in U\}$

(Определения такие же, как в теориях групп и колец.)
\end{definition}

\exersize Найти ядро и образ в отображениях из примеров.

\begin{enumerate}
  \item Поворот плоскости на угол $\alpha$, $R^\alpha : \R^2 \rightarrow \R^2$.
  
  $\Ker R^\alpha = \{0\}$, $\Im R^\alpha = \R^2$.
  
  \item Проекция точки на ось $OX$, $f_x : \R^2 \rightarrow \R$, $(a, b) \mapsto a$.
  
  $\Ker f_x = \{(0, b)\}$, $\Im f_x = \R$ -- вся прямая $OX$.
  
  \item Производная, $D : K[x] \rightarrow K[x]$.
  
  $\Ker D = K$ -- все элементы поля $K$, то есть константы. $\Im D = K[X]$.
\end{enumerate}

\begin{remark} $\vphi : U \rightarrow V$ -- линейное отображение.
\begin{enumerate}
  \item $\vphi$ -- инъективно $\Leftrightarrow$ $\Ker \vphi = \{0\}$.
  \item $\forall b \in V$ множество решений уравнения $\vphi(x) = b$, то есть $\vphi^{-1}(b)$,
  имеет вид $a + \Ker \vphi$, где $\vphi(a) = b$.
\end{enumerate}
\end{remark}

\begin{theorem} (О гомоморфизме)

  $U, V$ -- векторное пространство, $\vphi : U \rightarrow V$ -- линейное отображение.
  
  Тогда $U{/}_{\Ker \vphi} \cong \Im \vphi$.
\end{theorem}

\begin{remark} Пусть $U, W \le V$. Тогда

  $U \cap W \le V$.
  
  $U + W = \{u + w \mid u \in U, w \in W\} \le V$, причём иногда $\neq V$.
  
  (Например, $\R + \R \le \R^3$, $\{(a, 0)\} + \{(0, b)\}$ порождает плоскость, подпространство в $R^3$.)
  
  $\{U \cup W\} = U + W$.
\end{remark}

\begin{remark} Наши следующие вопросы:

  $\dim V{/}_U$~--- ?
  
  $\dim (U + W)$~--- ?
\end{remark}
