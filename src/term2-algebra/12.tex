\Subsection{Факториальность кольца многочленов над факториальным кольцом}

\begin{definition}
	\slashns
	
	$f = a_0 + a_1x + \ldots + a_nx^n \in R[x]$
	
	Если $d(f) = \gcd(a_0, a_1, \ldots, a_n) = 1$, то $f$ называется примитивным.
\end{definition}

\begin{remark}
	\slashns
	
	Вспомним определение ассоциированности.
	
	А именно, пусть $a, \, b \in R$, где $R$~--- область целостности.
	
	Тогда элементы $a$ и $b$ ассоциированны (или $a \sim b$), если $a = \eps b, \; \eps \in R^*$.
\end{remark}

\begin{remark}
	\slashns
	
	В евклидовых кольцах $\gcd$ определён с точностью до делителей единицы.
\end{remark}

\begin{lemma}
	\slashns
	
	$f, \, g \in R[x]$
	
	$\begin{rcases}
	\alpha f = \beta g\;\;\;\;\;\;\;\;\;\;\;\\
	d(f) = d(g) = 1\\
	\alpha, \, \beta \in R\setminus\{0\}\;\;
	\end{rcases} \implies \begin{array}{c}
	\alpha \sim \beta \text{ в } R\;\;\;\\
	f \sim g \text{ в } R[x]
	\end{array}$
	
	\begin{proof}
		\slashns
		
		Покажем, что $\alpha \sim \beta$:
		
		$f = a_0 + a_1x + \ldots + a_nx^n, \;\; g = b_0 + b_1x + \ldots + b_mx^m$
		
		$\alpha f = \beta g, \;\; \alpha, \, \beta \in R \setminus \{0\} \implies \begin{cases}
		n = m\\
		\alpha a_i = \beta b_i \;\;\; \forall \, i = 0 \,..\, n
		\end{cases}$
		
		$\gcd(a_0, a_1, \ldots, a_n) = 1, \;\; \gcd(b_0, b_1, \ldots, b_n) = 1$
		
		$\gcd(\alpha a_0, \alpha a_1, \ldots, \alpha a_n) = \alpha \sim \beta = \gcd(\beta b_0, \beta b_1, \ldots, \beta b_n)$
		
		Покажем теперь, что $f \sim g$:
		
		$\alpha = \eps \beta, \; \eps \in R^*$
		
		$\begin{rcases}
		\alpha f = \eps \beta f, \;\; \alpha f = \beta g\\
		\beta \ne 0
		\end{rcases} \implies \eps \beta f - \beta g = \beta(\eps f - g) = 0 \implies \eps f = g$, т.е. $f \sim g$
	\end{proof}
\end{lemma}

\begin{theorem}
	\slashns
	
	$R$~--- факториально $\implies$ $R[x]$~--- факториально.
	
	\begin{proof}
		\slashns
		
		$f \in R[x], \; f \ne 0$
		
		Покажем существование разложения на неприводимые. Для этого воспользуемся индукцией по $\deg f$.
		
		База: $\deg f = 0, \;\; f(x) = c \in R \setminus \{0\} \implies$ существует разложение на неприводимые.
		
		Переход: $\deg f > 0$.
		
		$f = d(f) \cdot f_1$, где $d(f) \in R \setminus \{0\}$
		
		Если $f_1$ неприводим, то уже получили искомое разложение.
		
		Иначе же получаем, что существует представление $f_1 = g_1 \cdot h_1$, где $g_1$ и $h_1$~--- раскладываются на неприводимые по предположению индукции.
		
		Покажем теперь единственность разложения.
		
		$f = p_1 p_2 \ldots p_k \, \cdot \, q_1 q_2 \ldots q_m = p_1'p_2' \ldots p_l' \, \cdot \, q_1' q_2' \ldots q_n'$,~~где~~$\begin{array}{l}
		p_i, \, p_i' \in R \text{ --- неприводимые коэффициенты}\\
		q_i, \, q_i' \in R[x] \text{ --- неприводимые многочлены}
		\end{array}$
		
		По доказанной выше лемме, $p_1 p_2 \ldots p_k \sim p_1' p_2' \ldots p_l', \;\; q_1 q_2 \ldots q_m \sim q_1' q_2' \ldots q_n'$.
		
		Уже знаем, что разложение над факториальным кольцом на неприводимые множители единственно. То есть, $k = l$, и существует такая перестановка $\tau_1 \in S_k$, что $p_i \sim p_{\tau_1(i)}'$.
		
		Ещё нам известно, что $q_i$ и $q_i'$~--- неприводимы в $R[x]$. Следовательно, по следствию из леммы Гаусса, они также неприводимы и в $(\Quot R)[x]$ (которое факториально) $\implies n = m$ и $q_i \sim q_{\tau_2(i)}'$ для некоторой перестановки $\tau_2 \in S_n$.
		
		Таким образом, показали, что $R[x]$~--- факториально.
	\end{proof}
\end{theorem}

\Subsection{Многочлены от многих переменных}

\begin{definition}
	\slashns
	
	Многочленом от многих переменных называется конечная сумма мономов вида
	
	$f(x_1, x_2, \ldots, x_n) = \sum a_{k_1, k_2, \ldots, k_n} x_1^{k_1} x_2^{k_2} \ldots x_n^{k_n}$
\end{definition}

\begin{definition}
	\slashns
	
	$R[x_1, x_2] := R[x_1][x_2]$
	
	$R[x_1, x_2, \ldots, x_n] := R[x_1, x_2, \ldots, x_{n-1}][x_n]$
	
	В частности, если $R$~--- факториально, то и $R[x_1, x_2, \ldots, x_n]$ также будет факториально.
\end{definition}

\begin{definition}
	\slashns
	
	Назовём многочлен однородным степени $d$, если в нём нет мономов степени, отличной от $d$.
\end{definition}

\begin{remark}
	\slashns
	
	\begin{enumerate}
		\item
		Пусть $R$~--- поле.
		
		Однородные многочлены степени $d$ образуют подпространство $R^{(d)}[x_1, x_2, \ldots, x_n]$ размерности $\binom{n+d-1}{d}$.
		
		\item
		$R[x_1, x_2, \ldots, x_n] = \bigoplus\limits_{d \ge 0} R^{(d)}[x_1, x_2, \ldots, x_n]$.
	\end{enumerate}
\end{remark}

\begin{denotation}
	\slashns
	
	$R$~--- область целостности.
	
	$f, \; g \in R[x]$~--- многочлены.
	
	$f = g$, если они совпадают покоэффициентно (формальное равенство).
	
	$f \equiv g$, если $\forall \, \lambda \in R: f(\lambda) = g(\lambda)$ (функциональное равенство).
\end{denotation}

\begin{theorem}
	\slashns
	
	$R$~--- поле, $\abs{R} = \infty$.

	$f, \; g \in R[x]$~--- многочлены.
	
	Тогда $f \equiv g \iff f = g$.
	
	\begin{proof}
		\slashns
		
		Положим $f(x) = a_0 + a_1 x + \ldots + a_n x^n, \; g(x) = b_0 + b_1 x + \ldots + b_m x^m$.
		
		``$\Longleftarrow$''  Пусть $f = g$.
		
		Тогда $n = m$ и $\forall \, k = 0..n: a_k = b_k$.
		
		Очевидно, что в таком случае $\forall \, \lambda \in R: f(\lambda) = g(\lambda)$, то есть $f \equiv g$.
		
		``$\Longrightarrow$'' Пусть $f \equiv g$.
		
		Рассмотрим многочлен $h = f - g \in R[x]$.
		
		Положим $h = c_0 + c_1 x + \ldots + c_l x^l$, где $l = \max\{n, m\}$.
		
		Мы знаем, что $\forall \, \lambda \in R: f(\lambda) = g(\lambda) \implies \forall \, \lambda \in R: h(\lambda) = f(\lambda) - g(\lambda) = 0$.
		
		Вспомним, что любой отличный от нуля многочлен степени $l$ не может иметь более $l$ корней.
		
		Но многочлен $h$ имеет бесконечное количество корней. Следовательно, он тождественно равен нулю.
		
		Таким образом, $h = 0 \implies f - g = 0 \implies f = g$.
	\end{proof}
\end{theorem}

\begin{consequence}
	\slashns
	
	$R$~--- поле, $\abs{R} = \infty$.
	
	$f, \; g \in R[x_1, x_2, \ldots, x_n]$~--- многочлены.
	
	Тогда $f \equiv g \iff f = g$.
	
	\begin{proof}
		\slashns
		
		Положим
		$$f(x_1, x_2, \ldots, x_n) = \sum\limits_{k=1}^{s} a_k \, x_1^{p_{k, 1}} x_2^{p_{k, 2}} \ldots x_n^{p_{k, n}}$$
		$$g(x_1, x_2, \ldots, x_n) = \sum\limits_{k=1}^{t} b_k \, x_1^{q_{k, 1}} x_2^{q_{k, 2}} \ldots x_n^{q_{k, n}}$$

		Если $f = g$, то функциональное равенство очевидно. Потому покажем следствие в другую сторону.
		
		Воспользуемся индукцией по числу переменных.
		
		Для одной переменной всё знаем. Рассмотрим случай $n$ переменных, где $n > 1$.
		
		Рассмотрим переменную $x_n$ в многочленах $f$ и $g$. Разобьём мономы $f$ и $g$ на группы в зависимости от того, в какой степени входит $x_n$ в тот или иной моном.
		
		Пусть $m$~--- максимальная из степеней $x_n$ в многочленах $f$ и $g$. Таким образом, можем получить следующее представление данных многочленов:
		$$f(x_1, x_2, \ldots, x_n) = \sum\limits_{k=0}^{m} f_k(x_1, x_2, \ldots, x_{n-1}) \, x_n^k$$
		$$g(x_1, x_2, \ldots, x_n) = \sum\limits_{k=0}^{m} g_k(x_1, x_2, \ldots, x_{n-1}) \, x_n^k$$
		
		Мы знаем, что такие многочлены функционально равны.
		
		Следовательно, $\forall \, x_1, x_2, \ldots x_{n-1} \in R \;\;\; \forall k = 0..m: f_k(x_1, x_2, \ldots x_{n-1}) = g_k(x_1, x_2, \ldots, x_{n-1})$.
		
		Однако, по предположению индукции, $f_k \equiv g_k \implies f_k = g_k$.
		
		Значит, $f \equiv g \implies f = g$.
	\end{proof}
\end{consequence}

\Subsection{Теорема Гильберта о базисе}

\begin{theorem}
	\slashns
	
	Пусть в $R$ любой идеал конечнопорождён.
	
	Тогда и в $R[x_1, x_2, \ldots, x_n]$~--- тоже.
	
	\begin{proof}
		\slashns
		
		Достаточно доказать для $R[x]$.
		
		Пусть $I$~--- произвольный идеал, $I \subseteq R[x]$.
		
		Рассмотрим множество старших коэффициентов многочленов из $I$. Обозначим его за $J$.
		
		Заметим, что $J$~--- идеал в $R$. Действительно:
		
		$f(x) = ax^n + \ldots \in I, \; g(x) = bx^m + \ldots \in I \implies\\\hspace*{150pt}\implies x^m f(x) + x^n g(x) = (a+b) x^{n+m} + \ldots \in I \implies a + b \in J$
		
		$f(x) = ax^n + \ldots \in I \implies cf(x) = cax^n + \ldots \in I \implies ca \in J$
		
		По предположению идеал $J$ конечнопорождён в $R$. Значит, существует некоторый набор его образующих $a_1, a_2, \ldots, a_s$.
		
		Рассмотрим произвольный набор $f_1, f_2, \ldots, f_s \in I$, такой что $f_i(x) = a_ix^{m_i} + \ldots$
		
		Положим $n = \max\limits_{i=1..s} \deg f_i$.
		
		Если $n = 0$, то заметим, что $a_1, a_2, \ldots, a_s$ также являются образующими и идеала $I$. А потому будем рассматривать случай $n > 0$.
		
		Покажем, что $\forall \, f \in I \;\; \exists \, \lambda_1, \lambda_2, \ldots, \lambda_s \in R[x], \; g \in I, \; \deg g < n: f(x) = g(x) + \sum\limits_{i=1..s} \lambda_i f_i$.
		
		Воспользуемся индукцией по $\deg f$.
		
		Если $\deg f < n$, то можем выбрать $g = f$ и $\lambda_i = 0$.
		
		Пусть $\deg f \ge n, \; f(x) = b_Nx^N + \ldots$
		
		$b_N \in J \implies b_N = \sum\limits_{i=1..s} \alpha_i a_i, \; \alpha_i \in R$
		
		Тогда выберем $g(x) = f(x) - \sum\limits_{i=1..s} \alpha_i x^{N - m_i}f_i(x)$ и $\lambda_i = \alpha_i x^{N - m_i} \implies \deg g < N$. И воспользуемся предположением индукции.
		
		Осталось показать, что $\exists$ конечный набор многочленов $h_i \in I$, что $\forall \, g \in I, \, \deg g < n$ представим в виде линейной комбинации $h_i$.

		Положим $J_{n-1}$~--- идеал, состоящий из старших членов коэффициентов многочленов из $I$ степени $n-1$. Любой такой идеал конечнопорождён $\implies J_{n-1} = \langle b_{1}, b_{2}, \ldots, b_{t} \rangle$.
		
		Следовательно, можем выбрать такой набор $g_1, g_2, \ldots, g_t \in I$, что $g_i(x) = b_i x^{n-1} + \ldots$
		
		Осталось заметить, что $\forall \, g \in I, \; \deg g = n - 1$ существует представление в виде
		
		$g(x) = \widetilde{g}(x) + \sum\limits_{i=1..t} \mu_i g_i(x)$, где $\mu_i \in R$ и $\deg \widetilde{g}(x) < n - 1$
		
		Получили сведение задачи от степени $n - 1$ к степени $n - 2$.
		
		Построив теперь аналогичную конструкцию для многочленов степени $n - 2, n - 3, \ldots, 0$, найдём оставшиеся образующие.
		
	\end{proof}
\end{theorem}