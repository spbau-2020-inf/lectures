\Subsection{Связь линейных операторов с матрицами}

$V \simeq K^n, \;\; n = \dim V$

Уже знаем, что такое гомоморфизм в векторном пространстве.

$K^n \simeq U \to V \simeq K^m$

Соответственно, каждому гомоморфизму в векторном пространстве будет соответствовать гомоморфизм в пространстве столбцов.

\begin{denotation}
	\slashns
	
	$e = (e_1, \, e_2, \, \ldots, \, e_n)$~--- базис $V$.
	
	$\forall \, x \in V: x = \sum_{i = 1}^{n} \alpha_i e_i = (e_1, \, e_2, \, \ldots, \, e_n) \cdot \begin{pmatrix}\alpha_1\\\alpha_2\\\ldots\\\alpha_n\end{pmatrix}$
	
	$x^e = \begin{pmatrix}\alpha_1\\\alpha_2\\\ldots\\\alpha_n\end{pmatrix}; \;\;\; x = ex^e$
\end{denotation}

\begin{lemma}
	\slashns
	
	Линейное отображение однозначно определяется образом базисных векторов.
	
	Переформулировка:
	
	$\begin{cases}
		e = (e_1, \, \ldots, \, e_n) \text{ --- базис пространства } U\\
		f = (f_1, \, \ldots, \, f_n), \;\; f_i \in V
	\end{cases}$
	
	Тогда $\exists$ единственное отображение $\phi: U \to V$, такое что $\phi(e_i) = f_i, \;\; 1 \le i \le n$
	
	\begin{proof}
		\slashns
		
		Единственность.
		
		Если $\phi: U \to V$~--- линейное, то $\phi(u) = \phi\left(\sum_{i=1}^{n} \alpha_i e_i\right) = \sum_{i=1}^{n} \alpha_i \phi(e_i) = \sum_{i=1}^{n} \alpha_i f_i$.
		
		Отображение $U \to V, \;\;\; \sum_{i=1}^{n} \alpha_i e_i \mapsto \sum_{i=1}^{n} \alpha_i f_i$ является линейным.
	\end{proof}
\end{lemma}

\begin{lemma}
	\slashns
	
	$e$~--- базис $U$.
	
	$\phi: U \to V$~--- линейное отображение.
	
	$\phi$~--- инъективно $\iff$ $\phi(e_1), \, \ldots, \, \phi(e_n)$~--- линейно независимы.
	
	$\phi$~--- сюръективно $\iff$ $\phi(e_1), \, \ldots, \, \phi(e_n)$~--- система образующих.
	
	$\phi$~--- биекция $\iff$ $\phi(e_1), \, \ldots, \, \phi(e_n)$~--- базис.
	
	\begin{proof}
		\slashns
		
		1) $\phi(e_1), \, \ldots, \, \phi(e_n)$~--- линейно зависимы $\iff \exists \, \alpha_1, \, \ldots, \, \alpha_n \in K$, не все равные 0.
		
		$\sum_{i=1}^{n} \alpha_i \phi(e_i) = 0 \iff \phi\left(\sum_{i=1}^{n} \alpha_i e_i\right) = 0$ (в силу линейности $\phi$).
		
		Следовательно, $\phi$ не инъективно, поскольку ядро нетривиально.
		
		$\phi(e_1), \, \ldots, \, \phi(e_n)$~--- линейно независимы $\implies \big(\sum_{i=1}^{n} \alpha_i \phi(e_i) = 0 \iff \forall \, \alpha_i = 0\big)$. 
		
		Следовательно, $\phi$~--- инъективно.
		
		2) $\Im \phi = \{\phi(u): u \in U\} = \{\phi\left(\sum_{i=1}^{n} \alpha_i e_i\right): \alpha_i \in K\} = \{\sum_{i=1}^{n} \alpha_i \phi(e_i): \alpha_i \in K\} = \langle\phi(e_1), \, \ldots, \, \phi(e_n)\rangle$
		
		3) 1 + 2
	\end{proof}
\end{lemma}

\begin{consequence}
	\slashns
	
	Два конечномерных пространства изоморфны $\iff$ они имеют одинаковые размерности.
\end{consequence}

\begin{denotation}
	\slashns
	
	$e = (e_1, \, \ldots, \, e_n)$~--- базис $U$.
	
	$f = (f_1, \, \ldots, \, f_n)$~--- базис $V$.
	
	$\phi: U \to V$~--- линейное отображение.
	
	$\phi$ однозначно определяется матрицей.
	
	$\phi_e^f = \phi_{e \to f} = \left(\phi(e_1)^f, \, \ldots, \, \phi(e_n)^f\right)$
\end{denotation}


\begin{denotation}
	\slashns

	$u_1, \, \ldots, \, u_n \in U, \;\;\; \phi: U \to V$~--- линейное отображение

	$u = (u_1, \, \ldots, \, u_n)$

	$\phi(u) = \left(\phi(u_1), \, \ldots, \, \phi(u_n)\right)$
\end{denotation}

\begin{statement}
	\slashns
	
	1) $\exists \, ! A \in Mat_{m \times n}(K): \forall \, u \in U \;\;\; (\phi(u))^f = A u^e$
	
	2) $\forall \, A \in Mat_{m \times n}(K) \;\;\; \exists \, !\phi: (\phi(u))^f = Au^e$
	
	То есть, линейный оператор $\phi$ однозначно определяется некоторой матрицей.
	
	\begin{proof}
		\slashns
		
		$(\phi(u))^f = (\phi(eu^e))^f = (\phi(e)u^e)^f = \phi_e^f \cdot u^e$
	\end{proof}
\end{statement}

\begin{definition}
	\slashns
	
	$\phi_e^f$~--- матрица линейного отображения $\phi$ в базисах $e$ и $f$.
	
	$\phi_e := \phi_e^e$
\end{definition}

\begin{example}
	\slashns
	
	$\phi: \R^2 \to \R^2$~--- поворот на угол $\alpha$
	
	\begin{tikzpicture}[scale=0.5]
	\draw[->] (-5, 0) -- (5, 0);
	\draw[->] (0, -5) -- (0, 5);
	\node at (5.5, 0) {x};
	\node at (0, 5.5) {y};
	\draw[thick, ->] (0, 0) -- (2, 0);
	\draw[thick, ->] (0, 0) -- (0, 2);
	\node[below] at (2, 0) {$e_1$};
	\node[left] at (0, 2) {$e_2$};
	\draw[->] (0, 0) -- (2, 3);
	\end{tikzpicture}
	
	$\left(\phi(e_1)^e, \, \phi(e_2)^e\right) = \begin{pmatrix}
	\cos \alpha & -\sin \alpha\\
	\sin \alpha & \cos \alpha
	\end{pmatrix}$~--- матрица линейного оператора.
\end{example}

\begin{definition}
	\slashns
	
	$V = U \oplus W$
	
	$e_1, \, \ldots, \, e_m$~--- базис $U$.
	
	$f_1, \, \ldots, \, f_k$~--- базис $W$.
	
	$e_1, \, \ldots, \, e_m, \, f_1, \, \ldots, \, f_k$~--- базис $V$.
	
	$\phi: V \to V, \;\;\; (u, v) \mapsto (u, 0)$~--- проекция.
	
	$\phi_{e,f} = \begin{pmatrix}
	1 & 0 & 0 & 0 & | & 0 \; & 0 \; & 0 \; & 0 \;\\
	0 & 1 & 0 & 0 & | & 0 \; & 0 \; & 0 \; & 0 \;\\
	0 & 0 & 1 & 0 & | & 0 \; & 0 \; & 0 \; & 0 \;\\
	0 & 0 & 0 & 1 & | & 0 \; & 0 \; & 0 \; & 0 \;\\
	\text{---} & \text{---} & \text{---} & \text{---} &  &  &  & \\
	0 & 0 & 0 & 0 &  & 0 \; & 0 \; & 0 \; & 0 \;\\
	0 & 0 & 0 & 0 &  & 0 \; & 0 \; & 0 \; & 0 \;\\
	0 & 0 & 0 & 0 &  & 0 \; & 0 \; & 0 \; & 0 \;\\
	0 & 0 & 0 & 0 &  & 0 \; & 0 \; & 0 \; & 0 \; 
	\end{pmatrix}$
	
	Заметим, что $\phi^2 = \phi$.
\end{definition}

\begin{example}
	\slashns

	$K[x]$~--- кольцо многочленов.
	
	$K_3[x] = \{f(x) \in K[x] : \deg f \le 3\}$
	
	$e = (1, \, x, \, x^2, \, x^3)$~--- базис $K_3[x]$
	
	$\phi = \frac{d}{dx}$
	
	$\phi_e = \begin{pmatrix}
	0 & 1 & 0 & 0\\
	0 & 0 & 2 & 0\\
	0 & 0 & 0 & 3\\
	0 & 0 & 0 & 0
	\end{pmatrix}$
\end{example}

\begin{statement}
	\slashns
	
	1) $U, \, V, \, W$~--- конечномерные векторные пространства.
	
	$e, \, f, \, g$~--- соответствующие базисы.
	
	$\phi: U \to V, \; \psi: V \to W$~--- линейные операторы.
	
	$U \stackrel{\phi}{\to} V \stackrel{\psi}{\to} W, \;\;\; \psi \circ \phi: U \to W$
	
	Тогда $(\psi \circ \phi)_e^g = \psi_f^g \cdot \phi_e^f$
	
	2) Если $U = V = W, \;\;\; e = f = g$, то $(\psi \circ \phi)_e = \psi_e \cdot \phi_e$.
	
	\begin{proof}
		\slashns
		
		%TODO j-ый столбик матрицы WTF???
		$((\psi \circ \phi)_e^g)_j = (\psi \circ \phi(e_j))^g = \big(\psi(\phi(e_j))\big)^g = \psi_f^g \cdot (\phi(e_j))^f = \psi_f^g \cdot (\phi_e^f)_j = (\psi_f^g \cdot \phi_e^f)_j$
	\end{proof}
\end{statement}

\begin{theorem} %WAT?
	\slashns
	
	$U, \, V$~--- векторные пространства над $K$.
	
	$e$~--- базис $U$.
	
	$e = (e_1, \, \ldots, \, e_n), \;\;\; n = \dim U$
	
	$f = (f_1, \, \ldots, \, f_n)$~--- базис $V$, $m = \dim V$
	
	Тогда:
	
	1) Имеется следующий изоморфизм векторных пространств:
	
	$(U \to V) \to Mat_{m \times n}(K)$ (из множества линейных отображений в множество матриц)
	
	$\phi \mapsto \phi_e^f$
	
	2) Имеется изоморфизм алгебр
	
	$\End U \to Mat_n(K)$
\end{theorem}

\Subsection{Замена базиса. Матрица перехода.}

\begin{definition}
	\slashns
	
	$V$~--- векторное пространство над $K$.
	
	$e, \, f$~--- базисы $V$.
	
	$f = f_1, \, \ldots, \, f_n$
	
	$f_i = \sum_{j=1}^{n} C_i^j e_j$
	
	$\begin{pmatrix}
	c_i^1\\
	\ldots\\
	c_i^n
	\end{pmatrix}
	= f_i^e$
	
	$(f_1, \, \ldots, \, f_n) = (e_1, \, \ldots, \, e_n)C$
	
	$C_f^e = (f_1^e, \, f_2^e, \, \ldots, \, f_n^e)$~--- матрица перехода от $e$ к $f$.
	
	\begin{remark}
		$C_e^e = E$
	\end{remark}
\end{definition}

\begin{statement}
	\slashns
	
	1) $\phi: V \to V$
	
	$\phi(e_i) = f_i$
	
	$\phi_e^f = C_f^e$
	
	2) $id: V_f \to V_e$
	
	$(id)_f^e = C_f^e$
\end{statement}

\begin{lemma}
	\slashns
	
	$C_e^f = (C_f^e)^{-1}$
	
	\begin{proof}
		\slashns
		
		$C_e^f \cdot C_f^e = (id)_e^f \cdot (id)_f^e = (id)_f^f = C_f^f = E$
	\end{proof}
\end{lemma}

\begin{statement}
	\slashns
	
	$f = (f_1, \, \ldots, \, f_n)$~--- линейно независимые $\iff
	\begin{pmatrix}
	\forall \, a, b \in K^m\\
	fa = fb \iff a = b
	\end{pmatrix}
	\iff
	\begin{pmatrix}
	\forall \, A, B \in Mat_{m \times n}(K)\\
	fA = fB \iff A = B
	\end{pmatrix}$
\end{statement}

\begin{statement}
	\slashns
	
	$f = (f_1, \, \ldots, \, f_n), \;\;\; e = (e_1, \, \ldots, \, e_n)$~--- базисы $V$.
	
	$v \in V$
	
	$v^f = C_e^fv^e$
	
	\begin{proof}
		\slashns
		
		$v = fv^f = ev^e = fC_e^fv^e$
		
		$v^f = C_e^fv^e$
	\end{proof}
\end{statement}

\Subsection{Изменение матрицы опеатора при замене базиса}

\begin{statement}
	\slashns

	$\phi: U \to V$~--- линейный оператор.
	
	$e, e'$~--- базисы $U$.
	
	$f, f'$~--- базисы $V$.
	
	$\phi_{e'}^{f'} = C_{f}^{f'} \cdot \phi_{e}^{f} \cdot C_{e'}^{e}$
	
	\begin{proof}
		\slashns
		
		$\phi_{e'}^{f'} = (id_v \circ \phi \circ id_u)_{e'}^{f'} = (id_v \circ \phi)_{e}^{f'} \cdot id_{e'}^e = (id)_f^{f'} \cdot \phi_e^f \cdot (id)_{e'}^e = C_f^{f'} \cdot \phi_e^f \cdot C_{e'}^e$
	\end{proof}
\end{statement}

\Subsection{Решение системы линейных уравнений}

$\begin{cases}
a_{1,1}x_1 + \ldots + a_{1,n}x_n = b_1\\
\vdots\\
\vdots\\
a_{m,1}x_1 + \ldots + a_{m,n}x_n = b_m
\end{cases}$

$A = \begin{pmatrix}
a_{1,1} & \ldots & \ldots\\
\ldots & \ddots & \ldots\\
\ldots & \ldots & a_{m,n}
\end{pmatrix} \in Mat_{m \times n}(K)$

$b = \begin{pmatrix}b_1\\\ldots\\b_m\end{pmatrix} \in K^m, \;\;\; x = \begin{pmatrix}x_1\\\ldots\\x_n\end{pmatrix} \in K^n$

$A \cdot \begin{pmatrix} x_1\\\ldots\\x_n\end{pmatrix} = b, \;\;\; Ax = b$

$\phi: K^n \to K^m, \;\;\; x \mapsto Ax$

Решения $Ax = b$.

$\phi^{-1}(b) = x_0 + \Ker \phi$

$x_0 \in \phi^{-1}(b)$, $x_0$~--- частное решение $Ax = b$.

$\Ker \phi$~--- решения $Ax = 0$ (однородной системы).

$\dim \Ker \phi = n - \rank \phi$

\begin{definition}
	\slashns
	
	Набор базисных векторов пространства $\Ker \phi$~--- фундаментальная система решений однородной системы.
\end{definition}

\begin{remark}
	\slashns
	
	Легко решить:
	
	$\begin{pmatrix}
	a_{1,1} & \ldots & 0\\
	\vdots & \ddots & \vdots\\
	0 & \ldots & a_{n,n}
	\end{pmatrix}
	\begin{pmatrix}
	x_1\\
	\vdots\\
	x_n
	\end{pmatrix}
	=
	\begin{pmatrix}
	b_1\\
	\vdots\\
	b_n
	\end{pmatrix}$
	
	Заметим также, что систему легко решить, если выше главной диагонали стоят произвольные коэффициенты.
\end{remark}

\begin{definition}
	\slashns
	
	$(A | b)$~--- расширенная матрица системы.
\end{definition}

\begin{definition}
	\slashns
	
	Элементарные преобразования:
	
	1) Прибавление к $i$-ой строки $j$-ой, умножение на $\alpha \in K$ .
	
	2) Умножение $i$-ой строки на $\alpha \ne 0, \; \alpha \in K$.
	
	3)Перестановка строк. 
\end{definition}

\begin{definition}
	\slashns
	
	Элементарные матрицы~--- матрицы вида:
	
	$E + \alpha e_i^j, \;\;\; \alpha \in K, \; 1 \le i = j \le n$
	
	%TODO
\end{definition}

\begin{definition}
	\slashns
	
	Ступенчатая матрица~--- матрица, у которой все нулевые строки внизу, а номера первых ненулевых коэффициентов в очередной строке строго возрастают.
\end{definition}

\begin{theorem}
	\slashns
	
	1) Всякую матрицу можно привести к ступенчатому виду с помощью элементарных преобразований.
	
	2) $\forall \, A \in Mat_{m \times n}(K) \;\; \exists \, l \ge 0 \;\; \exists \, B_1, \ldots, B_l \in Mat_{m}(K): B_1 \cdot \ldots \cdot B_l \cdot A$~--- ступенчатая матрица.
	
	$B_1, \ldots, B_l$~--- элементарные матрицы.
\end{theorem}