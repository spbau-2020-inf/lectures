\Section{Основы}{}
\Subsection{Немного истории}
В Java до версии 5 не было \texttt{generic}'ов. Затем с участием \href{https://ru.wikipedia.org/wiki/Одерски,_Мартин}{Мартина Одерски}, исследователя в области языков программирования, дженерики добавили. Тем не менее, стало понятно, что в рамках обратной совместимости нормально это сделать не получится. Тогда Одерски решил создать свой язык, с нормальной системой типов. Изначально Scala была академическим языком (для исследований в области ЯП), но постепенно она переросла в промышленный. Как академический язык, Scala развивалась силами студентов, которые пилили и добавляли свои фичи. Это привело к тому, что фич стало очень много, некоторые из них позволяют делать одно и то же действие разными способами. В связи с этим существуют разные мнения, как надо писать на этом языке. Однако есть официальный \href{https://docs.scala-lang.org/style/}{стайлгайд}, которого желательно придерживаться.

Scala совмещает в себе функциональную и объектно-ориентированную парадигмы. Начнем мы с ООП, так как это более привычно читателям. Многие вещи уже знакомы нам из Котлина и Java, на них подробно останавливаться не будем.

\Subsection{Классы}
\Subsubsection{\texttt{class}}
Обычный класс, как в Java. Внутри можно заводить поля и методы.

\begin{lstlisting}
class Foo {
  val bar: Int = 42
  def foo(): Int = bar + 1
}
\end{lstlisting}

У класса бывают примарный и побочный конструкторы, побочными пользуются редко. Побочный конструктор объявляется ключевым словом \texttt{this}.

\begin{lstlisting}
class Bar(innerFoo: Int) {
  this(doubleFoo: Double) =  // побочный конструктор
    this(doubleFoo.toInt)       // вызывает примарный

  def foo(): Int = innerFoo // используем значение из примерного конструктора
}
\end{lstlisting}

\Subsubsection{\texttt{trait}}
\texttt{trait} -- это интерфейс. Интерфейсы в скале поддерживают дефолтную реализацию методов. Также в интерфейсе можно завести немутабельное поле без значения.

\begin{lstlisting}
trait Bar {
  val bar: Double
  def foo(): Int = 42
}
\end{lstlisting}

\Subsubsection{\texttt{object}}
Объекты и \texttt{companion}-объекты в Scala есть. Они мощнее чем в Котлине, но об этом позже. Также надо заметить, то статических полей и методов в классах и интерфейсах нет, статичность достигается как раз за счет \texttt{object}'ов. Чтобы объявить \texttt{companion object}, надо написать объект с именем нужного класса.

\begin{lstlisting}
object Foo // синглтон Foo
class Bar // класс Bar
object Bar // companion object для класса Bar
\end{lstlisting}

\Subsection{Наследование}
Наследоваться, как и в Java, можно от одного класса и любого количества интерфейсов, но ключевое слово для них одно -- \texttt{extends}. для множественного наследования используется ключевое слово \texttt{with}. Если в множественном наследовании участвует класс, он должен стоять на первом месте.

\begin{lstlisting}
class Foo
trait Bar
trait Baz

class MyClass extends Foo with Bar with Baz
\end{lstlisting}

При наследовании как у полей, так и у методов надо писать модификатор \texttt{override}.

\begin{lstlisting}
class Foo extends Bar {
  override val bar: Double = 42
  override def foo(): Int = bar.toInt
}
\end{lstlisting}

\Subsubsection{Наследование в \texttt{generic}'ах}
Вместо \texttt{extends} и \texttt{super} в Scala используются специальные значки. Это пришло из теории типов (как мы помним, Scala изначально академический язык).

\begin{lstlisting}
class GenericFoo[F <: Foo] // <F extends Foo>
class GenericFoo[F >: Foo] // <F super Foo>
\end{lstlisting}

Символ \texttt{<:} также можно использовать для наследования вместо \texttt{extends}.

\begin{lstlisting}
trait Foo <: Serializable // trait Foo extends Serializable
\end{lstlisting}

\Subsection{Оператор \texttt{new} и функциональный метод \texttt{apply()}}
В отличие от Котлина, в Scala надо писать \texttt{new} при инстанцировании объекта. В Scala это пришло из Java, а туда -- из C++, в котором это важно -- создется ли объект в куче или на стеке.

\pagebreak

Есть способ, как не писать \texttt{new}. Для этого заведем \texttt{companion object} для нашего класса и реализуем в нем метод \texttt{apply()}.

\begin{lstlisting}
class Bar
object Bar {
  def apply(): Bar = new Bar
}

val bar = Bar() // теперь можем не писать new
val bar = Bar.apply() // аналогичный вызов
\end{lstlisting}

Метод \texttt{apply()} можно определять с разными аргументами, то есть это что-то вроде перегрузки круглых скобок (\texttt{operator ()} в С++).

Также \texttt{apply()} может заменить вторичный конструктор. Недостаток только в том, что при объявлении класса-наследника можно использовать только вызовы конструкторов. 

\begin{lstlisting}
class Bar(foo: Int) { ... }
object Bar {
  def apply(doubleFoo: Double) = new Bar(doubleFoo.toInt)
}

class Baz extends Bar(42.0) // ошибка
\end{lstlisting}

\Subsection{Скобки}
\Subsubsection{Писать -- не писать}
В Scala есть конвенция, что если какой-то код можно не писать, его писать не надо. Если у класса нет тела, то фигурные скобки не пишутся, то же и с вызовами функций. Если в функцию не передаются аргументы, и она не имеет побочных эффектов, скобки принято опускать. Если же побочные эффекты есть, скобки все же пишут, чтобы подчеркнуть, что мы вызываем функцию. При объявлении метода без побочных эффектов скобки тоже опускаются.

\Subsubsection{Скобки для \texttt{generic}'ов}
В Scala для \texttt{generic} параметров пишутся квадратные скобки, а не угловые. Так сделано, чтобы избежать неоднозначности при парсинге. Пример, когда угловые скобки распарсить не получается:

\begin{lstlisting}
f1(f2<A, B>(x + 1))
\end{lstlisting}

\Subsection{Массивы и доступ к элементам}
В качестве массивов используется класс \texttt{Array}. Поскольку квадратные скобки заняты под \texttt{generic}'и, для доступа к элементам они не используются. Зато для \texttt{Array} и для некоторых других коллекций определены волшебные методы -- уже знакомый нам \texttt{apply()} и \texttt{update()}. Они позволяют обращаться к элементам через круглые скобки.

\begin{lstlisting}
val array: Array[Int](3)
array(0) = 42 // array.update(0, 42), присваиваем первый элемент
println(array(0)) // array.apply(0), получаем первый элемент
\end{lstlisting}

\Subsection{Модификаторы доступа}
По-умолчанию все члены класса публичные. Для всего остального есть модификаторы \texttt{private} и \texttt{protected}. Вместо \texttt{package private} модификатора используются модификаторы с усилением.

\begin{lstlisting}
package ru.spbau.jvm.scala

class Foo {
  private var foo: Int             // доступ только внутри класса
  private[scala] var bar: Int // доступ только в пакете ru.spbau.jvm.scala
  private[jvm] var baz: Int        // доступ только в пакете ru.spbau.jvm
  private[this] var bus: Int      // доступ только внутри экземпляра

  def fun(other: Foo): Unit = {
    bus = 42         // ок, доступ изнутри экземпляра
    other.bus = 42 // ошибка, доступ из другого экземпляра
  }
}
\end{lstlisting}

Примарный конструктор тоже можно сделать защищенным или приватным

\begin{lstlisting}
class Foo protected()
class Bar private()
class Baz private[this]()
\end{lstlisting}

Так как \texttt{companion object}'ы имеют доступ к приватным полям своих классов, единственный способ что-то от них скрыть (в т.ч. конструктор) -- это \texttt{private[this]}. При этом, так как в Java такой фичи нет, в ней этот модификатор воспринимается как просто \texttt{private}.

\Subsection{Properties}
Они есть. Примерно как в Котлине. \TODO об этом позже

\Subsection{Hello world!}
Есть несколько способов написать запускающуюся программу на Scala. Самый простой и привычный -- это объявить метод \texttt{main}.

\begin{lstlisting}
object HelloWorld {
  def main(args: Array[String]): Unit = {
    println("Hello world!")
  }
}
\end{lstlisting}

Другой способ -- отнаследоваться от трейта \texttt{App} и написать всё прямо в теле класса. Но таким способом пользоваться не рекомендуется.

\begin{lstlisting}
class HelloWorld extends App {
  println("Hello world!")
}
\end{lstlisting}

\Subsection{Пакеты, \texttt{import}'ы и коллизии}
В Scala, как и в Котлине, есть свои коллекции, например \texttt{List}. Если мы хотим использовать \texttt{java.util.List}, надо либо явно писать весь пакет, либо заимпортить его. Но при импорте Scala'вский \texttt{List} перекроется, что не очень хорошо. Поэтому можно импортить прямо пакеты.

\begin{lstlisting}
import java.util
var list: util.List[Int]
\end{lstlisting}

Но такие названия как \texttt{util} тоже могут встречаться часто и конфликтовать. Чтобы этого избежать, можно создавать синонимы для пакетов. В основном это используют именно для \texttt{java.util}, для чего-то другого так делают редко.

\begin{lstlisting}
import java.{util => ju}
var list: ju.List[Int]
\end{lstlisting}

\Subsection{Избавляемся от лишнего}
Рассмотрим следующий код и поймем, что в нем можно сократить.

\begin{lstlisting}
def foo(x: Boolean): Int = {
  if (x) return 42
  return 43
}
\end{lstlisting}

Во-первых, последний \texttt{return} можно не писать, потому что результат последнего выражения и так возвращается по-умолчанию. Во-вторых, можно объединить две ветки условия в \texttt{if else} и избавиться от \texttt{return} совсем.

\begin{lstlisting}
def foo(x: Boolean): Int = {
  if (x) 42 else 43
}
\end{lstlisting}

Теперь у нас функция состоит из одного выражения, поэтому можно убрать скобки и возвращаемый тип, так как он выведется.

\begin{lstlisting}
def foo(x: Boolean) = if (x) 42 else 43
\end{lstlisting}

\Subsection{Рекурсия}
Напишем функцию, считающую факториал числа.

\begin{lstlisting}
def fac(n: Int): BigInt =
  if (n == 1) 1
  else n * fac(n - 1)
\end{lstlisting}

Проблема такой реализации в том, что при вызове \texttt{fac(10000)} случится переполнение стека. Хочется сделать хвостовую рекурсию, а затем развернуть ее в цикл.

\begin{lstlisting}
@tailrec
def facTailRec(n: Int, accumulator: BigInt = 1): BigInt = n match {
  case 1 => accumulator
  case _ => facTailRec(n - 1, n * accumulator)
}
\end{lstlisting}

Здесь мы видим сразу две фичи -- аннотацию \texttt{@tailrec} и матчер. Матчер работает примерно как \texttt{when} в Котлине. Аннотация \texttt{@tailrec} нужна только для того, чтобы удостовериться, что в методе используется хвостовая рекурсия. В противном случае код не скомпилируется. Компилятор же \textbf{всегда} разворачивает хвостовую рекурсию в цикл.

\Subsection{Метод в методе}
В нашей реализации факториала есть одна концептуальная проблема. Мы можем передать туда любой аккумулятор, и это повлияет на результат. Чтобы этого избежать, можно обернуть наш метод в другой метод, таким образом спрятав его от нерадивых пользователей.

\begin{lstlisting}
def facTailRec(n: Int): BigInt = {
  @tailrec
  def facTailRecInner(n: Int, accumulator: BigInt = 1): BigInt = n match {
    case 1 => accumulator
    case _ => facTailRec(n - 1, n * accumulator)
  }

  facTailRecInner(n)
}
\end{lstlisting}

\Subsection{\texttt{try}}
Блок \texttt{catch} очень похож на \texttt{match} тем, что внутри мы матчим исключения, чтобы по-разному их обрабатывать. Рассмотрим пример.

\begin{lstlisting}
try {
  fac(100000)
} catch {
  case e: StackOverflowError => facTailRec(100000)
  case _: IOException | _: FileException => ... // комбинируем случаи
  _: // матчим всё что не сматчилось ранее
}
\end{lstlisting}

\TODO подробнее будет позже, вместе с \texttt{match}

\Subsection{Алгебраические типы данных}

Хотим написать список в функциональном стиле (как в Haskell).

\begin{lstlisting}
sealed trait IntList

final class IntNil extends IntList
final class IntCons(val head: Int, val tail: IntList) extends IntList
\end{lstlisting}

Теперь можем заводить списки.

\begin{lstlisting}
val list: IntList = new IntCons(1, new IntCons(2, new IntCons(3, new IntNil)))
\end{lstlisting}

А потом матчить.

\begin{lstlisting}
list match {
  case cons: IntCons => println(cons.head) // теперь cons = list.asInstanceOf[IntCons]
  case _: IntNil => // ничего не делаем
}
\end{lstlisting}

Поскольку трейт \texttt{IntList} мы объявили \texttt{sealed}, у нас есть гарантия, что больше никаких наследников кроме этих двух нет, то есть всегда что-то заматчится. Иначе мы могли бы завести инстанс анонимного класса, и матчер бы свалился, потому что никакая ветка бы не подошла.

\Subsection{\texttt{case} классы}

В нашей реализации списка тоже есть лишний код, от которого лучше избавиться. Для начала поймем, что \texttt{IntNil} бывает всего один, поэтому его можно сделать \texttt{object}'ом. А теперь добавим к наследникам сключевое слово \texttt{case}. Оно создает для класса \texttt{companion object}, определяет для него методы \texttt{apply()} и \texttt{thenApply()} (о нем позже). Сделает неизменяемые параметры примарного конструктора полями (не надо будет писать в конструкторе \texttt{val}) и сгенерирует методы \texttt{equals()}, \texttt{hashCode()} и \texttt{toString()}. В Котлине очень похожее поведение имеют \texttt{data}-классы.

\begin{lstlisting}
sealed trait IntList

case object IntNil extends IntList
final case class IntCons(head: Int, tail: IntList) extends IntList

val list: IntList = IntCons(1, IntCons(2, IntCons(3, IntNil)))
\end{lstlisting}

Более того, теперь в матчере для \texttt{IntNil} не надо писать плейсхолдер, а \texttt{IntCons} теперь можно матчить по его параметрам. Теперь можем рекурсивно вывести список на экран.

\begin{lstlisting}
def printList(list: IntList): Unit = {
  list match {
    case IntNil =>
    case IntCons(head, tail) =>
      println(head)
      printList(tail)
  }
}
\end{lstlisting}