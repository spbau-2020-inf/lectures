\Section{Type-level programming}

\Subsection{Path dependent types}

Минутка алгоритмов. Пусть у нас есть граф. У него есть внутренние классы -- вершины и ребра. Хранятся эти вершины и ребра в списках, в которые можно добавлять элементы с помощью оператора \texttt{+=}.

\begin{lstlisting}
class Graph {

  case class Vertex()

  type Edge = (Vertex, Vertex)

  private val vertices = mutable.ArrayBuffer.empty[Vertex]
  private val edges = mutable.ArrayBuffer.empty[Edge]

  def +=(vertex: Vertex): Unit = {
    vertices += vertex
  }

  def +=(edge: Edge): Unit = {
    edges += edge
  }
}
\end{lstlisting}

Попробуем воспользоваться этим классом.

\begin{lstlisting}
// создаем граф
val firstGraph = new Graph
// создаем две вершины
val fromVertex = firstGraph.Vertex() 
val toVertex = firstGraph.Vertex()
// добавляем вершины в граф
firstGraph += fromVertex
firstGraph += toVertex
// добавляем ребро в граф
firstGraph += (fromVertex, toVertex)
\end{lstlisting}

Всё хорошо. Теперь создадим второй граф и попробуем в него добавить те же самые вершины и ребра.

\begin{lstlisting}
val secondGraph = new Graph
secondGraph += fromVertex // ошибка
secondGraph += (fromVertex, toVertex) // ошибка
\end{lstlisting}

У нас ничего не получится, потому что в Scala типы внутренних классов зависят от конкретного экземпляра внешнего класса, используемого при инстанцировании.

Если по-русски, то у \texttt{toVertex} и \texttt{fromVertex} будет тип \texttt{firstGraph.Vertex}, а во второй граф можно добавлять только вершины типа \texttt{secondGraph.Vertex}. Аналогично с ребрами.

Что же делать, если хотим функцию, принимающую любые вершины или ребра? Для этого надо указать тип аргумента не \texttt{Graph.Edge}, а \texttt{Graph\#Edge}. Решетка показывает, что нам не важно, какому именно графу принадлежит ребро. Теперь можем написать функцию, выводящую ребро в консоль.

\begin{lstlisting}
def printEdge(vertex: Graph#Edge): Unit = {
    val (from, to) = vertex
    println(s"$from -> $to")
}
\end{lstlisting}

\Subsection{Гетерогенные списки}
Гетерогенный список позволяет хранить элементы разных типов, при этом информация о типе каждого элемента тоже сохраняется.

Задать такой список можно рекурсивно, с помощью case классов.

\begin{lstlisting}
sealed trait HList

object HList {
  case class HCons[+H, +T <: HList](head: H, tail: T) extends HList
  case object HNil extends HList
}

val list = HCons(1, HCons("hello!", HNil))
val str: String = list.tail.head
\end{lstlisting}

Пока всё просто. Теперь научимся объявлять список более красиво, реализовав оператор \texttt{::}

\begin{lstlisting}
implicit class HListExt[R <: HList](private val list: R) extends AnyVal {
  def ::[H](head: H) = HCons(head, list)
}

val list = 1 :: "asd" :: HNil
\end{lstlisting}

Кажется, что здесь произошла магия, ведь мы сделали расширение для списка, а слева от первого \texttt{::} стоит \texttt{Int}. На самом деле оператор \texttt{::} является правоассоциативным, и у него справа будет стоять объект, от которого он вызывается, а слева -- аргумент. То есть наша запись развернется в следующую.

\begin{lstlisting}
val list = HNil.::("asd").::(1)
\end{lstlisting}

Едем дальше. Хотим склеивать списки. И тут нам придется выйти на новый уровень отношений со Scala -- type level.

Начнем с определения самого оператора. Он будет принимать в качестве implicit аргумента trait, который будет нам всё склеивать, и вызывать у него \texttt{apply}, в котором всё и будет происходить.

\begin{lstlisting}
def :::[L <: HList, Result <: HList](left: L)
                                          (implicit appendable: Appendable[L, R, Result]): Result =
  appendable(left, list)
\end{lstlisting}

А теперь приступим к реализации самого \texttt{Appendable}.

\begin{lstlisting}
trait Appendable[L <: HList, R <: HList, Result <: HList] {
  def apply(left: L, right: R): Result
}
\end{lstlisting}

В качестве типовых параметров \texttt{Appendable} принимает тип левого списка, тип правого списка и тип результата. Вспоминаем, что запись \texttt{L <: HList} означает, что тип \texttt{L} является наследником типа \texttt{HList} или им самим.

А теперь -- самое интересное. Мы, как в Haskell, по индукции, определим операцию склеивания.

\begin{lstlisting}
object Appendable {
  import HList._

  implicit def nilAppendable[R <: HList]: Appendable[HNil.type, R, R] =
    new Appendable[HNil.type, R, R] {
      override def apply(left: HNil.type, right: R) = right
    }

  implicit def appendable[L <: HList, R <: HList, H, Result <: HList]
                             (implicit appendable: Appendable[L, R, Result]):
                             Appendable[HCons[H, L], R, HCons[H, Result]] =
    new Appendable[HCons[H, L], R, HCons[H, Result]] {
      override def apply(left: HCons[H, L], right: R) =
        HCons(left.head, appendable(left.tail, right))
    }
}
\end{lstlisting}

Не торопитесь падать в обморок. Сейчас во всём разберемся. Для начала поймем, что происходит в \texttt{apply()}. Он принимает на вход два списка и должен вернуть третий. Так как \texttt{Appendable} является trait'ом, и \texttt{apply()} реализации не имеет, компилятор пытается найти функцию, которая бы вернула подходящую реализацию. Здесь представлено две такие функции.

Функция \texttt{nilAppendable()} возвращает тип \texttt{Appendable[HNil.type, R, R]}, то есть такую реализацию, в которой метод \texttt{apply()} в качестве левого списка принимает \texttt{HNil} и возвращает правый список. Теперь мы можем склеивать пустой список с любым другим. Это будет база нашей индукции.

Функция \texttt{appendable()} возвращает такую реализацию \texttt{Appendable}, которая принимает первым параметром \textbf{не}пустой список. Метод \texttt{apply()} в этой реализации будет возвращать список, состоящий из \texttt{left.head} и склеенных \texttt{left.tail} и \texttt{right}. Но чтобы склеить \texttt{left.tail} и \texttt{right}, потребуется уже другая реализация \texttt{Appendable}. Эту реализацию наша функция получает в качестве implicit аргумента и рекурсивно выбирает функцию, которая ее вернет.

Напоследок заменим анонимные классы на лямбды, чтобы выглядело всё не так страшно.

\begin{lstlisting}
implicit def nilAppendable[R <: HList]: Appendable[HNil.type, R, R] =
  (left: HNil.type, right: R) => right

implicit def appendable[L <: HList, R <: HList, H, Result <: HList]
                           (implicit appendable: Appendable[L, R, Result]):
                           Appendable[HCons[H, L], R, HCons[H, Result]] =
  (left: HCons[H, L], right: R) => HCons(left.head, appendable(left.tail, right))
\end{lstlisting}

Прелесть всего этого ужаса заключается в том, что все вычисления происходят еще на этапе компиляции, когда мы знаем типы всех элементов списка. И так как для разных типов создаются разные реализации \texttt{Appendeble}, они-то и позволяют нам помнить на этапе выполнения, какой тип имеет каждый элемент.

\pagebreak

\Subsection{Абстрактные типы}
Эту тему совсем чуть-чуть затронули на последней лекции. Краткое описание \href{https://docs.scala-lang.org/tour/abstract-types.html}{здесь}.

\Subsection{Продвинутые ограничения в generic типах}
Это тоже немного было на последней лекции. Читаем \href{https://twitter.github.io/scala_school/advanced-types.html#otherbounds}{здесь}.