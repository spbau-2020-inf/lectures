\select@language {russian}
\contentsline {section}{1. Введение}{1}{section*.2}
\contentsline {subsection}{\numberline {1.1}Графовые теоремы.}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Предельные теоремы для схем Бернулли}{2}{subsection.1.2}
\contentsline {section}{2. Общая теория вероятностей}{5}{section*.3}
\contentsline {subsection}{\numberline {2.1}Колмогоровское определение вероятности}{5}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Случайная величина, распределение, плотность}{7}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Примеры вероятностных распределений}{9}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Многомерные распределения}{11}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Свёртки мер}{12}{subsection.2.5}
\contentsline {section}{3. Математическое ожидание}{15}{section*.4}
\contentsline {subsection}{\numberline {3.1}Корреляция и ковариация}{21}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Сходимость случайных величин и закон больших чисел}{24}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Закон больших чисел}{26}{subsection.3.3}
\contentsline {section}{4. Характеристическая функция}{29}{section*.5}
\contentsline {section}{5. Центральная предельная теорема}{40}{section*.6}
\contentsline {section}{6. Случайные процессы}{45}{section*.7}
\contentsline {subsection}{\numberline {6.1}Определения}{45}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Условные мат. ожидания}{46}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Марковские цепи}{49}{subsection.6.3}
