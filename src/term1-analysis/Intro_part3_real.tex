\Subsection{Вещественные числа}

\Subsubsection{Понятие вещественных чисел}

\begin{definition}
	Вещественные числа~--- алгебраическая структура, над которой определены операции сложения <<$+$>> и умножения <<$\cdot$>> ($\R \times \R \to \R$).
\end{definition}

\begin{definition}
	Аксиомы вещественных чисел.
	
	\begin{itemize}
		\item[A1.] Ассоциативность сложения\\
		$x + (y + z) = (x + y) + z$
		
		\item[A2.] Коммутативность сложения\\
		$x + y = y + x$
		
		\item[A3.] Существование нуля\\
		$\exists \, 0 \in \R: \forall \, x \in R \; \; x + 0 = x$
		
		\item[A4.] Существование обратного элемента по сложению\\
		$\forall \, x \in \R \; \exists \, (-x) \in \R: x + (-x) = 0$
		
		\item[M1.] Ассоциативность умножения\\
		$x(yz) = (xy)z$
		
		\item[M2.] Коммутативность умножения\\
		$xy = yx$
		
		\item[M3.] Существование единицы\\		
		$\exists \, 1 \in \R: \forall \, x \in \R \; \; x \cdot 1 = x$
		
		\item[M4.] Существование обратного элемента по умножению\\	
		$\forall \, x \in \R \; \exists \, x^{-1} \in \R: x \cdot x^{-1} = 1$
		
		\item[AM.] Дистрибутивность\\
		$(x+y)z = xz + yz$

		\item[O1.] $x \le x \; \; \forall \, x$
		
		\item[O2.] $x \le y \text{ и } y \le x \implies x = y$
		
		\item[O3.] $x \le y \text{ и } y \le z \implies x \le z$
		
		\item[O4.] $\forall \, x, \, y \in \R: x \le y \text{ или } y \le x$
		
		\item[O5.] $x \le y \implies x + z \le y + z \; \; \forall z$
		
		\item[O6.] $0 \le x \text{ и } 0 \le y \implies 0 \le xy$
	\end{itemize}
	
	Объекты, отвечающие аксиомам A1-A4, M1-M4 и AM, образуют поле.
	
	Аксиомы O1-O6 называются аксиомами порядка и задают порядок на множестве вещественных чисел.
\end{definition}

\begin{definition}
	Аксиома полноты.
	
	$A, \, B \subset \R: A \ne \emptyset, \; B \ne \emptyset, \; \forall \, a \in A \; \; \forall \, b \in B \; \; a \le b$
	
	Тогда $\exists \, c \in \R: a \le c \le b \; \; \forall \, a \in A \; \; \forall \, b \in B$.
	
	\begin{remark}
		Для $\Q$ аксиома полноты не выполняется:
		
		$A = \{a \in \Q: a^2 < 2\}$
		
		$B = \{b \in \Q: b \ge 0, \; b^2 > 2\}$
		
		Тогда не существует $c \in \Q: a \le c \le b$, т.к. $c^2 = 2$.
	\end{remark}
	
\end{definition}

\Subsubsection{Принцип математической индукции}

Принцип математической индукции.

Положим $P_n$~--- последовательность утверждений.

1. $P_1$~--- верно

2. $\forall \, n \in \N$ из $P_n$ следует $P_{n+1}$.

Тогда $P_n$ верно при всех $n \in \N$.

\begin{statement}
	В конечном множестве вещественных чисел есть наибольший и наименьший элемент.
	
	\begin{proof}
		\thmslashn
		
		Будем доказывать это утверждение по индукции. Докажем для минимума (для максимума аналогично).
		
		База: $n = 1$~--- очевидно.
		
		Переход: $n \to n + 1$.
		
		Рассмотрим произвольное множество из $n$ элементов $\{x_1, \, x_2, \, ..., \, x_n\}$. Пусть мы уже знаем, что минимумом в нём является элемент $x_k$. Тогда рассмотрим то же множество с добавленным в него элементом $x_{n+1}$. Заметим, что:		
		\begin{enumerate}
			\item $x_k \le x_{n+1} \implies x_k$~--- минимум
			\item $x_k > x_{n+1} \implies$ минимумом является новый добавленный элемент $x_{n+1}$.
		\end{enumerate}
		
		Таким образом, в любом конечном множестве вещественных чисел существует минимальный элемент.
	\end{proof}
\end{statement}

\Subsubsection{Принцип Архимеда}

Согласно принципу Архимеда, $\forall \, x \in \R$ и $\forall \, y > 0 \in \R \; \; \exists \, n \in \N: x < ny$.

\begin{proof}
	\thmslashn
	
	$A = \{a \in \R: \exists \, n \in \N: a < ny\}, \' \;A \ne \emptyset, \text{ т.к. } 0 \in A$
	
	$B = \R \setminus A$
	
	Пусть $A \ne \R$. Тогда $B \ne \emptyset$. Покажем, что $a \le b$, если $a \in A, \; b \in B$.
	
	От противного. Пусть $b < a < ny \implies b < ny \implies b \in A \implies$ противоречие.
	
	Таким образом, по аксиоме полноты существует $c \in \R: a \le c \le b \; \; \forall a \in A \; \; \forall b \in B$.
	
	Пусть $c \in A$. Тогда $c < ny$ для некоторого $n \in \N \implies c + y < (n + 1) y \implies c + y \in A \implies c + y \le c \implies y \le 0$. Пришли к противоречию.
	
	Пусть $c \in B$. Возьмём $c - y < c \implies c - y \in A \implies c - y < ny \implies c < (n + 1)y \implies c \in A$. Снова противоречие.
	
	Таким образом, мы получили, что такое $c$ не существует $\implies B = \emptyset \implies A = \R$.	
\end{proof}

\begin{consequence}
	\thmslashn
	
	\begin{enumerate}
		\item $\forall \, \eps > 0 \; \; \exists \, n \in \N: \frac1n < \eps$
			
		\begin{proof}
			\thmslashn
			
			$x = 1, \; y = \eps \implies \exists \, n \in \N: 1 < n \eps$.
		\end{proof}
		
		\item Если $x, \, y \in \R, \; x < y$, то $\exists \, r \in \Q: x < r < y$.
		
		\begin{proof}
			\thmslashn
			
			Пусть $x < 0, \, y > 0$. Тогда $\exists \, r = 0 \in \Q: x < r < y$.
			
			Пусть $x \ge 0, \; \eps = y - x$. Тогда $\exists \, n \in \N: \frac1n < \eps$.
			
			По принципу Архимеда найдётся такое число $m$, что $\frac{m-1}{n} \le x < \frac{m}{n}$. 
			
			Предположим, что $\frac{m-1}{n} \le x < y \le \frac{m}{n}$. Однако тогда получаем, что $\frac1n \ge y - x = \eps$. Получили противоречие.
			
			Следовательно, $\exists \, m \in \N: x < \frac{m}{n} < y$.
			
			Случай $y \le 0$ аналогичен предыдущему.
		\end{proof}
		
		\item Если $x, \, y \in \R$ и $x < y$, то $\exists$ иррациональное число $r: x < r < y$.
		
		\begin{proof}
			\thmslashn
			
			$x - \sqrt{2} < y - \sqrt{2} \implies \exists \, r \in (x - \sqrt{2}, \, y - \sqrt{2}) \implies x < r + \sqrt{2} < y \implies r$~--- иррациональное.
		\end{proof}
		
		\item Если $x \ge 1$, то $\exists n \in \N: x - 1 < n \le x$
		
	\end{enumerate}
	
\end{consequence}