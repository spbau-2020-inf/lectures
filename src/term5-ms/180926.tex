\newpage

\Subsection{Построение оценок. Метод максимального правдоподобия}

\begin{motivation}
	\slashns
	
	Мы уже выяснили, что метод моментов умеет неплохо приближать реальные оценки. Тем не менее, он требует от нас вычисления обратной функции, что в случае произвольного распределения очень сложно и нетривиально. В некоторых же ситуациях он и вовсе не способен выдать никакого решения (к примеру, если матожидание отсутствует). 
	
	Потому хотелось бы рассмотреть метод получения оценок, который избавил бы нас по крайней мере от подобных проблем.
	
	Кроме того, мы также покажем, что рассматриваемый далее метод дает асимптотически эффективные оценки, в то время как про эффективность метода моментов в общем случае сложно что-то сказать.
\end{motivation}

\begin{reminder}
	\slashns
	
	Распределение $\P_{\theta}$ абсолютно непрерывно относительно меры $\mu$ (обозначается как $\P_{\theta} \ll \mu$), если $\mu(A) = 0 \implies \P_{\theta}(A) = 0$.
\end{reminder}

\begin{definition}
	\slashns
	
	$X_1, \, \ldots, \, X_n \sim \P_{\theta}, \; \theta \in \Theta$
	
	$\P_{\theta} \ll \mu$, где $\mu$~--- $\sigma$-конечная мера (к примеру, мера Лебега или считающая мера)
	
	По теореме Радона-Никодима $\P_{\theta}$ представима как $\P_{\theta}(A) = \int[A] f(x; \, \theta) \; \mu(dx)$ для некоторой $f$.
	
	В данном случае, $f(x; \, \theta)$~--- или плотность распределения (в случае меры Лебега), или вероятность $\P_{\theta}(X = x)$ события $x$ (в случае считающей меры).
	
	Тогда для произвольной заданной выборки $\mathbf{X} = (X_1, \, \ldots, \, X_n)$ можно определить функцию правдоподобия $L$ (от likelihood):
	$$L(\mathbf{X}; \, \theta) := L(X_1, \, \ldots, \, X_n; \, \theta) = \prod\limits_{i=1}^{n} f(X_i;\, \theta)$$
	
	$\widehat{\theta}$~--- оценка максимального правдоподобия (ОМП), если $L(\mathbf{X}; \, \widehat{\theta}) = \max\limits_{\theta} L(\mathbf{X}; \,\theta)$.
	
	Соответственно, основная идея метода максимального правдоподобия~--- подобрать такой набор параметров $\widehat{\theta}$, при котором вероятность получения данной выборки максимальна, т.е. достигается максимум $L(\mathbf{X}; \, \theta)$.
	
	Кроме того, нередко вместо $L(\mathbf{X}; \, \theta)$ максимизируют $\ln L(\mathbf{X}; \, \theta)$, поскольку можно перейти от произведения к сумме.
\end{definition}

\begin{remark}
	\slashns
	
	На самом деле, когда речь идет о функции правдоподобия, подразумевается именно функция одного аргумента $L_{\mathbf{X}}(\theta)$, т.е. сама выборка фиксируется. Тем не менее, в литературе часто выборка $\mathbf{X}$ также указывается в качестве аргумента $L$, а потому дальше будет использоваться именно такая нотация.
\end{remark}

\begin{example}
	\slashns
	
	Пусть $X_1, \, \ldots, \, X_n \sim \text{Poisson}(\theta), \;\; \theta \in \Theta$.
	
	Выпишем функцию правдоподобия для нашей выборки:
	$$L(\mathbf{X}; \,\theta) = \prod\limits_{i=1}^{n} \dfrac{\theta^{X_i}}{X_i!} \cdot e^{-\theta} = \dfrac{\theta^{X_1 + \ldots + X_n}}{X_1! \cdot \ldots \cdot X_n!} \cdot e^{-n\theta}$$
	
	Хотим найти параметр $\theta$, который бы максимизировал значение функции правдоподобия. Для этого достаточно продифференцировать $L(\mathbf{X}; \, \theta)$ по параметру и найти ноль производной, после чего проверить, что он действительно является максимумом. 
	
	Тем не менее, искать производную суммы легче, чем производную произведения, а потому прологарифмируем нашу функцию:
	$$\ln L(\mathbf{X}; \,\theta) = - \ln(X_1! \cdot \ldots \cdot X_n!) + \sum\limits_{i=1}^{n} X_i \cdot \ln \theta - n \theta$$
	$$\dfrac{\d}{\d \theta} \ln L(\mathbf{X}; \, \theta)  = \dfrac{1}{\theta} \cdot \sum\limits_{i=1}^{n} X_i - n = 0 \implies \widehat{\theta} = \overline{X}$$
	$$\dfrac{\d^2}{\d \theta^2} \ln L(\mathbf{X}; \,\theta) = - \dfrac{1}{\theta^2} \cdot \sum\limits_{i=1}^{n} X_i < 0 \implies \widehat{\theta} = \underset{\theta}{\argmax} \; L(\mathbf{X}; \, \theta)$$
\end{example}

\begin{definition}
	\slashnss
	
	$X_1, \, \ldots, \, X_n \sim \P_{\theta}, \;\; \theta \in \Theta$
	
	Информацией Фишера называется функция $I_n(\theta) = \E_{\theta} \left(\dfrac{\d \ln f(X_1, \, \ldots, \, X_n; \, \theta)}{\d \theta}\right)^2$
\end{definition}

\begin{remark}
	\slashns
	
	Так как наблюдения у нас независимы, $f(X_1, \, \ldots, \, X_n; \, \theta) = \prod\limits_{i=1}^{n} f(X_i; \, \theta) = L(\mathbf{X}; \, \theta)$.
	
	А потому информацию Фишера можно записать как $I_n(\theta) = \E\left(\dfrac{\d\ln L(\mathbf{X}; \, \theta)}{\d \theta} \right)^2$.
\end{remark}	

\begin{properties}[информации Фишера]
	\slashn
	
	\begin{enumerate}
		\item Информация Фишера для выборки размера $n$ представима как $I_n(\theta) = \int[\mathcal{X}] \dfrac{(f'_{\theta})^2}{f} \, d\mu$
		
		\begin{proof}
			$$I_n(\theta) = \E_{\theta} \left(\dfrac{\d \ln f(X_1, \, \ldots, \, X_n; \, \theta)}{\d \theta}\right)^2 = \E \left(\dfrac{f'_\theta}{f} \right)^2 = \int[\mathcal{X}] \left(\dfrac{f'_\theta}{f} \right)^2 \cdot f \, d\mu = \int[\mathcal{X}] \dfrac{(f'_\theta)^2}{f} \, d\mu$$
		\end{proof}

		\item Пусть для плотности распределения $f(X_1, \, \ldots, \, X_n; \,\theta)$ выполняется равенство:
		$$\int[\mathcal{X}] \dfrac{\d}{\d\theta} f(X_1, \, \ldots, \, X_n; \, \theta) \, d\mu = \dfrac{\d}{\d\theta}\; \int[\mathcal{X}] f(X_1, \, \ldots, \, X_n; \, \theta) \, d\mu$$
		
		Тогда $I_n(\theta) = n I_1(\theta)$.
		
		\begin{proof}
			\slashns
			
			Поскольку $f$~--- плотность случайного распределения, интеграл по ней равен 1 и, соответственно, никаким образом не зависит от параметра $\theta$. Отсюда имеем:	
			$$\E \left(\dfrac{\d \ln f }{\d \theta} \right) =  \int[\mathcal{X}] \dfrac{f'_{\theta}}{f} \cdot f \, d\mu = \int[\mathcal{X}] \dfrac{\d}{\d \theta} f \, d\mu = \dfrac{\d}{\d\theta} \;\int[\mathcal{X}] f \, d\mu = 0$$
			
			Этим свойством мы воспользуемся дальше в доказательстве. На текущий же момент хотим по-другому записать определение информации Фишера.
			
			Распишем чуть подробнее $\dfrac{\d \ln f}{\d \theta}$:			
			$$\dfrac{\d \ln f(X_1, \, \ldots, \, X_n; \, \theta)}{\d \theta} = \dfrac{\d}{\d \theta} \ln \prod\limits_{i=1}^{n} f(X_i; \, \theta)= \dfrac{\d}{\d\theta} \sum\limits_{i=1}^{n} \ln f(X_i; \, \theta) = \sum\limits_{i=1}^{n} \left(\dfrac{\d}{\d \theta} \ln f(X_i; \, \theta)\right)$$
			
			Отсюда уже становится видно, каким образом свести $I_n(\theta)$ к $I_1(\theta)$. Просто выпишем определение информации Фишера, раскрыв квадрат суммы и воспользовавшись свойствами матожидания. Тогда получим:
			$$I_n(\theta) = \E \left(\dfrac{\d \ln f(X_1, \, \ldots, \, X_n)}{\d \theta} \right)^2 = \E\left[\sum\limits_{i=1}^{n} \left(\dfrac{\d}{\d\theta}\ln f(X_i; \, \theta)\right)\right]^2 = $$
			$$= \E \left[\sum\limits_{i=1}^{n} \left(\dfrac{\d}{\d \theta} \ln f(X_i; \, \theta) \right)^2 \right] + \E\left[\sum\limits_{i\ne j} \left(\dfrac{\d}{\d \theta} \ln f(X_i; \, \theta) \cdot \dfrac{\d}{\d \theta} \ln f(X_j; \, \theta) \right) \right] =$$
			$$= n I_1(\theta) + \sum\limits_{i \ne j} \left(\E \left[\dfrac{\d}{\d\theta}\ln f(X_i; \, \theta)\right] \cdot \E \left[\dfrac{\d}{\d\theta} \ln f(X_j; \, \theta)\right]\right) = nI_1(\theta)$$
		\end{proof}
	\end{enumerate}
	
\end{properties}

\begin{reminder}
	\slashns
	
	Из курса математического анализа мы знаем, что если $\frac{\d^k f}{\d \theta^k}(x, \theta)$ существует и, к примеру, непрерывно дифференцируема по параметру $\theta$, то верно равенство:
	\begin{equation}\label{eq:parameter_differentiability}
	\frac{\d^{k+1}}{\d \theta^{k+1}} \int[\mathcal{X}] f(x; \, \theta) \, d\mu = \int[\mathcal{X}] \frac{\d^{k+1}}{\d \theta^{k+1}} f(x; \, \theta) \, d\mu
	\end{equation}

	Тем не менее, вместо того, чтобы проверять непрерывную дифференцируемость функции, иногда может быть проще проверить такое равенство вручную.
\end{reminder}

\begin{theorem}
	\slashns
	
	$X_1, \, \ldots, \, X_n \sim \P_{\theta}$~--- произвольная выборка, $\theta \in \Theta \subset \R$
	
	$d \P_{\theta} = f(x, \theta) \, d\mu$
	
	$0 \ne I_1(\theta) < +\infty$
	
	$\dfrac1n \cdot \dfrac{\d^3 \ln L}{\d \theta^3}(X; \, \theta)$~--- ограничено по $\theta$
	
	И пусть выполняется условие \eqref{eq:parameter_differentiability} для первых двух производных.
	
	Тогда верны следующие утверждения:
	\begin{enumerate}
		\item ОМП состоятельна, т.е. $\widehat{\theta}_n \overset{\P_{\theta}}{\to} \theta \;\; \forall \theta \in \Theta$
		\item ОМП асимптотически нормальна, т.е. $\sqrt{n} \left(\widehat{\theta}_n - \theta \right) \overset{d}{\to} \N\left(0,\, \dfrac{1}{I_1(\theta)}\right) \;\; \forall \theta \in \Theta$
	\end{enumerate}
	
	\begin{proof}
		\slashnl
		
		\begin{enumerate}
			\item Без доказательства
			\item Разложим $\dfrac{\d \ln L}{\d \theta}(\mathbf{X}; \, \theta)$ в ряд Тейлора:
			$$\dfrac{\d \ln L}{\d \theta}(\mathbf{X}; \, \theta) = \dfrac{\d \ln L}{\d \theta}(\mathbf{X}; \, \theta_0) + (\theta - \theta_0) \cdot \dfrac{\d^2 \ln L}{\d \theta^2}(\mathbf{X}; \, \theta_0) + \dfrac{1}{2}(\theta - \theta_0)^2 \cdot \dfrac{\d^3 \ln L}{\d \theta^3}(\mathbf{X}; \, \tilde{\theta}), \;\;\; \tilde{\theta} \in (\theta_0, \,\theta)$$
			
			Подставим теперь вместо $\theta$ нашу оценку максимального правдоподобия $\widehat{\theta}$. Поскольку в точке $\widehat{\theta}$ достигается максимум функции правдоподобия, значение производной в ней равно 0. Т.е. получаем:
			$$0 = \dfrac{\d \ln L}{\d \theta}(\mathbf{X}; \, \theta_0) + (\widehat{\theta} - \theta_0) \left(\dfrac{\d^2 \ln L}{\d \theta^2}(\mathbf{X}; \, \theta_0) + \dfrac{1}{2}(\widehat{\theta} - \theta_0) \cdot \dfrac{\d^3 \ln L}{\d \theta^3}(\mathbf{X}; \, \tilde{\theta})\right), \;\;\; \tilde{\theta} \in (\theta_0, \,\theta)$$
			
			Попробуем вычленить из полученного выражения значение $\widehat{\theta} - \theta_0$, домноженное на $\sqrt{n}$.
			$$\sqrt{n} \left(\widehat{\theta} - \theta_0\right) = -\dfrac{\dfrac{1}{\sqrt{n}} \cdot \dfrac{\d\ln L}{\d \theta} (\mathbf{X}; \, \theta_0)}{\dfrac1n \cdot \dfrac{\d^2 \ln L}{\d \theta^2}(\mathbf{X}; \, \theta_0) + \dfrac{1}{2n}(\widehat{\theta} - \theta_0) \cdot \dfrac{\d^3 \ln L}{\d \theta^3}(\mathbf{X}; \, \tilde{\theta})}, \;\;\; \tilde{\theta} \in (\theta_0, \,\theta)$$
			
			Теперь можем пробовать отдельно рассматривать каждую из частей полученного выражения. Начнем с первой производной:
			$$\dfrac{\d \ln L}{\d \theta}(\mathbf{X}; \, \theta_0) = \dfrac{\d}{\d \theta} \sum\limits_{i=1}^{n} \ln f(X_i; \, \theta_0) =  \sum\limits_{i=1}^{n} \dfrac{f'_{\theta}}{f}(X_i; \, \theta_0)$$
			
			Получили сумму независимых случайных величин. Кроме того, нормировка $\frac{1}{\sqrt{n}}$ намекает нам на возможность использования центральной предельной теоремы. Остается посчитать для этого матожидание и дисперсию:
			$$\E \left[\dfrac{f'_{\theta}}{f}(X_i; \, \theta_0)\right] = \int[\mathcal{X}] \dfrac{f'_{\theta}}{f} \cdot f(x; \, \theta_0) \, d\mu = \int[\mathcal{X}] f'_{\theta}(x; \, \theta_0) \, d\mu = 0$$
			$$\D \left[\dfrac{f'_{\theta}}{f} (X_i; \, \theta_0)\right] = \E\left[\dfrac{f'_{\theta}}{f} (X_i; \, \theta_0) \right]^2 = \E \left[\dfrac{\d \ln f(X_i ;\, \theta_0)}{\d \theta} \right]^2= I_1(\theta_0)$$
			
			Воспользовавшись ЦПТ, получаем:
			$$\dfrac{1}{\sqrt{n}} \cdot \dfrac{\d \ln L}{\d \theta} (\mathbf{X}; \, \theta_0) \overset{d}{\to} \N(0, \, I_1(\theta_0))$$
			
			Мы теперь знаем, как распределен числитель. Остается лишь понять, что делать со знаменателем. Посчитаем для начала вторую производную:
			$$\dfrac{\d^2 \ln L}{\d \theta^2}(\mathbf{X}; \, \theta_0) = \dfrac{\d}{\d \theta} \left(\dfrac{\d \ln L}{\d \theta}(\mathbf{X}; \, \theta_0) \right) = \dfrac{\d}{\d\theta} \sum\limits_{i=1}^{n} \dfrac{f'_{\theta}}{f}(X_i; \, \theta_0) = \sum\limits_{i=1}^{n} \dfrac{f''_{\theta} \cdot f - (f'_{\theta})^2}{f^2}(X_i; \, \theta_0)$$
			
			Мы снова получили сумму независимых случайных величин. Нормировка $\frac{1}{n}$ намекает при этом на возможность использования закона больших чисел. Для этого посчитаем матожидание:
			$$\E \left[\dfrac{f''_{\theta} \cdot f - (f'_{\theta})^2}{f^2}(X_i; \, \theta_0) \right] = \E \left[\dfrac{f''_{\theta}}{f}(X_i; \, \theta_0)\right] - \E \left[\dfrac{f'_{\theta}}{f}(X_i; \, \theta_0)\right]^2 =$$
			$$= \int[\mathcal{X}] \dfrac{\d^2 f}{\d \theta^2} (x; \, \theta_0) \, d\mu - I_1(\theta_0) = \dfrac{\d^2}{\d \theta^2} \;\int[\mathcal{X}] f(x; \, \theta_0) \, d\mu - I_1(\theta_0) = -I_1(\theta_0)$$
			
			Получили, что первое слагаемое в знаменателе сходится по вероятности к $-I_1(\theta_0)$, т.е.
			$$\frac{1}{n} \cdot \dfrac{\d^2 \ln L}{\d \theta^2} (\mathbf{X}; \, \theta_0) \overset{\P_{\theta_0}}{\to} -I_1(\theta_0)$$
			
			Осталось рассмотреть второе слагаемое знаменателя. 
			
			C одной стороны, из первого пункта теоремы мы знаем, что оценка $\widehat{\theta}$ состоятельна, т.е. $\widehat{\theta} - \theta \overset{\P_{\theta}}{\to} 0$. С другой стороны, оставшийся сомножитель у нас ограничен по условию. А значит, всё слагаемое по вероятности стремится к 0.
			
			В итоге мы выяснили, что числитель по распределению сходится к $\N\left(0, \, I_1(\theta_0)\right)$, а знаменатель сходится по вероятности к $-I_1(\theta_0)$. А значит получаем:
			$$\sqrt{n} \left(\widehat{\theta} - \theta_0 \right) \overset{d}{\to} \frac{1}{I_1(\theta_0) }\N(0, \, I_1(\theta_0)) = \N\left(0, \, \dfrac{1}{I_1(\theta_0)}\right)$$			
		\end{enumerate}

	\end{proof}
\end{theorem}

\begin{remark}
	\slashns
	
	Приведенная выше теорема верна и для $\Theta \subset \R^k$. Доказательство ее отличается лишь использованием формулой Тейлора для многомерного случая.
\end{remark}

Таким образом, мы показали, что оценки максимального правдоподобия состоятельны и асимптотически нормальны. Осталось показать, что, ко всему прочему, они нередко являются еще и асимптотически эффективными. Для этого докажем следующую теорему.

\begin{theorem}[Неравенство Рао-Крам\'ера]
	\slashns
	
	$X_1, \, \ldots, \, X_n \sim \P_{\theta}$~--- произвольная выборка, $\theta \in \Theta$
	
	$0 \ne I_1(\theta) < \infty$ 
	
	И пусть выполняется условие $\eqref{eq:parameter_differentiability}$ для первой производной.
	
	Рассмотрим произвольную оценку $T$ параметра $\theta$.
	
	$\E_{\theta} T = \theta + b(\theta)$, где $b(\theta)$~--- смещение (от bias)
	
	Тогда $\E_{\theta}(T - \theta)^2 \ge \dfrac{(1 + b'(\theta))^2}{I_n(\theta)} + b^2(\theta)$
	
	\begin{proof}
		\slashns
		
		Заметим, что чтобы получить значение $1 + b'(\theta)$, нам всего лишь необходимо продифференцировать наше матожидание по параметру.
		
		Запишем тогда это значение в немного другом виде, воспользовавшись возможностью переставлять интеграл и производную местами:
		$$1 + b'(\theta) = (\theta + b(\theta))' = (\E_{\theta} T)' = \dfrac{\d}{\d\theta} \int[\mathcal{X}] T \cdot f(x; \, \theta) \, d\mu = \int[\mathcal{X}] T \cdot f'_{\theta}(x; \, \theta) \, d \mu =$$
		$$= \int[\mathcal{X}] T \cdot f'_{\theta}(x; \, \theta) \, d \mu - \E_{\theta} T \cdot 0 = \int[\mathcal{X}] (T - \E_{\theta }T) \cdot f'_{\theta}(x; \, \theta) \, d \mu$$
		
		Попробуем теперь получить некоторую оценку сверху непосредственно на значение $(1 + b'(\theta))^2$. Для этого чуть иначе запишем полученный интеграл, а затем воспользуемся неравенством Коши-Буняковского:
		
		$$(1 + b'(\theta))^2 = \left[\int[\mathcal{X}] (T - \E_{\theta }T) \cdot f'_{\theta} \; d \mu\right]^2 =  \left[\int[\mathcal{X}] (T - \E_{\theta }T) \cdot \sqrt{f} \cdot \dfrac{f'_{\theta}}{\sqrt{f}} \, d \mu\right]^2 \overset{\text{КБ}}{\le}$$
		$$\overset{\text{КБ}}{\le} \int[\mathcal{X}] \Big((T - \E_{\theta}T) \cdot \sqrt{f} \Big)^2 \, d\mu \cdot \int[\mathcal{X}] \left(\dfrac{f'_{\theta}}{\sqrt{f}} \right)^2 \, d\mu$$
		
		Заметим, что левый интеграл~--- это в точности дисперсия $\D_{\theta} (T - \E_{\theta}T) = \E_{\theta} (T - \E_{\theta}T)^2$ отклонения нашей оценки. Правый интеграл~--- это информация Фишера. А потому имеем:
		$$(1 + b'(\theta))^2 \le \E_{\theta} (T - \E_{\theta}T)^2 \cdot I_n(\theta)$$
		$$\dfrac{(1+b'(\theta))^2}{I_n(\theta)} \le \E_{\theta}(T - \E_{\theta}T)^2 = \E_{\theta}\Big((T - \theta) - b(\theta)\Big)^2 = \E_{\theta} (T - \theta)^2 - 2b(\theta) \E_{\theta}(T - \theta) + b^2(\theta)$$
		
		Вспомнив теперь, что $\E_{\theta}(T - \theta) = b(\theta)$, получаем оценку снизу на $\E_{\theta} (T - \theta)^2$:
		$$\dfrac{(1 + b'(\theta))^2}{I_n(\theta)} \le \E_{\theta}(T - \theta)^2 - b^2(\theta) \implies \E_{\theta}(T - \theta)^2 \ge \dfrac{(1 + b'(\theta))^2}{I_n(\theta)}  + b^2(\theta)$$
		
		\if 0
		$\int f' \, d\mu = 0$
		
		$\E_{\theta} T = \int T \cdot f \, d\mu$
		
		$\left(\theta + b(\theta) \right)' = \left(\E_{\theta} T\right)' = \int T \cdot f' \, d\mu =  \int T \cdot f' \, d \mu - \E_{\theta} T \cdot 0 = \int \left(T - \E_{\theta} T \right) \cdot f' \, d\mu =$
		
		$ = \int (T - \E_{\theta} T) \cdot \sqrt{f} \cdot \frac{f'}{\sqrt{f}} \, d\mu$
			
		$(1 + b'(\theta) )^2 = \left( \int ((T - \E_{\theta}T) \cdot \sqrt{f}) \cdot \left(\frac{f'}{\sqrt{f}} \right) \, d\mu\right)^2  \int \left(T - \E_{\theta} T \right)^2 \cdot f \, d\mu \cdot \int \left(\frac{f'}{\sqrt{f}} \right)^2 \, d\mu =$
		
		$= \int(T - \E_{\theta} T) \cdot \sqrt{f} \cdot \frac{f'}{\sqrt{f}} \, d\mu$
		
		$\frac{(1 + b'(\theta))^2}{I_n(\theta)} \le \E_{\theta} (T - \E_{\theta}T)^2 = \ldots$ 
		\fi
	\end{proof}
\end{theorem}

\begin{consequence}
	\slashns
	
	Если в условии теоремы $b(\theta) \equiv 0$, то $\E_{\theta}(T - \theta)^2 \ge \dfrac{1}{I_n(\theta)}$
	
	\begin{proof}
		$$\E_{\theta}(T - \theta)^2 \ge \frac{(1 + b'(\theta))^2}{I_n(\theta)} + b^2(\theta) \overset{b, \,b' \equiv 0}{=}
		\frac{1}{I_n(\theta)}$$
	\end{proof}
\end{consequence}

\begin{consequence}
	\slashns
	
	Если в условии теоремы $T$~--- несмещенная оценка максимального правдоподобия, причем условие \eqref{eq:parameter_differentiability} выполняется вплоть до второй производной, то оценка $T$ асимптотически эффективна.
	
	\begin{proof}
		\slashns
		
		Мы знаем, что наша оценка несмещенная. А потому $\D(T - \theta)$ оценивается снизу как $I_n^{-1}(\theta)$. 
		
		Для удобства же будем сейчас рассматривать дисперсию величины $\sqrt{n}\left(T - \theta\right)$:
		$$\D\left(\sqrt{n} \left(T- \theta \right) \right) = \E \left(\sqrt{n} \left(T - \theta \right) \right)^2 = n \E\left(T - \theta \right)^2 \ge n \cdot \frac{1}{I_n(\theta)} = \frac{1}{I_1(\theta)}$$
		
		Вспомним, что оценка максимального правдоподобия является асимптотически нормальной:
		$$\sqrt{n} \left(T - \theta \right) \overset{d}{\to} \N\left(0, \frac{1}{I_1(\theta)} \right)$$
		
		Таким образом получили, что дисперсия отклонения нашей оценки сходится к своей нижней границе. А значит, несмещенная оценка максимального правдоподобия является асимптотически эффективной.
		
	\end{proof}
\end{consequence}