\Section{Регрессионный анализ}{Антон Ермилов}

\Subsection{Линейная регрессия}

\begin{preamble}
	\slashns
	
	Очень часто нам хочется определить, как зависит наблюдаемая случайная величина от каких-то других случайных величин. При этом в реальности так получается, что исследуемые зависимости почти всегда являются лишь статистическими, но не функциональными.
	
	К примеру, довольно типичной задачей является определение зависимости стоимости квартиры от каких-либо ее параметров: площади, этажа, района. При равных значениях независимых переменных стоимость квартиры может отличаться. По сути, она является некоторой случайной величиной, зависящей от входных параметров. А потому речь здесь может идти лишь о статистической зависимости.
	
	Собственно, регрессионный анализ как раз и занимается подобным исследованием поведения зависимой случайной величины от набора различных независимых переменных. 
	
	Попробуем теперь формально определить понятие регрессии.
\end{preamble}

\begin{definition}
	\slashns
	
	Пусть $\vec{X} = (X_1, \, \ldots, \, X_k)^T$~--- набор независимых случайных величин, а $Y$~--- произвольная случайная величина, зависящая от $\vec{X}$.
	
	Тогда функция $f(\vec{x}) = \E(Y \, | \,\vec{X} = \vec{x})$ называется регрессией $Y$ на $\vec{X}$.
\end{definition}

\vspace*{3pt}
Разумеется, в общем случае $Y$ может вести себя как угодно в зависимости от значений независимых переменных $\vec{X}$. А потому хочется для начала рассмотреть какую-нибудь простую модель регрессии.

\begin{definition}
	\slashns
	
	Будем считать, что поведение $Y$ описывается следующим образом:
	$$Y \, | \, \vec{X} = \vec{X}^T \vec{\theta} + \eps$$
	
	Здесь $\vec{\theta}$~--- набор из $k$ параметров, который нам неизвестен, но заранее фиксирован. Именно его мы и будем пытаться найти.
	
	Величина $\eps$ при этом является некоторой случайной величиной (шумом), которая обладает следующими свойствами:
	$$\E\eps = 0, \;\; \D \eps = \sigma^2, \text{ причем } \sigma^2 \text{ нам неизвестна}$$
	
	Сама регрессия в таком случае будет иметь вид
	$$f(\vec{x}) := \E(Y \, | \, \vec{X} = \vec{x}) = \vec{x}^{\,T} \vec{\theta}$$
	
	Такая модель регрессии, в которой значение функции $f$ линейно зависит от значений $\vec{X}$, называется линейной регрессией.
\end{definition}

\begin{remark}
	\slashns
	
	Рассмотрим все тот же пример с квартирой. 
	
	В этом случае значения $X_i$~--- это некоторые количественные параметры квартиры: площадь, этаж, год постройки здания и т.п.
	
	Соответственно, в модели линейной регрессии мы предполагаем, что стоимость квартиры имеет некоторую линейную зависимость от значений наших переменных. Но при этом допускаем отклонения стоимости в ту и другую сторону.
\end{remark}

\begin{remark}
	\slashns
	
	Далее мы везде будем работать с параметрами и независимыми переменными как с векторами. Явно при этом обозначение их ``векторной природы'' использоваться не будет.
\end{remark}

\vspace*{3pt}
Мы ввели понятие линейной регрессии и даже определили задачу: найти набор параметров $\theta$, который бы описывал поведение зависимой переменной $Y$. Вопрос лишь в том, как этот набор параметров находить.

Как и всегда, будем пробовать восстанавливать параметры на основе наблюдений.

\begin{statement} % TODO statement?
	\slashns
	
	Пусть $(X_1, Y_1), \, \ldots, \, (X_n, Y_n)$~--- наши наблюдения.
	
	Тогда в нашей линейной модели вектор значений $\mathbf{Y}$ представим следующим образом:
	$$\mathbf{Y} = \mathbf{X} \cdot \theta + \eps$$
	$$, \text{ где } \mathbf{X} = \begin{pmatrix}
	X_1^T\\\vdots\\X_n^T
	\end{pmatrix}, \;\; \eps = \begin{pmatrix}
	\eps_1\\\vdots\\\eps_n
	\end{pmatrix}, \;\; \E\eps_i \eps_j = 0 \text{ при } i \ne j$$
	
	Заметим, что если бы мы могли ``забыть'' про существующий шум $\eps$ и не учитывать его, то мы бы получили СЛАУ, которую можно было бы попробовать решить с помощью того же метода Гаусса.
	
	Тем не менее, обычно количество наших наблюдений ($n$) оказывается сильно больше, чем количество параметров ($k$). А потому такая система в общем случае вообще не будет иметь решения.
	
	\begin{center}
		\begin{tikzpicture}[scale=0.725]
			\draw[->] (-1, 0) -- (5, 0) node[right] {$X$};
			\draw[->] (0, -1) -- (0, 3.5) node[above] {$Y$};
			\draw (-1, -0.66) -- (4.5, 3);
			\draw[fill] (1, 0.3) circle [radius=0.1];
			\draw[fill] (1.5, 0.4) circle [radius=0.1];
			\draw[fill] (2.5, 2.2) circle [radius=0.1];
			\draw[fill] (3.9, 3.3) circle [radius=0.1];
			\draw[fill] (3.2, 1.7) circle [radius=0.1];
			\draw[fill] (0.3, 0.7) circle [radius=0.1];
			\draw[fill] (-0.5, -0.8) circle [radius=0.1];
			\node at (5.4, 3.6) {$k < n$} ;
		\end{tikzpicture}
	\end{center}
	
	Тогда возникает другая идея: искать набор параметров $\theta$ таким образом, чтобы минимизировать сумму квадратов отклонений значений зависимой переменной $Y$ от нашей прямой (или гиперплоскости в случае пространства большей размерности).
	
	\begin{center}
		\begin{tikzpicture}[scale=0.725]
		\draw[->] (-1, 0) -- (5, 0) node[right] {$X$};
		\draw[->] (0, -1) -- (0, 3.5) node[above] {$Y$};
		\draw (-1, -0.66) -- (4.5, 3);
		\draw[fill] (1, 0.3) circle [radius=0.1];
		\draw[ultra thick] (1, 0.3) -- (1, 0.66);
		\draw[fill] (1.5, 0.4) circle [radius=0.1];
		\draw[ultra thick] (1.5, 0.4) -- (1.5, 1);
		\draw[fill] (2.5, 2.2) circle [radius=0.1];
		\draw[ultra thick] (2.5, 2.2) -- (2.5, 1.63);
		\draw[fill] (3.9, 3.3) circle [radius=0.1];
		\draw[ultra thick] (3.9, 3.3) -- (3.9, 2.6);
		\draw[fill] (3.2, 1.7) circle [radius=0.1];
		\draw[ultra thick] (3.2, 1.7) -- (3.2, 2.13);
		\draw[fill] (0.3, 0.7) circle [radius=0.1];
		\draw[ultra thick] (0.3, 0.7) -- (0.3, 0.2);
		\draw[fill] (-0.5, -0.8) circle [radius=0.1];
		\draw[ultra thick] (-0.5, -0.8) -- (-0.5, -0.33);
		\node at (5.4, 3.6) {$k < n$} ;
		\end{tikzpicture}
	\end{center}
	
	Формально, хочется найти такой набор параметров $\theta$, который бы минимизировал функцию
	$$S(\theta) = \sum\limits_{i=1}^{n} \left(Y_i - X_i^T \theta\right)^2 = \norm{\mathbf{Y} - \mathbf{X} \,\theta}^2$$
\end{statement}

\begin{definition}
	\slashns
	
	Оценка $\theta^*$, такая что $S(\theta^*) = \min\limits_{\theta} S(\theta)$, называется оценкой наименьших квадратов (ОНК).
\end{definition}

\vspace*{3pt}

Осталось понять, каким образом эту оценку получить. Для начала введем следующее обозначение и докажем пару связанных с ним утверждений.

\begin{denotation}
	\slashns
	
	Введем следующее обозначение для вектора из частных производных:
	
	$$\frac{d f}{d\theta} :=\begin{pmatrix}
	\dfrac{\d f}{\d \theta_1}\\
	\vdots\\
	\dfrac{\d f}{\d \theta_k}
	\end{pmatrix}$$
\end{denotation}

\begin{statement}
	\slashns
	
	Пусть $\alpha$~--- произвольный вектор из $k$ компонент. Тогда верно равенство
	$$\dfrac{d}{d \theta} \Big(\alpha^{T} \theta \Big) = \dfrac{d}{d\theta} \Big(\theta^{T} \alpha\Big) = \alpha$$
	
	\begin{proof}
		\slashns
		
		Поскольку $\alpha^{T} \theta = \sum\limits_{i=1}^{k} \alpha_i \theta_i = \theta^{T} \alpha$, то частная производная по $\theta_i$ будет в точности равна $\alpha_i$.
	\end{proof}
\end{statement}

\begin{statement}
	\slashns
	
	Пусть $A$~--- симметричная матрица, т.е. $A^T = A$. Тогда верно равенство
	$$\dfrac{d}{d \theta} \left(\theta^{T}  A  \theta \right) = 2 A \theta$$
	
	\begin{proof}
		\slashns
		
		Распишем нашу квадратичную форму:
		$$\theta^{T}  A  \theta = \sum\limits_{i, j} A_{i,j} \theta_i \theta_j$$
		
		Остается лишь посмотреть на вид ее частной производной по параметру $\theta_i$ и убедиться, что она совпадает с $i$-ой строкой матрицы $2A\theta$:
		$$\dfrac{\d}{\d \theta_i} \left(\sum\limits_{i, j} A_{i,j} \theta_i \theta_j \right) = \sum\limits_{j=1}^{k} (A_{i,j} + A_{j,i}) \, \theta_j = 2 \sum\limits_{j=1}^{k} A_{i,j} \theta_j = \Big(2A\theta\Big)_i$$
	\end{proof}
\end{statement}

\vspace*{3pt}
Перейдем теперь к нахождению оценки наименьших квадратов.

\begin{statement}
	\slashns
	
	Пусть $(X_1, Y_1), \, \ldots, \, (X_n, Y_n)$~--- наши наблюдения.
	
	Тогда ОНК $\theta^*$ достигается на любом решении системы $\mathbf{X}^T\mathbf{X} \, \theta = \mathbf{X}^T\mathbf{Y}$.
	
	В частности, если матрица $\mathbf{X}^T \mathbf{X}$ обратима, ОМП имеет вид $\theta^* = (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \mathbf{Y}$.
	
	\begin{proof}
		\slashns
		
		Распишем чуть иначе нашу функцию $S(\theta)$:
		$$S(\theta) = \norm{\mathbf{Y} - \mathbf{X} \,\theta}^2 = \Big<\mathbf{Y} - \mathbf{X}  \,\theta, \, \mathbf{Y} - \mathbf{X}  \,\theta\Big> = \theta^{T} \,\mathbf{X}^T \mathbf{X} \,\theta - 2 \mathbf{Y}^{T} \,\mathbf{X}\, \theta + \mathbf{Y}^{T}\,\mathbf{Y}$$
		
		Такая функция всегда достигает своего минимума, поскольку полученная квадратичная форма неотрицательно определена. В частности, в точке минимума значение производной нашей функции равно 0. 
		
		Посмотрим для начала на то, как наша производная вообще выглядит:
		$$\dfrac{d S(\theta)}{d \theta} = 2\mathbf{X}^T \mathbf{X} \, \theta - 2\mathbf{X}^T\mathbf{Y}$$
		
		Отсюда уже видно, что нас интересуют решения системы $\mathbf{X}^T\mathbf{X} \, \theta = \mathbf{X}^T\mathbf{Y}$.
		
		А тогда если $\mathbf{X}^T\mathbf{X}$ обратима, то решение единственно, и наша ОНК принимает вид
		$$\theta^* = (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \mathbf{Y}$$
		
		Остается показать, что если матрица $\mathbf{X}^T\mathbf{X}$ необратима, то нам подойдет любое решение системы.
		
		Для этого рассмотрим какое-нибудь решение $\theta^*$. И покажем, что при любом другом значении параметра $\theta$ мы получим решение не лучше:
		$$S(\theta) = \norm{\mathbf{Y} - \mathbf{X} \, \theta}^2 = \norm{\mathbf{Y} - \mathbf{X} \, \theta^* + \mathbf{X}\,(\theta^* - \theta)}^2 =$$
		$$= \norm{\mathbf{Y} - \mathbf{X} \, \theta^*}^2 + \norm{\mathbf{X}\,(\theta^* - \theta)}^2 + 2 \Big<\mathbf{X}\,(\theta^* - \theta), \mathbf{Y} - \mathbf{X} \,\theta^*\Big> = $$
		$$= \norm{\mathbf{Y} - \mathbf{X} \, \theta^*}^2 + \norm{\mathbf{X}\,(\theta^* - \theta)}^2 + 2(\theta^* - \theta)^T \Big(\mathbf{X}^T\mathbf{Y} - \mathbf{X}^T\mathbf{X} \,\theta^*\Big) =$$
		$$= \norm{\mathbf{Y} - \mathbf{X} \, \theta^*}^2 + \norm{\mathbf{X}\,(\theta^* - \theta)}^2 + 0 \ge \norm{\mathbf{Y} - \mathbf{X} \, \theta^*}^2 = S(\theta^*)$$
	\end{proof}
\end{statement}

\begin{remark}
	\slashns
	
	Поскольку выбор матрицы $\mathbf{X}$ в нашей власти (она формируется из наших наблюдений), то всегда можно добиться того, чтобы матрица $\mathbf{X}^T\mathbf{X}$ была обратима. Хотя с точки зрения нахождения решения разницы никакой не будет (придется или обращать матрицу, или находить явно какое-нибудь решение с помощью того же метода Гаусса).
\end{remark}

\vspace*{3pt}
Таким образом, мы получили некоторую оценку параметров для нашей модели линейной регрессии. Вопрос лишь в том, насколько наша оценка в действительности хорошая.

\begin{definition}
	\slashns
	
	Пусть $\theta_1$ и $\theta_2$~--- оценки набора параметров $\theta$.
	
	Тогда $\theta_1$ эффективнее $\theta_2$, если $\cov(\theta_2) - \cov(\theta_1)$~--- неотрицательно определенная матрица.
	
	Здесь $\cov(\theta)$~--- ковариационная матрица случайного вектора $\theta$.
\end{definition}

\begin{remark}
	\slashns
	
	Данное определение эффективности является обобщением соответствующего определения для одномерного случая.
	
	Только если раньше мы пытались оценивать дисперсию нашей оценки, то теперь можем пробовать оценивать ее ``многомерный вариант'', т.е. ковариационную матрицу.
\end{remark}

\begin{remark}
	\slashns
	
	Введем отношение порядка на матрицах как ``$A \le B$,  если $B - A$ неотрицательно определена''.
	
	Тогда оценка $\theta_1$ эффективнее оценки $\theta_2$, если $\cov(\theta_1) \le \cov(\theta_2)$.
\end{remark}

\begin{theorem}[Gauss, Markov]
	\slashns
	
	Пусть $(X_1, Y_1), \, \ldots, \, (X_n, Y_n)$~--- наши наблюдения, и существует $(\mathbf{X}^T\mathbf{X})^{-1}$.
	
	Причем будем рассматривать модель, в которой $Y$ имеет распределение $Y \, | \, X = X^T \theta + \eps$.
	
	Тогда $\theta^* = (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \mathbf{Y}$~--- наилучшая линейная несмещенная оценка параметра $\theta$.
	
	В английской литературе она известна как best linear unbiased estimator, или попросту BLUE.
	
	\begin{proof}
		\slashns
		
		Линейность нашей оценки очевидна (делаем линейные преобразования с элементами из $\mathbf{Y}$).
		
		Проверим тогда несмещенность нашей оценки $\theta^*$:
		$$\E_{\theta}\theta^* = \E_{\theta} \Big[(\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T\mathbf{Y} \Big] = \E_{\theta} \Big[(\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T (\mathbf{X}\,\theta + \eps)\Big]  =$$
		$$= \E_{\theta} \theta + \E_{\theta}\Big[(\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \eps \Big]= \theta + (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \E_{\theta} \eps = \theta$$
		
		Последний переход здесь верен в силу линейности матожидания и того, что распределение $\mathbf{X}$ никак не зависит от $\theta$.
		
		Осталось проверить эффективность. Для этого возьмем какую-нибудь другую линейную несмещенную оценку $\tilde{\theta}$ и покажем, что она не лучше.
		
		Во-первых, эта оценка будет иметь вид $\tilde{\theta} = L \mathbf{Y}$. А так как мы хотим, чтобы эта оценка была несмещенной, то матрица $L$ должна удовлетворять условию $\E_{\theta} (L\mathbf{Y}) = \theta \;\; \forall \theta$, то есть:
		$$\forall \theta : \; \theta = \E_{\theta}(L\mathbf{Y}) = \E_{\theta}(L \mathbf{X} \, \theta + L \eps) = L \mathbf{X} \, \theta$$ 
		$$\implies L \mathbf{X} = E_k, \text{ где } E_k \text{~--- единичная матрица размера } k$$
		
		Посмотрим теперь на ковариационную матрицу $\theta^*$:
		$$\cov(\theta^*) = \E_{\theta} \Big[(\theta^* - \theta)(\theta^* - \theta)^T\Big]=$$
		$$= \E_{\theta}\Big[\Big((\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T (\mathbf{X}\,\theta + \eps) - \theta\Big) \Big((\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T (\mathbf{X}\,\theta + \eps) - \theta \Big)^T \Big] =$$
		$$= \E_{\theta}\Big[\Big((\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps\Big) \Big((\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps\Big)^T \Big] =\E_{\theta}\Big[(\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps \eps^T \mathbf{X} (\mathbf{X}^T \mathbf{X})^{-1} \Big] \; \textcolor{red}{=}$$
		
		Заметим, что в последнем выражении случайной является только величина $\eps\eps^T$. А потому всю остальную часть по линейности можно вынести из-под матожидания.
		$$\textcolor{red}{=} \; (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T \cdot \E_{\theta}(\eps\eps^T) \cdot \mathbf{X} (\mathbf{X}^T \mathbf{X})^{-1} \; \textcolor{red}{=}$$
		
		Заметим, что $\E(\eps\eps^T)$~--- это, по сути, ковариационная матрица ``шумов''. Более того, на диагонали этой матрицы будут стоять дисперсии шумов, а в остальных позициях~--- нули, поскольку различные шумы не коррелируют между собой.		
		$$\textcolor{red}{=} \; (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T \cdot \sigma^2 E_n \cdot \mathbf{X} (\mathbf{X}^T \mathbf{X})^{-1} = \sigma^2 \cdot (\mathbf{X}^T \mathbf{X})^{-1}$$
		
		Таким образом, мы смогли посчитать ковариационную матрицу для оценки $\theta^*$.
		
		Осталось посчитать ковариационную матрицу для $\tilde{\theta} = L \mathbf{Y}$, при условии что $L \mathbf{X} = E_k$.
		
		Заведем для начала следующее обозначение:
		$$C := L - (\mathbf{X}^T \mathbf{X})^{-1} \mathbf{X}^T \implies
		 L = C + (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T$$
		
		Теперь попробуем расписать саму ковариационную матрицу $\cov(\tilde{\theta})$:
		$$\cov(\tilde{\theta})= \E_{\theta} \Big[(\tilde{\theta} - \theta)(\tilde{\theta} - \theta)^T \Big] = \E_{\theta} \Big[(L \mathbf{Y} - \theta)(L\mathbf{Y} - \theta)^T \Big] =$$
		$$= \E_{\theta} \Big[\Big((C + (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T) (\mathbf{X} \, \theta + \eps) - \theta\Big) \Big((C + (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T) (\mathbf{X} \, \theta + \eps) - \theta\Big)^T \Big] =$$
		$$= \E_{\theta} \Big[\Big(C\mathbf{X} \, \theta + C \eps + (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps \Big) \Big(C\mathbf{X} \, \theta + C \eps + (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps \Big)^T\Big] \; \textcolor{red}{=}$$
		
		Из определения $C$ и условий на $L$ заметим, что $C \mathbf{X} = 0$. Тогда:
		$$\textcolor{red}{=} \; \E_{\theta} \Big[\Big(C \eps + (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps \Big)\Big(C \eps + (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps \Big)^T \Big] =$$
		$$= \E_{\theta} \Big[C \eps \eps^T C^T + (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps \eps^T C^T + C \eps \eps^T\mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} + (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\eps\eps^T\mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1}\Big] \;\textcolor{red}{=}$$
		
		По линейности матожидания попробуем оценить каждое слагаемое по отдельности. 
		$$\textcolor{red}{=} \; \sigma^2 \cdot CC^T + \Big[\sigma^2 \cdot (\mathbf{X}^T\mathbf{X})^{-1}\textcolor{blue}{\mathbf{X}^TC^T} + \sigma^2 \cdot \textcolor{blue}{C \mathbf{X}}(\mathbf{X}^T\mathbf{X})^{-1}\Big] + \sigma^2 (\mathbf{X}^T\mathbf{X})^{-1} =$$
		$$= \sigma^2 \cdot CC^T  + 0 + \sigma^2 (\mathbf{X}^T\mathbf{X})^{-1} = \cov(\theta^*) + \sigma^2 \cdot CC^T \ge \cov(\theta^*)$$
		
		Таким образом мы получили, что наша оценка $\theta^*$ является эффективной, что и требовалось.
	\end{proof}
\end{theorem}

Напоследок получим еще один маленький результат. А именно, попробуем оценить дисперсию шумов, которая изначально была нам неизвестна.

\begin{statement}
	\slashnss
	
	$$\dfrac{1}{n-k} S(\theta^*)\text{~--- несмещенная оценка } \sigma^2$$
	
	\begin{proof}
		\slashns
		
		Оценим матожидание величины $S(\theta^*)$:
		$$\E_{\theta, \sigma} S(\theta^*) = \E_{\theta, \sigma} \Big[(\mathbf{Y} - \mathbf{X} \, \theta^*)^T (\mathbf{Y} - \mathbf{X} \, \theta^*) \Big] =$$
		$$= \E_{\theta, \sigma} \Big[\Big(\mathbf{X} \, \theta + \eps - \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T (\mathbf{X} \theta + \eps) \Big)^T \Big(\mathbf{X} \, \theta + \eps - \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T (\mathbf{X} \theta + \eps) \Big)\Big] =$$
		$$= \E_{\sigma} \Big[\Big(\eps - \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \eps \Big)^T\Big(\eps - \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \eps \Big) \Big] =$$
		$$= \E_{\sigma} \Big[\eps^T \Big(E_n - \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \Big)^T\Big(E_n - \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \Big) \eps\Big] =: \E_{\sigma} \left(\eps^T B^T B \eps\right)$$
		
		Поскольку наши шумы независимы, а их дисперсия совпадает, то можем переписать полученное матожидание в следующем виде:
		$$\E_{\sigma} \left(\eps^T B^T B \eps\right) = \sum\limits_{i, j} (B^TB)_{i,j} \E_{\sigma} \eps_i \eps_j = \sum\limits_{i=1}^{n} (B^TB)_{i,i} \E_{\sigma} \eps_i^2 = \sigma^2 \cdot \tr(B^TB)$$
		
		Осталось понять, что след нашей матрицы будет равен $n - k$. Действительно:
		$$B^TB = E_n - 2 \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T + \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \cdot \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T = E_n - \mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T$$
		$$\tr(B^TB) = \tr(E_n) - \tr(\mathbf{X} (\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T) \overset{\tr(AB) \;=\; \tr(BA)}{=} n - \tr((\mathbf{X}^T\mathbf{X})^{-1} \mathbf{X}^T \mathbf{X}) = n - k$$
	\end{proof}
\end{statement}