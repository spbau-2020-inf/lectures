\Section{Моделирование распределений}{Антон Ермилов}

Напоследок обсудим следующую тему, связанную с моделированием распределений.

Разумеется, сейчас существует большое количество различных библиотек, умеющих моделировать многие стандартные распределения. Тем не менее, какие-то более хитрые распределения приходится реализовывать самостоятельно, и именно об этом будет рассказываться в этой главе.

Далее все предлагаемые методы будут основываться на предположении, что мы некоторым образом умеем генерировать выборки из стандартного равномерного распределения.

\Subsection{Метод обратной функции}

\begin{denotation}
	\slashns
	
	Пусть $\F$~--- функция распределения желаемой случайной величины $X$.
	
	Определим функцию $\text{G} : (0, 1) \to \R$ следующим образом:
	$$\text{G}(y) := \inf\limits_{x} \{x : \F(x) > y\}$$
\end{denotation}

\begin{lemma}
	\slashns
	
	$\text{G}(y) < x \iff y < \F(x)$
	
	\begin{proof}
		\slashns
		
		Обозначим $A_y = \{x \in \R: \F(x) > y \}$ и положим $z = \text{G}(y)$.
		
		Заметим, что т.к. $\F$ монотонно неубывает, то любое значение, большее чем $z$, лежит в множестве. Само же $z$ может как лежать в нем, так и не лежать. То есть, $A_y = \langle z, +\infty)$.
		
		Кроме того, $\F(z) \le y$, т.к. иначе по непрерывности слева нашлась бы такая точка $z^* < z$, что $\F(z^*) > y$. А это противоречит определению $z = \text{G}(y)$.
		
		Таким образом мы получили, что на самом деле $A_y = (z, +\infty)$. А поскольку при заданном $y$ каждое из условий $z = G(y) < x$ и $y < F(x)$ задает одно и то же множество, то эти условия равносильны.
	\end{proof}
\end{lemma}

\begin{lemma}
	\slashns
	
	$\text{G}(\text{U}[0, 1]) \sim X$, где $X$~--- моделируемая случайная величина
	
	\begin{proof}
		\slashns
		
		Пусть $\alpha \sim \text{U}[0, 1]$.
		
		Тогда $\P(\text{G}(\alpha) < t) = \P(\alpha < \F(t)) = \F(t)$.
	\end{proof}
\end{lemma}

Две приведенные выше леммы показывают, каким образом моделировать произвольные случайные величины в случае, когда мы можем посчитать функцию $G$. По сути, мы таким образом просто свели генерацию случайной выборки из заданного распределения к генерации выборки из стандартного равномерного распределения.

Кроме того, в некоторых случаях $G$ вычисляется довольно просто. А именно, если $\F$ обратима и мы можем легко вычислить обратную функцию.

Рассмотрим несколько примеров, чтобы лучше разобраться в этом.

\newpage
\begin{examples}
	\slashn
	
	\begin{enumerate}
		\item Пусть $X \sim \text{Exp}(\lambda)$, $\alpha \sim \text{U}[0, 1]$.
		
		Мы знаем, что функция распределения нашей случайной величины имеет вид 
		$$\F(x) = 1 - e^{-\lambda x}, \, x > 0$$
		
		Легко проверить, что обратная функция будет, соответственно, иметь вид 
		$$\text{G}(y) = -\dfrac{\ln(1-y)}{\lambda}$$
		
		А тогда можем моделировать нашу случайную величину по правилу $$-\dfrac{\ln(1-\alpha)}{\lambda} \sim \text{Exp}(\lambda)$$
		
		\item
		Пусть $X \sim \text{Cauchy}$.
		
		Мы знаем, что функция распределения нашей случайной величины имеет вид 
		$$\F(x) = \dfrac12 + \dfrac1\pi \arctan x$$
		
		Для этого распределения обратная функция тоже считается довольно просто:
		$$\text{G}(y) = \tan\left(\pi\left(y - \dfrac12 \right) \right)$$
		
		Само же распределение моделируется все тем же способом на основе значений $\alpha \sim \text{U}[0, 1]$.
		
		\item
		Пусть $X \sim \N(0,1)$.
		
		Поскольку функция распределения $\Phi$ не имеет хорошего представления (вообще говоря, является некоторым интегралом), то с ходу и непонятно, каким образом искать обратную функцию $\text{G} = \Phi^{-1}$.
		
		Потому воспользуемся трюком и научимся генерировать сразу пару значений из нормального распределения. А именно, научимся получать стандартный гауссовский вектор $(\xi, \eta)$, где $\xi, \eta \sim \N(0,1)$ и независимы.
		
		Для этого перейдем к полярным координатам: $(\xi, \eta) \to (\rho, \phi)$, где $\xi = \rho \cos \phi, \, \eta = \rho \sin \phi$.
		
		Тогда если в старых координатах плотность гауссовского вектора имела вид
		$$f_{(\xi, \eta)}(x, y) = \dfrac{1}{2\pi} e^{-\frac12(x^2 + y^2)},$$
		
		то в новых координатах плотность будет иметь вид
		$$f_{(\rho, \phi)}(r, \alpha) = \dfrac{1}{2\pi }e^{-\frac12 r^2} \cdot r.$$
		
		Заметим, что если распределения $\phi$ и $\rho$ у нас независимы, то плотность их совместного распределения попросту равна произведению плотностей этих распределений.
		
		Тогда скажем, что $f_{\phi}(\alpha) = \dfrac{1}{2\pi}$, $f_{\rho}(r) = re^{-\frac12 r^2}$.
		
		Очевидно, что тогда в качестве $\phi$ мы можем просто выбрать $\text{U}[0, 2\pi]$. Осталось разобраться, как получить распределение $\rho$ по его плотности. Для этого взглянем на его функцию распределения:
		$$\F_{\rho}(r) = 1 - e^{-\frac12r^2} \implies G(y) = \sqrt{-2\ln(1-y)} \overset{\alpha \sim \text{U}[0,1]}{\implies} G(\alpha) = \sqrt{-2\ln(1-\alpha)} \overset{\zeta \sim \text{Exp}(1)}{\sim} \sqrt{2\zeta}$$
		
		Таким образом, мы научились генерировать сразу пару значений из $\N(0, 1)$ как
		$$\xi = \sqrt{2\zeta} \cdot \cos \phi, \; \eta = \sqrt{2 \zeta} \cdot \sin \phi, \text{ где } \zeta \sim \text{Exp}(1), \, \phi \sim \text{U}[0, 2\pi]$$
	\end{enumerate}
\end{examples}

\Subsection{Метод отбора}

Предположим, что плотность $p(x)$ моделируемой случайной величины $\eta$ существует на некотором отрезке $[a, b]$. И пусть при этом плотность ограничена. То есть:
$$p(x) = 0 \text{ при } x \notin [a, b], \;\;\; p(x) \le M$$

Приведем алгоритм  моделирования распределении для такой ситуации.

\begin{algorithm}
	\slashns
	
	Представим себе график плотности. В силу описанных условий, его ненулевая часть полностью помещается в прямоугольник $[a, b] \times [0, M]$.
	
	\TODO picture
	
	Выберем внутри этого прямоугольника случайную точку $(\xi_1, \xi_2) \sim \text{U}\big([a, b]\times [0, M]\big)$. 
	
	Если $p(\xi_1) > \xi_2$, т.е. точка попала в подграфик $p$, то положим $\eta = \xi_1$. 
	
	Иначе повторим предыдущий шаг.
	
	\TODO one more picture
\end{algorithm}

Поймем, почему описанный алгоритм действительно моделирует наше случайное распределение, а также насколько хорошо он работает. Начнем с последнего.

\begin{statement}
	\slashns
	
	Матожидание количества действий, необходимых для генерации одного элемента из распределения $p(x)$ с помощью нашего алгоритма, равно $M(b - a)$.
	
	\begin{proof}
		\slashns
		
		Действительно. Так как мы выбираем случайную точку в прямоугольнике размера $M(b - a)$, в то время как площадь подграфика $p$ равна 1, нам в среднем потребуется сгенерировать как раз $M(b - a)$ точек, прежде чем мы попадем в подграфик.
		
		Соответственно, имеет смысл применять алгоритм в таком виде только тогда, когда $M(b - a)$ не слишком велико. 
	\end{proof}
\end{statement}

\begin{statement}
	\slashns

	Случайная величина $\eta$, моделируемая с помощью описанного алгоритма, распределена с плотностью $p$.
	
	\begin{proof}
		\slashns
		
		Заметим, что генерируя точки равномерно на $[a, b] \times [0, M]$, мы в том числе получаем равномерное распределение на подграфике $\mathcal{P}_{p}$ нашей плотности.
		
		А тогда имеем:
		$$\P(\eta < t) = \int[\substack{(x, y) \in \mathcal{P}_p\\x < t}] 1 \, dx\, dy = \int[a][t] \left(\int[0][p(x)] 1 \, dy \right) \, dx = \int[a][t] p(x) \, dx$$
	\end{proof}
\end{statement}

С одной стороны, мы привели метод, который позволяет довольно просто генерировать многие распределения. Тем не менее, его главными минусами являются допущения, описанные в начале. В частности, такой способ уже не подходит для распределений, плотность которых не ограничена.

Тем не менее, этой проблемы можно попробовать избежать, если считать нашу плотность не относительно меры Лебега, а относительно какой-нибудь другой меры.

А именно, предположим, что нам даны распределения $\mathcal{P}$ и $\mathcal{Q}$ с соответствующими плотностями $p(x)$ и $q(x)$, причем $\mathcal{P}$ абсолютно непрерывно относительно $\mathcal{Q}$.

Плотность распределения $\mathcal{P}$ относительно $\mathcal{Q}$ определяется как $\dfrac{d \mathcal{P}}{d \mathcal{Q}} = \dfrac{p}{q} = r$.

Тогда если $r(x) \le M$ на $[a, b]$, то можем воспользоваться похожим алгоритмом для моделирования распределения $p$.

\begin{algorithm}
	\slashns
	
	Выберем случайную точку $(\xi_1, \xi_2)$, т.ч. $\xi_1 \sim q(x), \, \xi_2 \sim \text{U}[0, M]$.
	
	Если $r(\xi_1) > \xi_2$, т.е. точка попала в подграфик $r$, то положим $\eta = \xi_1$. 
	
	Иначе повторим предыдущий шаг.
\end{algorithm}

\Subsection{Метод декомпозиции}

Рассмотрим еще один метод генерации данных, в основе которого лежит представление нашей случайной величины в виде линейной комбинации других случайных величин, каждую из которых мы умеем моделировать.

\begin{lemma}
	\slashns
	
	Пусть функция распределения исследуемой случайной величины $X$ имеет вид
	$$\F(x) = \sum\limits_{i=1}^{n} p_i \F_i(x), \text{ где } p_i \ge 0, \, p_1 + \ldots + p_n = 1$$
	
	Здесь $\F_i$~--- функции распределения, соответствующие некоторым независимым случайным величинам $\xi_i$.
	
	Кроме того, пусть нам дана независимая случайная величина $\tau$, принимающая значение $k$ с вероятностью $p_k$.
	
	Тогда $X \sim \xi_{\tau}$.
	
	\begin{proof}
		\slashns
		
		$$\P(X < t) = \sum\limits_{i=1}^{n} \P(\tau = i) \cdot \P(\xi_i < t \, | \, \tau = i) = \sum\limits_{i=1}^{n} \P(\tau = i) \cdot \P(\xi_i < t) = \sum\limits_{i=1}^{n} p_i \P(\xi_i < t)$$
	\end{proof}
	
\end{lemma}