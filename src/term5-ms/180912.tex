\Section{Квантили}{Антон Ермилов}

\Subsection{Определение квантили}

\begin{definition}
	\slashns
	
	$X$~--- некоторая случайная величина, $X \sim \F$
	
	$p \in (0, 1)$ и $\exists t: \; \F(t) = p$ и в окрестности $t$ функция $\F$ непрерывна и строго возрастает
	
	Квант\'иль порядка $p$~--- $\zeta_p = \F^{-1}(p)$
\end{definition}

\begin{remark}
	\slashns
	
	Возникает вопрос, что делать, если функция распределения не непрерывна (а именно с такими нам приходится иметь дело в математической статистике).
	
	Отсюда цель: научиться оценивать квантили.
\end{remark}

\begin{definition}
	\slashns
	
	$X_1, \, \ldots, \, X_n$~--- наблюдения.
	
	Рассмотрим случайную величину $\xi$, принимающую значения $X_1, \, \ldots, \, X_n$ с вероятностью $\frac1n$.
	
	Тогда наши выборочные моменты~--- это просто матожидание для $\xi$.
	
	Действительно, ранее мы уже поняли, что $\E X$ можно оценивать выборочным средним $\overline{X}$. А оно в точности совпадает с $\E \xi$.
	
	Аналогично, $\F(t)$ мы научились оценивать через эмпирическую функцию распределения $\F_n(t)$, которая также совпадает с функцией распределения $\F_\xi$.
	
	Тогда выборочная квантиль порядка $p$~--- это $Z_p = X_{([np]+1)}$, т.е. порядковая статистика с индексом $[np]+1$.
\end{definition}

\Subsection{Оценка генеральных квантилей}

\begin{theorem}
	\slashns
	
	$X_1, \, \ldots, \, X_n$~--- наблюдения, $X_i \sim \F$, $f = \F'$
	
	$p \in (0, 1)$, $\zeta_p$~--- генеральная квантиль порядка $p$ и $f(\zeta_p) \ne 0$
	
	Тогда выборочная квантиль будет асимптотически нормальной, т.е. $\sqrt{n}(Z_p - \zeta_p) \overset{d}{\to} \N\left(0, \frac{p(1-p)}{f^2(\zeta_p)}\right)$.
	
	\begin{proof}
		\slashns
		
		$k := [np] + 1$
		
		$\P\left(\sqrt{n} (Z_p - \zeta_p) < t \right) = \P\left(Z_p < \zeta_p + \frac{t}{\sqrt{n}} \right) = \P \left(X_{(k)} < \zeta_p + \frac{t}{\sqrt{n}} \right) = $
		
		$= \P\left(\text{хотя бы } k \text{ наблюдений } < \zeta_p + \frac{t}{\sqrt{n}} \right) = \P \left(\sum_{i=1}^{n} \Char\left\{X_i < \zeta_p + \frac{t}{\sqrt{n}} \right\} \ge k \right)$
		
		В левой части мы получили сумму независимых одинаково распределенных случайных величин, можем использовать центральную предельную теорему. Для этого посчитаем матожидание и дисперсию:
		
		$\E \Char \left\{X_i < \zeta_p + \frac{t}{\sqrt{n}} \right\} = \P \left(X_i < \zeta_p + \frac{t}{\sqrt{n}} \right) = \F\left(\zeta_p + \frac{t}{\sqrt{n}} \right) \overset{\text{Тейлор}}{=} \F(\zeta_p) + \frac{t}{\sqrt{n}} \F'(\zeta_p) + o\left(\frac{t}{\sqrt{n}} \right)$
		
		Вспомнив, что $\F(\zeta_p) = p$ и $\F'(\zeta_p) = f(\zeta_p)$, получаем:
		
		$\E \Char \left\{X_i < \zeta_p + \frac{t}{\sqrt{n}} \right\} = p + \frac{t}{\sqrt{n}} f(\zeta_p) + o\left(\frac{t}{\sqrt{n}} \right)$
		
		Посчитаем дисперсию:
		
		$\D \Char \left\{X_i < \zeta_p + \frac{t}{\sqrt{n}} \right\} = \F\left(\zeta_p+ \frac{t}{\sqrt{n}} \right) \left(1 - \F\left(\zeta_p+ \frac{t}{\sqrt{n}} \right)\right) \overset{\text{Тейлор}}{=} \left(\F(\zeta_p) + o(1) \right) \left(1 - \F(\zeta_p) + o(1)\right)$
		
		После подстановки аналогично получаем:
		
		$ \D \Char \left\{X_i < \zeta_p + \frac{t}{\sqrt{n}} \right\}  = p(1-p)(1 + o(1))$
		
		Положим $S_n = \sum_{i=1}^{n} \Char \left\{X_i < \zeta_p + \frac{t}{\sqrt{n}} \right\}$. Кроме того, введем $\Phi(t)$~--- функцию распределения для $\N(0, 1)$. Тогда по ЦПТ имеем:
		
		$\P \left(S_n \ge k \right) = 1 - \P \left(S_n < k \right) = 1 - \P \left(\frac{S_n - \E S_n}{\sqrt{\D S_n}} < \frac{k - \E S_n}{\sqrt{\D S_n}}\right) \overset{\text{ЦПТ}}{=}  1 - \Phi\left(\frac{k - n \left(p + \frac{t}{\sqrt{n}} f(\zeta_p) + o\left(\frac{t}{\sqrt{n}}\right) \right)}{\sqrt{p(1-p)(1+o(1)) \cdot n}} + o(1)\right) =$
		
		$= 1 - \Phi \left(\frac{[np] + 1 - np  - \sqrt{n} \cdot t f(\zeta_p) + o\left(\sqrt{n} \right)}{\sqrt{p(1-p)} \cdot \sqrt{n} \cdot (1+o(1))} \right) + o(1) = 1 - \Phi\left(\frac{-\sqrt{n} \cdot t f(\zeta_p) + o(\sqrt{n})}{\sqrt{p(1-p)} \cdot \sqrt{n} \cdot (1 + o(1))} \right) + o(1) = $
		
		$= 1 - \Phi \left(\frac{-t f(\zeta_p) + o(1)}{\sqrt{p(1-p)}} + o(1) \right) + o(1) = 1 - \Phi \left(\frac{-t f(\zeta_p)}{\sqrt{p(1-p)}} \right) + o(1) = \Phi \left(\frac{t f(\zeta_p)}{\sqrt{p(1-p)}} \right) + o(1)$
		
		Получили, что $\P\left(\sqrt{n} (Z_p - \zeta_p) < t \right) \to \Phi \left(\frac{t f(\zeta_p)}{\sqrt{p(1-p)}} \right) = \Phi\left(\frac{t}{\sigma} \right)$, где $\sigma = \frac{\sqrt{p(1-p)}}{f(\zeta_p)}$.
		
		А это в точности то, что мы и хотели.
	\end{proof}
\end{theorem}

\begin{statement}
	\slashns
	
	$\sqrt{n} \left(T_n - \theta \right) \overset{d}{\to} \N(0, \sigma^2) \implies T_n - \theta \overset{\P}{\to} 0$
	
	\begin{proof}
		\slashns
		
		Выпишем условие сходимости по вероятности:
		
		$\forall \eps > 0: \; \P \left(\abs{T_n - \theta} > \eps\right) \to 0$
		
		$\P \left(\abs{T_n - \theta} > \eps\right) = 1 - \P \left(\sqrt{n} \cdot \abs{T_n - \theta} \le \sqrt{n} \cdot \eps\right) = 1 - \P\left(-\sqrt{n} \cdot \eps \le \sqrt{n} \cdot (T_n - \theta) \le \sqrt{n} \cdot \eps \right)$
		
		Тогда, воспользовавшись сходимостью по распределению, имеем:
		
		$\P \left(\abs{T_n - \theta} > \eps\right) = 1 - \left(\Phi\left(\frac{\sqrt{n} \cdot \eps}{\sigma} \right) - \Phi\left(-\frac{\sqrt{n} \cdot \eps}{\sigma} \right) + o(1)\right) \underset{n \to \infty}{\to} 0$
	\end{proof}
\end{statement}

\Section{Случайные векторы}{Антон Ермилов}

\Subsection{Гауссовский вектор}

\begin{definition}
	\slashns
	
	Стандартный гауссовский вектор~--- $(X_1, \, \ldots, \, X_n)$, где $X_i$~--- независимые случайные величины из $\N(0, 1)$.
	
	Вектор $Y$ имеет гауссовское распределение, если представим в виде $Y = a + LX$, где $a \in \R^n$, а $L : \R^n \to \R^n$~---  линейное отображение.
\end{definition}

\begin{definition}
	\slashns
	
	Характеристическая функция случайного вектора: $\phi_\xi(t) = \E e^{i \angles{t, \xi}}$
\end{definition}

\begin{statement}
	\slashns
	
	Поймем, как выглядит характеристическая функция стандартного гауссовского вектора:
	
	$\phi_X(t) = \E e^{i  \angles{t, X}}= \E e^{i \sum_{j=1}^{n} t_j X_j} =\prod\limits_{j=1}^{n}  \E  e^{i t_j X_j} = \prod\limits_{j=1}^{n} e^{- \frac12 \cdot t_j^2} = e^{- \frac12 \angles{t, t}}$
	
	Для произвольного же вектора с гауссовским распределением имеем:
	
	$\phi_Y(t) = \E e^{i \angles{t, Y}} = \E e^{i \angles{t, a + LX}} = \E e^{i \angles{t, a}} \cdot e^{i \angles{t, LX}} = e^{i \angles{t, a}} \cdot \E e^{i \angles{t, LX}} = e^{i \angles{t, a}} \cdot \E e^{i\angles{L^* t, X}}$
	
	Тогда, воспользовавшись видом характеристической функции для стандартного гауссовского вектора, получаем характеристическую функцию для произвольного вектора $Y$, имеющего гауссовское распределение:
	
	$\phi_Y(t) = e^{i \angles{t, a}} \cdot e^{-\frac12 \angles{L^*t, L^*t}} = e^{i \angles{t, a}} \cdot e^{-\frac12 \angles{LL^*t, t}}$, где $Y = a + LX$
	
\end{statement}

\begin{statement}
	\slashns
	
	Поймем, как определяется плотность для гауссовского вектора
	
	Положим $Y = a + LX$. 
	
	Кроме того, потребуем обратимость $L$, т.к. иначе $Y$ живет на некотором подпространстве и тогда плотности никакой нет.
	
	Тогда имеем:
	
	$\P(X \in A) = \int[A] f_X(t) \; dt = \int[A] f_{X_1}(t_1) \cdot \ldots \cdot f_{X_n}(t_n) \; dt = \int[A] \frac{1}{(2\pi)^{n/2}} \cdot e^{-\frac12 \angles{t, t}} \; dt$
	
	Поймем, что происходит с $Y$. 
	
	Понятно, что сдвиг на плотность не влияет, потому будем считать, что $a = 0$.
	
	$\P(LX \in A) = \int[L^{-1}A] f_X(t) \; dt = \int[A] f_X(L^{-1} t) \cdot \abs{\det L^{-1}} \; dt \implies f_Y(t) = f_X(L^{-1} t) \cdot \abs{\det L^{-1}}$
	
	В общем же случае вид плотности будет следующий:
	
	$f_Y(t) = f_{a+LX}(t) = f_{LX}(t-a) = f_X(L^{-1}(t-a)) \cdot \abs{\det L^{-1}}$
\end{statement}

\begin{statement}
	\slashns
	
	Посмотрим на матожидание стандартного гауссовского вектора $X$:
	
	$\E X = 0$, т.к. $X_i \sim \N(0, 1)$
	
	$\E [X X^T]$~--- ковариационная матрица (ее элементы как раз имеют вид $\E [X_iX_j]$), причем эта матрица будет единичной, т.к. дисперсия равна единице.
	
	Поймем, что происходит с произвольным гауссовским вектором $Y = a + LX$:
	
	$\E Y = \E [a + LX] = a + \E[LX] = a$
	
	$\E [(Y - a)(Y-a)^T] = \E [L X X^T L^T] = L (\E[XX^T]) L^T = LL^T$
	
	Если же нам загадали некоторый гауссовский случайный вектор с ковариационной матрицей $R$, то построить сам вектор мы можем путем извлечения корня из $R$ (это возможно сделать в силу того, что $R$ симметричная и положительно определенная).
	
	Таким образом, получив матрицу $R^{\frac12}$, мы в то же время получаем линейное отображение $L$, а значит~--- и сам вектор $Y$ (с точностью до сдвига).
\end{statement}

\Section{Статистические оценки}{Антон Ермилов}

\Subsection{Требования, предъявляемые к оценкам}

\begin{definition}
	\slashns
	
	$X_1, \, \ldots, \, X_n$~--- выборка, $X_i \sim \P_{\theta}, \; \theta \in \Theta$
	
	$T = T(X_1, \, \ldots, \, X_n)$~--- произвольная оценка.
	
	Определим критерии ``хорошести'' нашей оценки:
	
	\begin{enumerate}
		\item Несмещенность: $\forall \theta \;\; \E_{\theta} T = \theta$
		\item Асимптотическая несмещенность: $\forall \theta \;\; \E_{\theta} T_n\to \theta$
		\item Состоятельность: $\forall \theta \;\; T_n \overset{\P_{\theta}}{\to} \theta$
		\item Сильная состоятельность: $\forall \theta \;\; T_n \overset{\text{п.н.}}{\to} \theta$
		\item Асимптотическая нормальность: $\sqrt{n} (T_n - \theta) \overset{d}{\to} \N(0, \sigma^2(\theta))$
		\item Эффективность:  $T_1$ ``эффективнее'' $T_2$, если $\forall \theta \;\; \E_{\theta}(T_1 - \theta)^2 \le \E_{\theta}(T_2 - \theta)^2$
	\end{enumerate}
	
\end{definition}

\begin{remark}
	\slashns
	
	Тем не менее, совсем необязательно, чтобы наши оценки удовлетворяли всем вышеописанным критериям. Даже отсутствие несмещенности как правило не делает оценку ``плохой''.
\end{remark}