\Subsection{Ранги. Наиболее мощные ранговые критерии}

Предположим, что наши данные непрерывны, но устроены таким образом, что мы умеем лишь сравнивать наблюдения между собой и говорить, какое из них ``меньше''. Причем будем считать такое отношение транзитивно.

Такие данные называются ранговыми, и именно их мы и будем обсуждать сейчас. Но для этого нам сначала придется чуть ближе познакомиться с порядковыми статистиками.

\begin{reminder}
	\slashns
	
	Пусть $X_1, \, \ldots, \, X_n$~--- некоторые наблюдения, обладающие описанным выше свойством.
	
	$X_{(i)}$~--- $i$-ая порядковая статистика, она же значение $i$-ого по величине наблюдения из $X_1, \, \ldots, \, X_n$.
\end{reminder}

\begin{denotation}
	\slashns

	Пусть $X_1, \, \ldots, \, X_n$~--- некоторые наблюдения, обладающие описанным выше свойством.
	
	Введем следующие обозначения:
	
	$\mathbf{X} := (X_1, \, \ldots, \, X_n)$~--- вектор наблюдений
	
	$\mathbf{X}_{(\cdot)} := (X_{(1)}, \, \ldots, \, X_{(n)})$~--- вектор порядковых статистик (вариационный ряд)

\end{denotation}

\begin{definition}
	\slashns
	
	Пусть $X_1, \, \ldots, \, X_n \sim \F$~--- непрерывно распределенные наблюдения. Непрерывность будем требовать для того, чтобы вероятность совпадения двух событий была равна 0.
	
	Определим ранг $R_i$ как позицию элемента $X_i$ в вариационном ряду $\mathbf{X}_{(\cdot)}$, т.е. $X_i = X_{(R_i)}$.
	
	$\mathbf{R} := (R_1, \, \ldots, \, R_n)$~--- вектор рангов.
	
\end{definition}

\begin{remark}
	\slashns
	
	Из определения видно, что вектор $\mathbf{R}$ является перестановкой чисел $\{1, \, \ldots, \, n\}$, т.е. $\mathbf{R} \in S_n$.
\end{remark}

\begin{theorem}
	\slashns
	
	Пусть $f$~--- совместная плотность случайного вектора $\mathbf{X}$.
	
	Тогда вариационный ряд $\mathbf{X}_{(\cdot)}$ имеет распределение с плотностью
	$$\overline{f}(\mathbf{x}_{(\cdot)}) := \sum\limits_{\mathbf{r} \in S_n} f(x_{(r_1)}, \, \ldots, \, x_{(r_n)})$$
	
	Кроме того, 
	$$\P(\mathbf{R} = \mathbf{r} \, | \, \mathbf{X}_{(\cdot)} = \mathbf{x}_{(\cdot)}) = \dfrac{f(x_{(r_1)}, \, \ldots, \, x_{(r_n)})}{\overline{f}(\mathbf{x}_{(\cdot)})}$$
	
	\begin{proof}
		\slashns
		
		Пусть $\mathcal{A}_{(\cdot)}$~--- $\sigma$-алгебра борелевских подмножеств $\mathcal{X}_{(\cdot)}$, где $\mathcal{X}_{(\cdot)}$~--- подпространство $\mathcal{X}$, на котором определены все порядковые статистики.
		
		Рассмотрим произвольное множество $A \in \mathcal{A}_{(\cdot)}$. Тогда для него будет верно:
		$$\P(\mathbf{X}_{(\cdot)} \in A) = \int[\mathbf{x}_{(\cdot)} \in A] f(x_1, \, \ldots, \, x_n) \; dx_1 \, \ldots \, dx_n = \sum\limits_{\mathbf{r}\in S_n} \;\; \int[\substack{\mathbf{x}_{(\cdot)} \in A\\ x_i = x_{(r_i)} }] f(x_1, \, \ldots, \, x_n) \; dx_1 \, \ldots \, dx_n \; \textcolor{red}{=}$$
		
		Заметим, что по вектору порядковых статистик $\mathbf{x}_{(\cdot)}$ и вектору рангов $\mathbf{r}$ однозначно восстанавливается исходный вектор $\mathbf{x}$. А потому можем сделать замену переменной и перейти к интегрированию по множеству $\mathbf{x} \in A$.
		
		В частности, поскольку матрица замены соответствует некоторой перестановке элементов, модуль якобиана замены будет равен единице.
		$$\textcolor{red}{=}\;  \sum\limits_{\mathbf{r}\in S_n} \;\; \int[\mathbf{x} \in A] f(x_{(r_1)}, \, \ldots, \, x_{(r_n)}) \; dx_{(1)} \, \ldots \, dx_{(n)} \overset{f \ge 0}{=} \int[\mathbf{x} \in A]\;\; \sum\limits_{\mathbf{r}\in S_n} f(x_{(r_1)}, \, \ldots, \, x_{(r_n)}) \; dx_{(1)} \, \ldots \, dx_{(n)}$$
		
		С другой стороны, если $\overline{f}$~--- плотность распределения вариационного ряда, то для любого $A \in \mathcal{A}_{(\cdot)}$ выполняется:
		$$\P(\mathbf{X}_{(\cdot)} \in A) = \int[\mathbf{x} \in A] \overline{f}(\mathbf{x}_{(\cdot)}) \; dx_{(1)} \, \ldots \, dx_{(n)}$$
		
		А значит, 
		$$\overline{f}(\mathbf{x}_{(\cdot)}) = \sum\limits_{\mathbf{r}\in S_n} f(x_{(r_1)}, \, \ldots, \, x_{(r_n)})$$
		
		В частности, на основе приведённых выше выкладок мы можем запросто посчитать следующую совместную вероятность:
		$$\P(\mathbf{R} = \mathbf{r}, \, \mathbf{X}_{(\cdot)} \in A) = \int[\substack{\mathbf{x}_{(\cdot)} \in A\\ x_i = x_{(r_i)} }] f(x_1, \, \ldots, \, x_n) \; dx_1 \, \ldots \, dx_n = \int[\mathbf{x} \in A] f(x_{(r_1)}, \, \ldots, \, x_{(r_n)}) \; dx_{(1)} \, \ldots \, dx_{(n)}$$
		
		А тогда вероятность $\P(\mathbf{R} = \mathbf{r} \, | \, \mathbf{X}_{(\cdot)} = \mathbf{x}_{(\cdot)})$ вычисляется по формуле условной вероятности просто как отношение соответствующих плотностей, т.е.
		$$\P(\mathbf{R} = \mathbf{r} \, | \, \mathbf{X}_{(\cdot)} = \mathbf{x}_{(\cdot)}) = \dfrac{f(x_{(r_1)}, \, \ldots, \, x_{(r_n)})}{\overline{f}(\mathbf{x}_{(\cdot)})}$$
	\end{proof}
\end{theorem}

Мы сейчас доказали теорему о распределении рангов произвольного случайного вектора. Когда же мы работаем с реальными данными, мы все-таки привыкли предполагать, что наши наблюдения в идеальном случае независимы и одинаково распределены. Как мы сейчас увидим, в таком случае распределение вектора рангов и вариационного ряда принимает довольно простой и приятный вид.

\begin{theorem}\label{independence}
	\slashns
	
	Пусть $X_1, \, \ldots, \, X_n$~--- независимые одинаково распределенные случайные величины с совместной плотностью $q$.
	
	Тогда случайные векторы $\mathbf{R}$ и $\mathbf{X}_{(\cdot)}$  независимы, причем
	$$\P(\mathbf{R} = \mathbf{r}) = \frac{1}{n!}, \;\;\;\;\;\; \overline{q}(\mathbf{x}_{(\cdot)}) = n! \, q(\mathbf{x}_{(\cdot)})$$
	
	\begin{proof}
		\slashns
		
		Заметим, что если случайные величины независимы и одинаково распределены, то
		$$q(x_{(1)}, \, \ldots, \, x_{(n)}) = q(x_{(r_1)}, \, \ldots, \, x_{(r_n)}) \;\; \forall r \in S_n$$
		
		А тогда 
		$$\overline{q}(\mathbf{x}_{(\cdot)}) = \sum\limits_{r \in S_n} q(x_{(r_1)}, \, \ldots, \, x_{(r_n)})  = n! \, q(\mathbf{x}_{(\cdot)})$$
		
		В частности, $\forall \mathbf{x}_{(\cdot)} \in \mathcal{X}_{(\cdot)}$ выполняется следующее равенство:
		$$\P(\mathbf{R} = \mathbf{r} \, | \, \mathbf{X}_{(\cdot)} = \mathbf{x}_{(\cdot)}) = \dfrac{q(x_{(r_1)}, \, \ldots, \, x_{(r_n)})}{\overline{q}(\mathbf{x}_{(\cdot)})} = \dfrac{q(\mathbf{x}_{(\cdot)})}{ n! \, q(\mathbf{x}_{(\cdot)})} = \dfrac{1}{n!}$$
		
		То есть получили, что распределение вектора рангов никак не зависит от распределения вариационного ряда. А значит, векторы $\mathbf{R}$ и $\mathbf{X}_{(\cdot)}$ независимы.
	\end{proof}
\end{theorem}

Осталось теперь немного поговорить о том, что такое ранговые критерии и как они устроены.

\begin{definition}
	\slashns
	
	Ранговые критерии~--- такие критерии, что их критическая область зависит только от вектора рангов $\mathbf{R}$.
\end{definition}

\begin{theorem}
	\slashns
	
	Пусть дана выборка $X_1, \, \ldots, \, X_n$, такая что $\mathbf{X} \sim \P$.
	
	И пусть проверяются следующие  простые гипотезы:
	$$H_0: \P = \P_0, \;\;\;\; H_1: \P = \P_1$$
	
	Тогда если распределение $P_0$ соответствует совместному распределению независимых одинаково распределенных случайных величин, то наиболее мощный ранговый критерий имеет следующую структуру:
	$$\phi(\mathbf{X}) = \begin{cases}
	1, & \P_1(\mathbf{R} = \mathbf{r}) \ge k\\
	0, & \P_1(\mathbf{R} = \mathbf{r}) < k
	\end{cases}, \text{ где } \mathbf{r} \text{ --- ранги элементов } \mathbf{X}$$
	
	\begin{proof}
		\slashns
		
		Воспользуемся леммой Неймана-Пирсона.
		
		По определению ранговых критериев наша критическая область зависит только от вектора рангов, а потому и интересовать нас будет только распределение вектора рангов.
		
		Но если наблюдения независимы, то $\P_0(\mathbf{R} = \mathbf{r}) = \frac{1}{n!}$, т.е. эта величина является константой.
	\end{proof}
\end{theorem}

Но, как и прежде, нас чаще все-таки будет интересовать проверка простой гипотезы против сложной альтернативы. А потому введем понятие локально наиболее мощного критерия.

\begin{definition}
	\slashns
	
	Пусть нам дано семейство вероятностных распределений $\{\P_{\delta} \}$, запараметризованных некоторым параметром $\delta$.
	
	И пусть совместное распределение элементов выборки $X_1, \, \ldots, \, X_n$ лежит в этом семействе.
	
	Будем проверять следующие гипотезы:
	$$H_0: \delta = 0, \;\;\;\; H_1: \delta > 0$$
	
	Критерий называется локально наиболее мощным, если $\exists \eps > 0 : \forall \delta_0 \in (0, \eps)\;\; \phi$~--- наиболее мощный критерий против альтернативы $H_1 : \delta = \delta_0$.
\end{definition}

\begin{remark}
	\slashns
	
	В дальнейшем мы будем считать, что плотности распределений элементов выборки принадлежат семейству плотностей $\{f(X, \theta)\}$, а совместная плотность распределения имеет следующий вид:
	$$q_{\delta}(\mathbf{X}) = \prod\limits_{i=1}^{n} f(X_i, \delta c_i), \;\;\; \delta > 0$$
\end{remark}

Кроме того, для определения последующих критериев мы будем требовать от плотностей некоторых условий регулярности. А именно:

\begin{conditions}[регулярности]\label{rank_regular_conditions}
	%\hyperref[rank_regular_conditions]{\textcolor{blue}{условия регулярности}}
	\slashnl
	
	\begin{enumerate}
		\item $f(x, \theta)$ абсолютно непрерывна по $\theta$ (или $f(x, \cdot) \in C^1$)
		\item $\exists\; f'(x, 0) = \lim\limits_{\theta\to 0} \dfrac{f(x, \theta) - f(x, 0)}{\theta}$
		\item $\lim\limits_{\theta\to0} \int[-\infty][\infty] \abs{f'(x, \theta)} \; dx = \int[-\infty][\infty] \abs{f'(x, 0)} \; dx < \infty$
	\end{enumerate}
\end{conditions}

Перейдем теперь непосредственно к основным гипотезам и критериям для их проверки.

\Subsection{Ранговые критерии. Гипотеза случайности}

Начнем с определения гипотезы случайности.

\begin{definition}
	\slashns
	
	$X_1, \, \ldots, \, X_n$~--- произвольная выборка, имеющая совместную плотность $q$.
	
	Гипотеза $H_0 : q(X_1, \, \ldots, \, X_n) = \prod\limits_{i=1}^{n} f(X_i)$ называется гипотезой случайности.
	
	Иными словами, эта гипотеза попросту предполагает, что все наблюдения независимы и одинаково распределены.
\end{definition}

Мы уже \hyperref[independence]{\textcolor{blue}{знаем}} как распределены $\mathbf{R}$ и $\mathbf{X}_{(\cdot)}$ в случае, если гипотеза случайности верна.

Получим теперь вид локально наиболее мощного критерия для проверки этой гипотезы, чтобы на основе него затем построить остальные критерии.

\begin{denotation}
	\slashns
	
	Будем называть метками (score) функции вида:
	$$a_n(i, f) = \E_0 \dfrac{f'(X_{(i)}, 0)}{f(X_{(i)}, 0)}$$
	
	Здесь $n$~--- размер выборки, $\E_0$~--- матожидание, соответствующее плотности $f(x, 0)$.
\end{denotation}

\begin{theorem}
	\slashns
	
	Пусть выполняются \hyperref[rank_regular_conditions]{\textcolor{blue}{условия регулярности}}. 
	
	Тогда локально наиболее мощный ранговый критерий для проверки гипотезы случайности~--- критерий с критической областью следующего вида:
	$$\sum\limits_{i=1}^{n} c_i \,a_n(R_i, f) \ge k$$
	
	\begin{proof}
		\slashns
		
		Распишем вероятность встретить конкретный вектор рангов при распределении $\P_{\delta}$:
		$$\P_{\delta}(\mathbf{R} = \mathbf{r}) = \int[\mathbf{R} = \mathbf{r}] q_{\delta} (x_1, \, \ldots, \, x_n) \, dx_1 \, \ldots \, dx_n = \int[\mathbf{R} = \mathbf{r}] \prod\limits_{i=1}^{n} f(x_i, \delta c_i) \, dx_1 \, \ldots \, dx_n = $$
		$$= \int[\mathbf{R} = \mathbf{r}] \prod\limits_{i=1}^{n} f(x_i, 0) \, dx_1 \, \ldots \, dx_n + \delta \cdot \int[\mathbf{R} = \mathbf{r}] \dfrac1\delta \left(\prod\limits_{i=1}^{n} f(x_i, \delta c_i) - \prod\limits_{i=1}^{n} f(x_i, 0) \right) \, dx_1 \, \ldots \, dx_n \; \textcolor{red}{=}$$
		
		Заметим, что первый интеграл~--- вероятность появления конкретного ранга при справедливости нулевой гипотезы. Это значение нам уже известно и равно $\frac{1}{n!}$.
		
		Второй же интеграл можно несколько переписать, воспользовавшись свойством следующего вида:
		$$\prod\limits_{i=1}^{n} a_i - \prod\limits_{i=1}^{n} b_i = \sum\limits_{i=1}^{n} \left((a_i - b_i)  \cdot \prod\limits_{j=1}^{i - 1} a_j \cdot \prod\limits_{j=i+1}^{n} b_j\right)$$
		
		А тогда получаем:
		$$ \textcolor{red}{=} \; \dfrac{1}{n!} + \delta \cdot \int[\mathbf{R} = \mathbf{r}] \sum\limits_{i=1}^{n}  \left( c_i \cdot \dfrac{f(x_i, \delta c_i) - f(x_i, 0)}{\delta c_i}  \cdot \prod\limits_{j=1}^{i-1} f(x_j, \delta c_j)\cdot \prod\limits_{j=i+1}^{n} f(x_j, 0) \right) \, dx_1 \, \ldots \,  dx_n \; \textcolor{red}{=} s $$
		
		Воспользуемся описанными выше \hyperref[rank_regular_conditions]{\textcolor{blue}{условиями регулярностями}} и перейдем к пределу по $\delta$:
		$$\textcolor{red}{=} \; \dfrac1{n!} + \delta \cdot \int[\mathbf{R} = \mathbf{r}]\sum\limits_{i=1}^{n}  \left( c_i f'(x_i, 0) \cdot \prod\limits_{j \ne i} f(x_j, 0) \right) \, dx_1 \, \ldots \, dx_n + o(\delta) =$$
		$$= \dfrac1{n!} + \delta \cdot \sum\limits_{i=1}^{n}\;\; \int[\mathbf{R} = \mathbf{r}] \left(c_i \cdot \dfrac{f'(x_i, 0)}{f(x_i, 0)} \cdot \prod\limits_{j=1}^{n} f(x_j, 0)\right) \, dx_1 \, \ldots \, dx_n + o(\delta) =$$
		$$= \dfrac{1}{n!} + \delta \cdot \sum\limits_{i=1}^{n} c_i \E_0\left(\dfrac{f'(x_i, 0)}{f(x_i, 0)} \, \Big| \, \mathbf{R} = \mathbf{r} \right) + o(\delta)= \dfrac{1}{n!} + \delta \cdot \sum\limits_{i=1}^{n} c_i \E_0 \dfrac{f'(x_{(r_i)}, 0)}{f(x_{(r_i)}, 0)} + o(\delta)=$$
		$$= \dfrac1{n!} + \delta \cdot \sum\limits_{i=1}^{n} c_i a_n(r_i, f) + o(\delta)$$
		
		Вспомним теперь, что при фиксированной $\delta$ критическая область имеет следующий вид:
		$$\P_{\delta} (\mathbf{R} = \mathbf{r}) \ge \tilde{k}$$
		
		А тогда можно найти такое $k$, что в некоторой окрестности нуля будет выполняться:
		$$\dfrac1{n!} + \delta \cdot \sum\limits_{i=1}^{n} c_i a_n(r_i, f) + o(\delta) \ge \tilde{k}, \;\; \delta > 0 \implies \sum\limits_{i=1}^{n} c_i a_n(r_i, f) \ge k$$
	\end{proof}
\end{theorem}

Мы теперь знаем общий вид локально наиболее мощного критерия для гипотезы случайности. Построим на основе него несколько критериев для тестирования этой гипотезы против гипотезы сдвига.

А именно, пусть дана пара выборок $X_1, \, \ldots, \, X_m$ и $X_{m+1}, \, \ldots, \, X_{m+n}$, и мы проверяем гипотезы следующего вида:
$$H_0 : q_{0}(\mathbf{X}) = \prod\limits_{i=1}^{n+m} f(X_i), \;\;\;\; H_1 : q_{\delta}(\mathbf{X}) = \prod\limits_{i=1}^{m} f(X_i - \delta) \cdot \prod\limits_{i=1}^{n} f(X_{m+i})$$

\begin{enumerate}
	\item \textbf{\textit{Критерий нормальных меток}}
	
	Пусть наши выборки были получены из нормального распределения, т.е. 
	$$f(x) = \dfrac{1}{\sqrt{2\pi}} e^{-x^2/2} \;\;\;\text{и}\;\;\; f(x, \theta) := f(x + \theta)$$
	
	Поймем, как в этом случае устроены метки:
	$$\dfrac{f'(x, 0)}{f(x, 0)} = (\ln f(x, 0))' = \left(-\dfrac{x^2}{2} \right)' = -x$$
	$$a_N(i, f) = \E_0 \dfrac{f'(X_{(i)}, 0)}{f(X_{(i)}, 0)} = \E_0 (-X_{(i)}) = -\E \Phi^{-1}(\text{U}_{(i)})$$
	
	А тогда, поскольку $c_1, \, \ldots, \, c_m$ равны -1 и $c_{m+1}, \, \ldots, \, c_{m+n}$ равны 0, сама критическая область может быть записана как
	$$\sum\limits_{i=1}^{m} -a_{n+m}(R_i, f) = \sum\limits_{i=1}^{m}  \E \Phi^{-1}(\text{U}_{(R_i)})  \ge k$$
	
	\item \textbf{\textit{Критерий ван дер Вардена}}
	
	Главная проблема критерия нормальных меток заключается в сложности вычисления $a_n(i, f)$.
	
	Но заметим, что в силу $\E \Phi^{-1}(\text{U}_{(i)}) \approx \Phi^{-1} (\E \text{U}_{(i)})$ его можно переписать в более удобном виде:
	$$\sum\limits_{i=1}^{m} a_{n+m}(R_i, f) = \sum\limits_{i=1}^{m} \Phi^{-1}\left(\dfrac{R_i}{n+m+1}  \right) \ge k$$
	
	\item \textbf{\textit{Критерий Вилк\'оксона}}
	
	Рассмотрим теперь случай логистического распределения с плотностью $f(x) = \dfrac{e^{-x}}{(1 + e^{-x})^2}$.
	
	Снова поймем, как в этом случае будут устроены метки:
	$$\dfrac{f'(x, 0)}{f(x, 0)} = (\ln f(x, 0))' = \left(-x - 2 \ln (1 + e^{-x}) \right)' = -1 + 2\dfrac{e^{-x}}{1 + e^{-x}} = \dfrac{e^{-x} - 1}{e^{-x} + 1}$$
	$$\F(x) = \dfrac{1}{1 + e^{-x}}, \;\; \F^{-1}(y) = -\ln \left( \dfrac{1}{y} - 1\right) = \ln \dfrac{y}{1-y}$$
	$$\dfrac{f'}{f}\left(\F^{-1}(y)\right) = \left(\dfrac{e^{-x} - 1}{e^{-x} + 1} \right) \circ \ln \dfrac{y}{1-y} = \dfrac{\dfrac{1-y}{y} - 1}{\dfrac{1-y}{y} + 1} = 1-2y$$
	$$a_N(i, f) = \E_0 \left(\dfrac{f'}{f}(X_{(i)}, 0)\right) = \E \left(\dfrac{f'}{f}\left(\F^{-1}(\text{U}_{(i)}), 0 \right)\right) = \E \Big(1 - 2\text{U}_{(i)}\Big) = 1 - \dfrac{2i}{N + 1}$$
	
	Как и раньше, какие-то $c_i$ у нас равны -1, а какие-то равны 0. Сам же критерий может быть переписан теперь в таком виде:
	$$\sum\limits_{i=1}^{m} R_i \ge k$$
\end{enumerate}
