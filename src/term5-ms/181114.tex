\Subsection{Критерии для категориальных и дискретных числовых данных}

Мы обсудили некоторые критерии, неплохо работающие в случае, когда наши данные имеют непрерывную природу.

Но такие критерии плохо работают для данных, принимающих лишь значения из некоторого дискретного множества. К примеру, это могут быть числовые данные, принимающие лишь целочисленные значения, либо нечисловые данные, в которых каждому элементу соответствует какая-то категория (label).

Тем не менее, для данных именно такой природы тоже существует большое количество критериев. И одним из наиболее сильных таких критериев является критерий Пирсона, также известный как критерий хи-квадрат.

\begin{theorem}[Pearson's chi-squared test]
	\slashns
	
	Пусть дана выборка $X_1, \, \ldots, X_n \sim \P$, где $X_i$~--- независимы.
	
	И пусть верна гипотеза $H_0 : \P \sim \P_0$, где
	$$\P_0 : \begin{array}{c|c|c|c}
	x_1 & x_2 & \ldots & x_s\\\hline
	p_1 & p_2 & \ldots & p_s
	\end{array}$$
	
	Тогда верно следующее утверждение:
	$$\chi^2 := \sum\limits_{i=1}^{s} \dfrac{(\nu_i - np_i)^2}{np_i} \overset{H_0}{\underset{n\to\infty}{\to}} \chi_{s-1}^2, \text{ где } \nu_i = \#\{k : X_k = x_i \}$$
	
	\begin{proof}
		\slashns
		
		Введем для начала несколько обозначений:
		$$a_i(X_j) := \Char \{X_j = x_i \}$$
		$$\nu := \left(\dfrac{\nu_1 - np_1}{\sqrt{np_1}}, \, \ldots, \, \dfrac{\nu_s - np_s}{\sqrt{np_s}} \right)^T, \;\;\; t := \left(t_1, \, \ldots, \, t_s \right)^T$$
		
		Здесь $\nu$~--- случайный вектор, а $t$~--- некоторый произвольный набор коэффициентов.
		
		Рассмотрим теперь скалярное произведение $\langle \nu, t \rangle$. Попробуем переписать его в некотором более удобном виде:
		$$\langle \nu, t \rangle = \sum\limits_{i=1}^{s} \dfrac{\nu_i - np_i}{\sqrt{np_i}} \cdot t_i = \Bigg[\nu_i = \sum\limits_{j=1}^{n} a_i(X_j) \Bigg] =  \sum\limits_{i=1}^{s} \left(\dfrac{t_i}{\sqrt{np_i}} \sum\limits_{j=1}^{n} a_i(X_j) - t_i\sqrt{np_i} \right) =$$
		$$= \Bigg[t_i\sqrt{np_i} = \dfrac{t_i}{\sqrt{np_i}} \sum\limits_{j=1}^{n} p_i \Bigg] = \sum\limits_{i=1}^{s} \left(\sum\limits_{j=1}^{n} \dfrac{t_i}{\sqrt{np_i}} \big(a_i(X_j) - p_i\big)\right) =$$
		$$=  \dfrac{1}{\sqrt{n}} \sum\limits_{j=1}^{n} \left(\sum\limits_{i=1}^{s} \dfrac{t_i}{\sqrt{p_i}} \big(a_i(X_j) - p_i\big)\right) = \Bigg[Y_j := \sum\limits_{i=1}^{s} \dfrac{t_i}{\sqrt{p_i}} \big(a_i(X_j) - p_i\big) \Bigg] = \dfrac{1}{\sqrt{n}}\sum\limits_{j=1}^{n} Y_j$$
		
		Заметим, что т.к. $X_j$ независимы, то $Y_j$ также независимы. А значит, можно попробовать воспользоваться центральной предельной теоремой. Для этого требуется посчитать матожидание и дисперсию $Y_j$.
		
		Посчитаем матожидание:
		$$\E a_i(X_j) = p_i \implies \E Y_j = \sum\limits_{i=1}^{s} \dfrac{t_i}{\sqrt{p_i}} \big(\E a_i(X_j) - p_i \big) = 0$$
		
		И дисперсию:
		$$\D Y_j = \E Y_j^2 = \sum\limits_{i=1}^{s} \dfrac{t_i^2}{p_i} \E\big(a_i(X_j) - p_i \big)^2 + \sum\limits_{i \ne k} \dfrac{t_it_k}{\sqrt{p_ip_k}} \E\bigg( \big(a_i(X_j) - p_i \big) \big(a_k(X_j) - p_k \big)\bigg) \; \textcolor{red}{=}$$
		
		$$\E \big(a_i(X_j) - p_i \big)^2 = \E a_i(X_j)^2 - 2p_i \E a_i(X_j) + p_i^2 = p_i - p_i^2 = p_i(1 - p_i)$$
		$$\E \bigg( \big(a_i(X_j) - p_i \big) \big(a_k(X_j) - p_k \big)\bigg) = \E a_i(X_j) a_k(X_j) - p_i \E a_k(X_j) - p_k \E a_i(X_j) + p_ip_k = -p_ip_k$$
		
		$$\textcolor{red}{=} \; \sum\limits_{i=1}^{s} \dfrac{t_i^2}{p_i} p_i (1-p_i) + \sum\limits_{i \ne k} \dfrac{t_it_k}{\sqrt{p_ip_k}} (-p_ip_k) =\sum\limits_{i=1}^{s} t_i^2 - \sum\limits_{i=1}^{s} (t_i\sqrt{p_i})^2 - \sum\limits_{i\ne k} t_i\sqrt{p_i} \cdot t_k \sqrt{p_k} =$$
		$$= \sum\limits_{i=1}^{s} t_i^2 - \left(\sum\limits_{i=1}^{s} t_i \sqrt{p_i} \right)^2$$
		
		Соберем вместе все полученные результаты:
		$$q(t_1, \, \ldots, \, t_s) := \sum\limits_{i=1}^{s} t_i^2 - \left(\sum\limits_{i=1}^{s} t_i \sqrt{p_i} \right)^2$$
		$$\langle \nu, t \rangle = \dfrac{1}{\sqrt{n}} \sum\limits_{j=1}^{n} Y_j, \;\;\;\; \E Y_j = 0, \;\;\;\; \D Y_j = q(t_1, \, \ldots, \, t_s)$$
		
		По центральной предельной теореме мы имеем:
		$$\langle \nu, t \rangle \overset{d}{\underset{n \to \infty}{\to}} \N(0, q(t_1, \, \ldots, \, t_s))$$
		
		Поскольку мы знаем вид характеристической функции для нормального распределения, то можем посчитать предельный вид следующей величины:
		$$\E \exp\big(i \, \lambda \, \langle \nu, t \rangle\big) \to \exp\left(-\dfrac{\lambda^2}{2} q(t_1, \, \ldots, \, t_s)\right)$$
		
		А т.к. это верно для любого $\lambda$, то при $\lambda = 1$ получаем в точности предельный вид характеристической функции вектора $\nu$:
		$$ \phi_{\nu}(t) = \E \exp\big(i \, \langle \nu, t \rangle \big) \to \exp \left(-\dfrac{1}{2} q(t_1, \, \ldots, \, t_s) \right)$$
		
		Заметим, что $q(t_1, \, \ldots, \, t_s)$~--- некоторая квадратичная функция. Положим $Q$~--- соответствующая этой квадратичной функции матрица.
		
		Если же мы вспомним, как выглядит характеристическая функция гауссовского вектора, то запросто получим и предельное распределение вектора $\nu$:
		$$\phi_\nu(t) \to \exp\left(-\dfrac{1}{2} q(t_1, \, \ldots, \, t_s) \right) = \exp \left(-\dfrac12\, \langle Qt, t \rangle \right) \implies \nu \overset{d}{\to} \eta \sim \N(0, Q)$$
		
		А тогда:
		$$\chi^2 = \norm{\nu}^2 \to \norm{\eta}^2 = \sum\limits_{i=1}^{s} \eta_i^2$$
		
		Остается лишь понять, как распределена сумма квадратов компонент вектора $\eta$.
		
		Для этого воспользуемся похожим трюком, что и при доказательстве леммы Фишера. А именно, рассмотрим произвольную ортогональную матрицу $C$ следующего вида:
		$$C = \begin{pmatrix}
		\sqrt{p_1} & \cdots & \sqrt{p_s}\\
		\cdot & \cdot & \cdot\\
		\cdot & \cdot & \cdot
		\end{pmatrix}$$
		
		Поскольку $C$~--- ортогональная матрица, то $\eta \overset{d}{=} C \eta$.
		
		Поймем, как устроена ковариационная матрица вектора $C \eta$. Для этого выпишем его характеристическую функцию:
		$$\phi_{C \eta}(\tau) = \phi_{\eta}(C^{-1} \tau) \overset{C^{-1} = C^T}{=} \phi_{\eta}(C^T \tau) = \exp\left(-\dfrac12 \, \langle Q C^T \tau, \,C^T \tau \rangle \right)$$
		$$ \langle Q C^T \tau, \,C^T \tau \rangle = \sum\limits_{i=1}^{s} (C^T \tau)_i^2 - \left(\sum\limits_{i=1}^{s}(C^T\tau)_i \cdot \sqrt{p_i} \right)^2 = \norm{C^T \tau}^2 - \big(CC^T\tau \big)_1^2 = \norm{\tau}^2 - \tau_1^2 = \sum\limits_{i=2}^{s} \tau_i^2$$
		
		То есть мы получили, что ковариационная матрица вектора $C\eta$ содержит только $s-1$ единицу на главной диагонали. А значит, распределение $\norm{C \eta}^2$ в точности соответствует распределению $\chi^2_{s-1}$, что и требовалось.
	\end{proof}
\end{theorem}

\begin{remark}
	\slashns
	
	В доказанной нами теореме мы пользовались тем, что наше пространство конечно.
	
	Но на самом деле, ее можно использовать и в случае, когда наше пространство бесконечно (в т.ч. если оно непрерывно). Для этого достаточно выбрать произвольное разбиение пространства $\mathcal{X} = \mathcal{X}_1 \sqcup \ldots \sqcup \mathcal{X}_s$ и определить $p_i := \P(X \in \mathcal{X}_i)$.
\end{remark}

\begin{remark}
	\slashns
	
	Кроме того, мы в данной теореме нигде не используем структуру нашего пространства. Все, что нас интересует~--- это вероятность $p_i$ попадания события в каждый из блоков. А потому полученный критерий работает и в случае категориальных данных.
\end{remark}

Мы теперь знаем, как с помощью критерия Пирсона проверять простые гипотезы согласия. Для сложных же гипотез верна следующая теорема:

\begin{theorem}
	\slashns
	
	Пусть дана выборка $X_1, \, \ldots, X_n \sim \P$.
	
	И пусть дано семейство распределений $\{\P_{\theta} \, | \, \theta \in \Theta \subset \R^d  \}$, такое что:
	$$\P_\theta : \begin{array}{c|c|c|c}
	x_1 & x_2 & \ldots & x_s\\\hline
	p_1(\theta) & p_2(\theta) & \ldots & p_s(\theta)
	\end{array}$$
	
	Определим $\chi^2(\theta)$ следующим образом:
	$$\chi^2(\theta) := \sum\limits_{i=1}^{s} \dfrac{\big(\nu_i - np_i(\theta)\big)^2}{np_i(\theta)}, \text{ где } \nu_i = \#\{k : X_k = x_i \}$$
	
	И выберем такое $\widehat{\theta}$, что $\chi^2(\widehat{\theta}) = \inf\limits_{\theta} \chi^2(\theta)$.
	
	Тогда если верна гипотеза $H_0 : \P \in \{\P_\theta\}$, то верно следующее утверждение:
	$$\chi^2(\widehat{\theta}) = \sum\limits_{i=1}^{s} \dfrac{\big(\nu_i - np_i(\widehat{\theta})\big)^2}{np_i} \overset{H_0}{\underset{n\to\infty}{\to}} \chi_{s-d-1}^2$$
	
	\begin{proof}
		\slashns
		
		Без доказательства.
	\end{proof}
\end{theorem}

\begin{remark}
	\slashns
	
	На самом деле достаточно использовать не точное значение $\widehat{\theta}$, а лишь какую-нибудь близкую оценку, т.к. при небольшом изменении параметра значение функции $\chi^2(\theta)$ также меняется несильно.
\end{remark}

\begin{remark}
	\slashns
	
	Количество степеней свободы распределения хи-квадрат следует воспринимать следующим образом.
	
	Будем считать, что наше распределение однозначно задается с помощью $s-1$ параметра, т.к. все вероятности, кроме одной выбираются случайным образом, последняя же получается однозначно. А поскольку мы делаем оценку для $d$ параметров, то количество случайных параметров уменьшается до $s-d-1$.
\end{remark}

Эта теорема хороша не только тем, что позволяет строить критерии для сложных гипотез согласия, но и тем, что с ее помощью довольно просто строятся критерии для гипотез однородности и независимости.

\begin{statement}
	\slashns
	
	Пусть даны выборки $X_1, \, \ldots, \, X_{n_1} \sim \F$ и $Y_1, \, \ldots, \, Y_{n_2} \sim \text{G}$, где $\F$ и $\text{G}$~--- дискретные распределения со значениями $x_1, \, \ldots, \, x_s$.
	
	И пусть мы проверяем гипотезу однородности $H_0 : \F = \text{G}$.
	
	Посчитаем для каждого исхода количество соответствующих событий:
	$$\begin{array}{c|cccc}
	&x_1&x_2&\ldots&x_s\\\hline
	X&\nu_{1,1}&\nu_{1,2}&\ldots&\nu_{1,s}\\
	Y&\nu_{2,1}&\nu_{2,2}&\ldots&\nu_{2,s}\\\hline
	&\nu_{\cdot,1}&\nu_{\cdot,2}&\ldots&\nu_{\cdot,s}
	\end{array}, \text{ где } \nu_{\cdot,i} = \nu_{1,i} + \nu_{2,i}$$
	
	И построим оценки для вероятностей как $\widehat{p}_i = \dfrac{\nu_{\cdot,i}}{n_1+n_2}$.
	
	Тогда если гипотеза $H_0$ верна, то верно следующее утверждение:
	$$\sum\limits_{\substack{1 \le i \le 2\\1\le j \le s}} \dfrac{(\nu_{i,j} - n_i \widehat{p}_j)^2}{n_i \widehat{p}_j} \overset{d}{\to} \chi^2_{s-1}$$
	
	\begin{proof}
		\slashns
		
		Без доказательства.
	\end{proof}
\end{statement}

\begin{remark}
	\slashns
	
	Неформальное доказательство можно описать следующим образом: внутри полученной таблички каждая из строк содержит по $s-1$ независимому параметру, а т.к. мы строим по ней оценку для $s-1$ параметра, то в результате случайность остается лишь в $2(s-1) - (s-1) = s-1$ параметре.
\end{remark}

\begin{statement}
	\slashns
	
	Пусть дана совместная выборка $(X_1, Y_1), \, \ldots, \, (X_n, Y_n) \sim \P$.
	
	И пусть мы проверяем гипотезу независимости $H_0 : \Big(\exists \P_1, \, \P_2 : \P(x, y) = \P_1(x) \cdot \P_2(y) \Big)$.
	
	Посчитаем для каждого исхода количество соответствующих событий:
	$$\begin{array}{c|cccc|c}
	X \setminus Y &y_1&y_2&\cdots&y_r&\\\hline
	x_1 & \nu_{1,1}& \nu_{1,2} & \cdots & \nu_{1,r}&\nu_{1,\cdot}\\
	x_2 & \nu_{2,1}& \nu_{2,2} & \cdots & \nu_{2,r}&\nu_{2,\cdot}\\
	\vdots & \vdots & \vdots & \ddots & \vdots&\vdots\\
	x_s & \nu_{s,1}& \nu_{s,2} & \cdots & \nu_{s,r}&\nu_{s,\cdot}\\\hline
	& \nu_{\cdot,1} & \nu_{\cdot,2} & \cdots & \nu_{\cdot,r} &
	\end{array}, \text{ где } \nu_{\cdot,i} = \sum\limits_{j=1}^{s} \nu_{j,i}, \; \nu_{i,\cdot} = \sum\limits_{j=1}^{r} \nu_{i,j}$$
	
	И построим на основе полученной таблицы оценки для вероятностей как $\widehat{p}_{i,j} = \dfrac{\nu_{i,\cdot} \cdot \nu_{\cdot,j}}{n^2}$.
	
	Тогда если гипотеза $H_0$ верна, то верно следующее утверждение:
	$$\sum\limits_{\substack{1 \le i \le s\\1\le j \le r}} \dfrac{(\nu_{i,j} - n \widehat{p}_{i,j})^2}{n \widehat{p}_{i,j}} \overset{d}{\to} \chi^2_{(s-1)(r-1)}$$
	
	\begin{proof}
		\slashns
		
		Без доказательства.
	\end{proof}
\end{statement}

\begin{remark}
	\slashns
	
	Снова приведем неформальное доказательство сего утверждения: сама табличка изначально содержит $rs-1$ независимый параметр, оценку же мы приводим для $(r-1) + (s-1)$ параметров (соответственно, это $\nu_{i,\cdot}$ и $\nu_{\cdot,j}$, на основе которых вычисляются $\widehat{p}_{i,j}$). Отсюда получаем, что количество степеней свободы хи-квадрат~--- в точности $rs - 1 - (r-1) - (s-1) = (r-1)(s-1)$.
\end{remark}