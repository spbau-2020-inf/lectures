\Section{Сети в \texttt{Java}}

В джаве можно работать только с пакетами уровня \texttt{Application} и \texttt{Transport}.

Для работы с сетью используется \textbf{сокет} -- это низкоуровневый \texttt{API}, для пересылки байтов по сети. Сокеты в джаве поддерживают протоколы \texttt{TCP} и \texttt{UDP}.

\Subsection{\texttt{UDP-Socket}}

При создании сокета, ОС выделяет порт и закрепляет за этим сокетом.

Рассмотрим пример отправки \texttt{UDP}-запросов (клиент).

\begin{lstlisting}
try (DatagramSocket s = new DatagramSocket()) {
  DatagramPacket p = new DatagramPacket(buf, buf.length, remoteAddress);
  s.send(p);
}
\end{lstlisting}

Создаем сокет и пакет, который хотим передать. В качестве параметров конструктор\\\texttt{DatagramPacket} принимает массив байтов, длину массива и адрес получателя. Адрес включает в себя \texttt{IP}-адрес и порт получателя.

В нашем примере порт на нашем компьютере выбирается случайно. Если важно использовать конкретный порт, его номер можно передать в конструктор сокета.

Теперь рассмотрим пример сервера.

\begin{lstlisting}
try (DatagramSocket s = new DatagramSocket(port)) {
  byte[] buf = new byte [1024];
  DatagramPacket p = new DatagramPacket(buf, buf.length);
  s.receive(p);
}
\end{lstlisting}

Что будет, если придет пакет размера больше \texttt{buf.length} байтов? Тогда первые \texttt{buf.length} байта считаются, а остальные останутся в сокете, и их можно будет прочитать при следующем вызове \texttt{receive()}.

Далее у пакета можно спросить много разной информации -- кто отправил, какая длина, итд. В конце надо из полученных байтов как-то восстановить данные, которые мы ожидали получить.

\Subsection{\texttt{TCP-Socket}}

В отличие от \texttt{UDP}, в \texttt{TCP} мы не создаем пакет, а отправляем данные в сокет, и он всё формирует и отправляет за нас.

Рассмотрим процесс общения по \texttt{TCP}. Клиент хочет создать соединение c \texttt{vk.com}. Он посылает запрос на сервер, на 80 порт. Потом приходит второй клиент. \texttt{TCP}-соединение необходимо постоянно поддерживать, а значит нельзя со всеми клиентами общаться через один порт (иначе буфер, в который приходят запросы, будет переполняться). Поэтому, когда сервер получает запрос на 80 порт, он в ответ высылает номер другого порта, через который данный клиент должен продолжить общение с сервером.

В \texttt{UDP} такой проблемы нет, поэтому там все клиенты общаются через один порт.\\\\

Рассмотрим пример использования \texttt{TCP} сокета (клиент).

\begin{lstlisting}
Socket socket = new Socket ("localhost", 11111);

OutputStream os = socket.getOutputStream();
os.write(requestBytes);
os.flush();

InputStream is = socket.getInputStream();
is.read(responseBytes); 
\end{lstlisting}

Создаем сокет, передаем ему адрес и порт, с которыми соединяемся, запрашиваем у сокета \texttt{OutputStream} и \texttt{InputStream}. Метод \texttt{flush()} сбрасывает всё, что накопилось в буфере и пытается отправить это по сети.

Варианты конструктора для сокета:

\begin{itemize}
  \item \texttt{Socket(String host, int port) throws UnknownHostException, IOException} -- получает имя хоста и номер порта.
  \item \texttt{Socket(InetAddress host, int port) throws IOException} -- то же самое, только вместо имени хоста -- адрес.
  \item \texttt{Socket(String host, int port, InetAddress localAddress, int localPort) throws\\IOException} -- дополнительно получает локальный адрес и порт. Локальный адрес бывает надо передать, если у компьютера несколько \texttt{IP}-адресов, так как у него есть разные сетевые интерфейсы. Такой сокет может слушать, например, только \texttt{Wi-fi}.
  \item \texttt{Socket(InetAddress host, int port, InetAddress localAddress, int localPort)\\throws IOException}.
  \item \texttt{Socket()} -- неподключенный сокет. Чтобы его подключить, надо вызвать метод \texttt{connect()}.
\end{itemize}

Поскольку мы не знаем, сколько байтов нам хотят прислать, есть соглашение первым делом отправлять количество данных, а затем сами данные.

Рассмотрим пример серверного \texttt{TCP}-сокета.

\begin{lstlisting}
ServerSocket server = new ServerSocket(11111);
Socket socket = server.accept();

InputStream is = socket.getInputStream();
is.read(requestBytes); 

OutputStream os = socket.getOutputStream();
os.write(responseBytes);
os.flush();
\end{lstlisting}

\texttt{ServerSocket} -- это как раз тот сокет, который не общается с клиентом, а отправляет его на другой сокет. Ему в качестве параметра передается порт, на который приходят запросы соединения. Далее блокирующий метод \texttt{accept()} ждет, пока не придет клиент, а потом создает и возвращает сокет для общения.

\pagebreak

Варианты конструктора серверного сокета:

\begin{itemize}
  \item \texttt{ServerSocket(int port) throws IOException} -- cлушает заданный порт.
  \item \texttt{ServerSocket(int port, int backlog) throws IOException} -- получает максимальное количество клиентов в очереди на соединение.
  \item \texttt{ServerSocket(int port, int backlog, InetAddress address) throws IOException} -- cлушает только запросы, поступившие на заданный адрес.
  \item \texttt{ServerSocket() throws IOException} -- не привязанный сокет, привязывается методом \texttt{bind()}.
\end{itemize}

\Subsection{Сервер с блокирующей архитектурой}

Код из предыдущего примера сможет обработать только одного клиента, потому что \texttt{accept()} вызывается только один раз. Чтобы обрабатывать много клиентов, можно, например, выполнять этот код в цикле, но есть проблема. Мы работаем в одном потоке, поэтому сможем общаться только с одним клиентом одновременно. Еще одна проблема такого подхода в том, что создается соединение ради одноразового обмена пакетами.

Другой вариант -- принимать соединение и создавать отдельный поток для общения с клиентом. Проблема в том, что клиентов может быть очень много, а много потоков это плохо. Хочется их переиспользовать.

Для этого заведем тредпул. Теперь надо понять, какие задачи ему давать. Задача "пообщаться с клиентом" плохая, потому что обычно общение подразумевается длительное, поэтому одновременно будут обрабатываться столько клиентов, сколько потоков в пуле, а остальные будут ждать в очереди. Хорошая задача для тредпула -- обработка одного запроса (например, клиент просит отсортировать массив).

Помимо выполнения запроса, надо отправить клиенту ответ. Тут есть два варианта. Мы можем отправить ответ сами, а можем куда-то сложить результат, и кто-то другой его отправит.

\begin{wrapfigure}{r}{0.5\textwidth}
\begin{center}
\vspace{-20pt}
\begin{tikzpicture}[scale=0.5, very thick]

\draw (0, 10) rectangle (4, 11);
\draw (6, 7) rectangle (10, 9);
\draw (6, 5) rectangle (10, 7);
\draw (6, 2) rectangle (10, 4);
\draw (6, 0) rectangle (10, 2);
\draw (13, 8) rectangle (17, 9);
\draw (13, 7) rectangle (17, 8);
\draw (13, 6) rectangle (17, 7);
\draw (13, 5) rectangle (17, 6);
\draw (13, 4) rectangle (17, 5);
\node at (2, 10.5) {\texttt{accept}};
\node at (8, 8) {\texttt{read}};
\node at (8, 6) {\texttt{write}};
\node at (8, 3) {\texttt{read}};
\node at (8, 1) {\texttt{write}};
\node at (15, 10) {\texttt{ThreadPool}};

\begin{scope}[decoration = {markings, mark = at position 0.7 with {\arrow{>}}}]
\draw[postaction = {decorate}] (3, 10) to[out = -30, in = 30, distance = 2.5cm] (3, 11);
\draw[postaction = {decorate}] (8, 7) to[out = -30, in = 30, distance = 2.4cm] (8, 9);
\draw[postaction = {decorate}] (8, 2) to[out = -30, in = 30, distance = 2.4cm] (8, 4);
\end{scope}

\begin{scope}[decoration = {markings, mark = at position 1 with {\arrow{>}}}]
\draw[postaction = {decorate}] (1.5, 10) -- (1.5, 8) -- (6, 8);
\draw[postaction = {decorate}] (1, 10) -- (1, 3) -- (6, 3);
\draw[postaction = {decorate}] (0.5, 10) -- (0.5, 0);
\draw[postaction = {decorate}] (10, 8) -- (13, 7.6);
\draw[postaction = {decorate}] (13, 7.4) -- (10, 6);
\draw[postaction = {decorate}] (10, 3) -- (13, 4.6);
\draw[postaction = {decorate}] (13, 4.4) -- (10, 1);
\end{scope}

\end{tikzpicture}
\vspace{-10pt}
\end{center}
\end{wrapfigure}

Итого, есть основной поток, который принимает соединения и создает для каждого новый поток. Внутри этих потоков происходит чтение запросов клиентов и отправка задач в тредпул. Тредпул выполняет задачи и либо отправляет результат клиенту, либо возвращает его в поток, ответственный за этого клиента, и тот уже отправляет. На самом деле отправлять ответ в тредпуле плохо, потому что метод \texttt{write()} -- блокирующий. Он ждет, пока сеть не освободится для записи.

При таком подходе всё еще создается очень много потоков, но из них реально работают только потоки тредпула и потоки, которые в данный момент читают или пишут данные, общаясь с клиентом. Остальные потоки находятся в заблокированном состоянии и на работу других не влияют.  Такая архитектура называется \textbf{блокирующая}. Блокирующая -- потому что методы \texttt{accept()}, \texttt{read()} и \texttt{write()} блокируют поток, пока не дождутся. Она хорошо работает, если клиентов много, но одновременно общаются мало. В других ситуациях могут пригодиться другие архитектуры.

\pagebreak

Как мы помним, в многопоточных программах надо аккуратно работать с разделяемыми ресурсами. В нашем случае разделяемый ресурс -- это сеть. Посмотрим подробнее, что надо делать, а что не надо.

\begin{itemize}
  \item Сокеты, привязанные к разным портам, блокировать не надо, потому что они все работают независимо.
  \item У сокета есть \texttt{InputStream} и \texttt{OutputStream}. Если один поток читает, а другой пишет, блокировку брать тоже не надо.
  \item Брать блокировку надо, например, если разные потоки пытаются одновременно из одного сокета читать или одновременно в него писать. Тут как раз пригождается\\\texttt{SingleThreadExecutor}. Заводим такой для каждого клиента, и тредпул просит его выводить результаты выполненных задач.
\end{itemize}

\Subsection{Пакет \texttt{java.nio}}

\texttt{NIO} (New Input/Output) -- альтернативная библиотека ввода/вывода.

\Subsubsection{Каналы}

Каналы пришли на замену \texttt{InputStream} и \texttt{OutputStream} и имеют ряд существенных отличий от предшественников:

\begin{itemize}
  \item Канал двунапрваленный -- из него можно читать и одновременно в него писать.
  \item Каналы позволяют асинхронные чтение и запись.
  \item Канал пишет данные в буфер и читает из буфера.
\end{itemize}

Основные реализации каналов в \texttt{java.nio}:

\begin{itemize}
  \item \texttt{FileChannel} -- работает с файлами.
  \item \texttt{DatagramChannel} -- передает данные по \texttt{UDP}.
  \item \texttt{SocketChannel} -- передает данные по \texttt{TCP}.
  \item \texttt{ServerSocketChannel} -- принимает входящие \texttt{TCP} соединения. На каждое новое соединение создается \texttt{SocketChannel} для дальнейшего общения с клиентом.
\end{itemize}

\Subsubsection{Буферы}

Буфер -- это блок данных, в который можно писать данные и потом считывать. Буферы бывают байтовые (\texttt{ByteBuffer}), символьные (\texttt{CharBuffer}), вещественные (\texttt{DoubleBuffer}), итд.

По смыслу, буфер -- это массив с

\begin{itemize}
  \item заданной вместимостью (\texttt{capacity})
  \item отмеченной границей, сколько максимально данных можно читать/писать (\texttt{limit})
  \item отмеченной текущей позицией чтения/записи (\texttt{position})
\end{itemize}

\begin{wrapfigure}{r}{0.5\textwidth}
\begin{center}
\begin{tikzpicture}[scale=0.7, every node/.style={transform shape}, very thick]

\foreach \p in {0, 7} {

  \draw[rounded corners = 10pt] (-6.5 + \p, 0) rectangle (-2 + \p, 6);

  \foreach \i in {0, ..., 9} {
    \ifthenelse {\i > 4} {
      \draw[fill=green!30] (-3.6 + \p, 0.5 + 0.5 * \i) rectangle (-2.2 + \p, 1 + 0.5 * \i);
    } {
      \draw (-3.6 + \p, 0.5 + 0.5 * \i) rectangle (-2.2 + \p, 1 + 0.5 * \i);
    }
  }

  \node at (-2.9 + \p, 5.25) {\texttt{0}};
  \node at (-2.9 + \p, 4.75) {\texttt{1}};
  \node at (-2.9 + \p, 4.25) {\texttt{...}};
  \node at (-2.9 + \p, 0.75) {\texttt{cap-1}};
}

\node at (-5.5, 0.75) {\texttt{limit}};
\node at (-5.5, 2.75) {\texttt{position}};

\node at (1.5, 2.75) {\texttt{limit}};
\node at (1.5, 5.25) {\texttt{position}};

\node at (-0.75, 3.25) {\texttt{flip()}};

\node at (-4.25, -0.5) {Buffer -- режим записи};
\node at (2.75, -0.5) {Buffer -- режим чтения};

\begin{scope}[decoration = {
    markings,
    mark = at position 1 with {\arrow{>}}}
]

\draw[postaction={decorate}] (-4.85, 0.75) -- (-3.7, 0.75);
\draw[postaction={decorate}] (-4.55, 2.75) -- (-3.7, 2.75);

\draw[postaction={decorate}] (2.15, 2.75) -- (3.3, 2.75);
\draw[postaction={decorate}] (2.45, 5.25) -- (3.3, 5.25);

\draw[postaction={decorate}] (-1.75, 3) -- (0.25, 3);

\end{scope}

\end{tikzpicture}
\vspace{-20pt}
\end{center}
\end{wrapfigure}

Мы записали какие-то данные и находимся в состоянии записи (левая картинка). Хотим перейти в состояние чтения (правая картинка). Для этого вызываем метод \texttt{flip()}, который переводит \texttt{position} в нулевую позицию, а \texttt{limit} переводит в максимальное значение, которого достигал \texttt{position}.

Чтобы перевести буфер обратно из состояния чтения в состояние записи, используется \texttt{clear()} или \texttt{compact()}. Метод \texttt{clear()} переводит \texttt{limit} в \texttt{capacity}, а \texttt{position} в ноль, то есть непрочитанные данные забываются. Метод \texttt{compact()} копирует непрочитанные данные в начало буфера и устанавливает \texttt{position} в первую свободную ячейку.

В чем преимущество буфера над массивом? Можно передать буфер каналу, чтобы тот что-то в него записал, а потом снова передать каналу, и новые данные допишутся в этот же буфер.

Буфер можно получить методом \texttt{allocate()}.

\begin{lstlisting}
ByteBuffer buf1 = ByteBuffer.allocate(48); // буфер на 48 байтов
IntBuffer buf2 = IntBuffer.allocate(1024); // буфер на 1024 инта
\end{lstlisting}

Обратите внимание, что здесь, как и у всех классов \texttt{nio}, объекты создаются не конструкторами, а статическими методами.

Записать данные в буфер можно вручную методом \texttt{put()} или через канал.

\begin{lstlisting}
buf.put(127); // запись в буфер вручную
int bytesRead = channel.read(buffer); // запись в буфер из канала
\end{lstlisting}

Метод \texttt{read()} возвращает количество записанных в буфер байтов.

Прочитать данные из буфера можно вручную методом \texttt{get()} или через канал.

\begin{lstlisting}
byte value = buf.get(); // чтение из буфера вручную
int bytesWritten = channel.write(buffer); // чтение из буфера в канал
\end{lstlisting}

Рассмотрим пример.

\begin{lstlisting}
RandomAccessFile file = new RandomAccessFile("data/nio-data.txt", "rw");
FileChannel channel = file.getChannel();
ByteBuffer buf = ByteBuffer.allocate(48);
int bytesRead = channel.read(buf);

while (bytesRead != -1) {
  System.out.println("Read " + bytesRead);
  buf.flip();
  while(buf.hasRemaining()) {
    System.out.print((char) buf.get());
  }
  buf.clear();
  bytesRead = channel.read(buf);
}
file.close();
\end{lstlisting}

Открываем файл на чтение и запись, создаем канал и буфер на 48 байтов. Читаем из файла байты и пишем их в буфер. Если что-то еще прочиталось, выводим количество, переводим буфер в режим чтения, читаем по одному байту из буфера всё содержимое, выводим на экран, переводим буфер в режим записи и снова читаем из файла.

Если вместо \texttt{clear()} вызвать \texttt{flip}, может произойти что-то плохое. Если при записи \texttt{position} не дошел до конца, то \texttt{limit} установится не в конец, и при дальнейшем чтении доступный размер буфера будет меньше.

Можно пометить какую-то позицию в буфере с помощью метода \texttt{mark()}, а потом вернуться в неё, вызвав метод \texttt{reset()}. Например, если надо через буфер передавать пакеты с одинаковым заголовком, можно его один раз записать, а потом перед каждой записью переходить в позицию после заголовка и дописывать новый пакет.

Методу \texttt{read()} можно передать массив буферов. Тогда данные будут записываться в эти буферы последовательно -- сначала в первый, потом во второй, итд. Это называется \texttt{Scattering}. \texttt{} Методу \texttt{write()} тоже можно передать массив буферов. Тогда данные будут читаться сначала из первого, потом из второго, итд. Это называется \texttt{Gathering}.

Это полезно при работе с пакетами. Если пакеты имеют заголовок известной фиксированной длины, имеет смысл при получении и отправке передавать каналу два буфера -- с заголовком и с данными.

\begin{lstlisting}
ByteBuffer header = ByteBuffer.allocate(128);
ByteBuffer body = ByteBuffer.allocate(1024);

// заполняем заголовок и тело пакета

ByteBuffer[] bufferArray = { header, body };
channel.write(bufferArray);

// пример с чтением выглядит аналогично
\end{lstlisting}

\Subsubsection{Селекторы}

Пусть есть несколько каналов. Селектор может отвечать на вопрос, какие каналы в данный момент готовы к каким-либо действиям (читать, писать, ...).

Селекторы работают только с каналами в неблокирующем режиме. В этом режиме операции \texttt{accept()}, \texttt{read()} и \texttt{write()} не ждут, а завершаются очень быстро. Что это значит? Когда пакеты приходят на компьютер, они разворачиваются и складываются в специальный буфер. Грубо говоря, неблокирующая операция \texttt{read()} заглядывает в буфер. Если там что-то есть, она это прочитывает (возможно даже не полностью), а иначе возвращает ноль. Блокирующий \texttt{read()} будет сидеть и ждать, пока придет столько байтов, сколько его попросили прочитать.

Почему селекторы работают с каналами только в неблокирующем режиме? Во-первых, потому что селектор постоянно взаимодействует с каналами. Если бы они блокировались, всё бы зависало. Во-вторых, идея селектора в том, чтобы в одном потоке взаимодействовать с кучей клиентов сразу. Если этот поток блокируется первой же операцией, толку в нем мало.

Создание и регистрация селектора:

\begin{lstlisting}
Selector selector = Selector.open(); // создаем селектор
channel.configureBlocking(false); // переводим канал в неблокирующий режим
SelectionKey key = channel.register(selector, SelectionKey.OP_READ); // регистрируем канал
                                                                                 // в селекторе

\end{lstlisting}

\pagebreak

Вторым аргументом метод \texttt{register()} принимает набор ключей, задающих, какие изменения в канале должен слушать селектор:

\begin{itemize}
  \item \texttt{SelectionKey.OP\_CONNECT}
  \item \texttt{SelectionKey.OP\_ACCEPT}
  \item \texttt{SelectionKey.OP\_READ}
  \item \texttt{SelectionKey.OP\_WRITE}
\end{itemize}

Ключи можно комбинировать с помощью оператора побитового "или".

Метод \texttt{register()} возвращает объект класса \texttt{SelectionKey}. Он содержит следующую информацию:

\begin{itemize}
  \item набор ключей, на которые подписан селектор (interest set):

\begin{lstlisting}
// получаем interest set
int interestSet = selectionKey.interestedOps();
// подписаны ли мы на чтение
boolean isInterestedInRead = interestSet & SelectionKey.OP_READ;
// подписаны ли мы на запись
boolean isInterestedInWrite = interestSet & SelectionKey.OP_WRITE;
\end{lstlisting}

  \item набор действий, к которым готов канал (ready set):

\begin{lstlisting}
// первый способ
int readySet = selectionKey.readyOps();
boolean isReadyToRead = readySet & SelectionKey.OP_READ;

// второй, аналогичный способ
selectionKey.isReadable();
\end{lstlisting}

  \item ссылки на канал и селектор:

\begin{lstlisting}
Channel channel = selectionKey.channel();
Selector selector = selectionKey.selector();
\end{lstlisting}

  \item прикрепленный объект:

\begin{lstlisting}
// прикрепляем так
SelectionKey key = channel.register(selector, SelectionKey.OP_READ);
key.attach(myObject);

// или так
SelectionKey key = channel.register(selector, SelectionKey.OP_READ, myObject);

// получаем так
Object attachedObject = key.attachment();
\end{lstlisting}

\end{itemize}

После того, как мы зарегистрировали каналы в селекторе, можно использовать один из методов \texttt{select()}. Эти методы возвращают количество каналов, которые стали готовы к интересующим нас действиям с момента предыдущего вызова. То есть если мы однажды вызвали \texttt{select()} и получили 1, и после этого еще один канал стал готов, то следующий вызов снова вернет нам 1, не зависимо от того, обработали мы первый канал или нет.

\pagebreak

Метод \texttt{select()} бывает:

\begin{itemize}
  \item \texttt{int select()} -- блокирующий метод. Ждет, когда хотя бы один канал станет готов.
  \item \texttt{int select(long timeout)} -- ждет не дольше заданного времени.
  \item \texttt{int selectNow()} -- выдает результат сразу, возможно пустой.  
\end{itemize}

Также у селектора можно вызвать метод \texttt{selectedKeys()} и получить \texttt{Set<SelectionKey>}, содержащий информацию о каналах, готовых к интересующим нас действиям. Из полученных \texttt{SelectionKey} можно получать сами каналы и прикрепленные объекты. Например, можно было прикрепить к каналу абстракцию, связанную с клиентом и теперь как-то с ней работать.

Пример. Получаем готовые к чему-либо потоки, перебираем их итератором, проверяем, к чему готов каждый из них, и удаляем \texttt{SelectionKey} из множества. Удалять ключи надо, потому что каждый следующий вызов \texttt{selectedKeys()} добавляет новые ключи к уже имеющимся, поэтому информация о старых ключах может быть уже неактуальной.

\begin{lstlisting}
Set<SelectionKey> selectedKeys = selector.selectedKeys();
Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

while (keyIterator.hasNext())  {
  SelectionKey key = keyIterator.next();
  if (key.isAcceptable()) {
    // ServerSocketChannel принял входящее соединение
  } else if (key.isConnectable()) {
    // установлено соединение с удаленным сервером
  } else if (key.isReadable()) {
    // канал готов к чтению
  }  else if (key.isWritable()) {
    // канал готов к записи
  }
  keyIterator.remove();
}
\end{lstlisting}

\Subsubsection{\texttt{SocketChannel}}

\texttt{SocketChannel} -- это канал для работы с \texttt{TCP} соединениями. Как и обычный \texttt{Socket}, его можно создать вручную и привязать к удаленному серверу либо получить на сервере из метода \texttt{accept()}.

Поскольку в \texttt{nio} конструкторов нет, вручную канал создается статическим методом \texttt{open()}.

\begin{lstlisting}
SocketChannel channel = SocketChannel.open();
channel.connect(new InetSocketAddress("http://aptu.ru", 80));
\end{lstlisting}

Теперь мы можем читать и писать через буферы. Пример записи.

\begin{lstlisting}
String newData = "New String to write to file..." + System.currentTimeMillis();
ByteBuffer buf = ByteBuffer.allocate(48);
buf.clear();
buf.put(newData.getBytes());
buf.flip();

while (buf.hasRemaining()) {
  channel.write(buf);
}
\end{lstlisting}

После использования канал, как и сокет и файл, надо закрыть методом \texttt{close()}.

В неблокирующем режиме метод \texttt{connect()} завершается быстро, даже если еще не успел подключиться. Чтобы узнать, когда канал уже подключился, есть метод \texttt{finishConnect()}. Выглядит это так.

\begin{lstlisting}
SocketChannel channel = SocketChannel.open();
channel.configureBlocking(false);
channel.connect(new InetSocketAddress("vk.com", 80));

while (!socket.finishConnect()) {
  // ждем или делаем еще что-то
}
\end{lstlisting}

Методы \texttt{read()} и \texttt{write()} в неблокирующем режиме могут прочитать/записать данные не до конца и завершиться. Поэтому их надо вызывать в цикле, пока они не прочитают/запишут столько, сколько нужно.

\Subsubsection{\texttt{ServerSocketChannel}}

\texttt{ServerSocketChannel} принимает входящие соединения и создает им каналы для общения.

Пример с блокирующим режимом.

\begin{lstlisting}
ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
serverSocketChannel.socket().bind(new InetSocketAddress(9999));

while (true) {
  SocketChannel socketChannel = serverSocketChannel.accept();
  // общаемся с клиентом
}

serverSocketChannel.close();
\end{lstlisting}

Пример с неблокирующим режимом. Здесь \texttt{accept()} надо вызывать в цикле.

\begin{lstlisting}
ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
serverSocketChannel.socket().bind(new InetSocketAddress(9999));
serverSocketChannel.configureBlocking(false);

while (true) {
  SocketChannel socketChannel = serverSocketChannel.accept();
  if (socketChannel != null) {
    //обрабатываем клиента
  }
}
\end{lstlisting}

Так писать смысла нет, потому что мы, вместо блокировки, активно ожидаем. Неблокирующий режим полезно использовать вместе с селекторами.

\pagebreak

\Subsubsection{\texttt{DatagramChannel}}

\texttt{DatagramChannel} -- это канал для работы с \texttt{UDP} соединениями.

Пример. Обратите внимание, что чтобы привязать канал к порту, надо вызвать метод \texttt{socket()}, хотя формально у \texttt{UDP} никаких сокетов нет.

\begin{lstlisting}
DatagramChannel channel = DatagramChannel.open();
channel.socket().bind(new InetSocketAddress(9999));

ByteBuffer buf = ByteBuffer.allocate(48);
buf.clear();

channel.receive(buf);
....
String newData = "New  String  to  write  to  file..." + System.currentTimeMillis();
buf.clear();
buf.put(newData.getBytes());
buf.flip();

int bytesSent =  channel.send(buf, new InetSocketAddress("aptu.ru", 80));
\end{lstlisting}

Неблокирующий режим для \texttt{DatagramChannel} есть, но он бесполезен, так как, во-первых, пакеты могут теряться, а во-вторых, все клиенты шлют пакеты на один порт, поэтому их все можно обрабатывать в одном потоке.

\Subsection{Сервер с неблокирующей архитектурой}

\begin{wrapfigure}{r}{0.45\textwidth}
\vspace{-20pt}
\begin{center}
\begin{tikzpicture}[scale=0.5, very thick]

\draw (8, 10) rectangle (17, 12);
\draw (8, 11) -- (17, 11)
(11, 10) -- (11, 12)
(14, 10) -- (14, 12);
\draw (2, 10) rectangle (6, 11);
\draw (6, 5) rectangle (10, 9);
\draw (6, 0) rectangle (10, 4);
\draw (13, 6) rectangle (17, 7);
\draw (13, 5) rectangle (17, 6);
\draw (13, 4) rectangle (17, 5);
\draw (13, 3) rectangle (17, 4);
\draw (13, 2) rectangle (17, 3);
\node at (9.5, 11.5) {\small{\texttt{r buff}}};
\node at (9.5, 10.5) {\small{\texttt{w buff}}};
\node at (12.5, 11.5) {\small{\texttt{r buff}}};
\node at (12.5, 10.5) {\small{\texttt{w buff}}};
\node at (15.5, 11.5) {\small{\texttt{r buff}}};
\node at (15.5, 10.5) {\small{\texttt{w buff}}};
\node at (12.5, 13) {\texttt{clients}};
\node at (4, 10.5) {\texttt{accept}};
\node at (8, 7) {\texttt{Selector}};
\node at (8, 6.4) {\texttt{(read)}};
\node at (8, 2) {\texttt{Selector}};
\node at (8, 1.4) {\texttt{(write)}};
\node at (15, 8) {\texttt{ThreadPool}};

\begin{scope}[decoration = {markings, mark = at position 0.7 with {\arrow{>}}}]
\draw[postaction = {decorate}] (5, 10) to[out = -30, in = 30, distance = 2.5cm] (5, 11);
\draw[postaction = {decorate}] (8, 5) to[out = -30, in = 30, distance = 2.7cm] (8, 9);
\draw[postaction = {decorate}] (8, 0) to[out = -30, in = 30, distance = 2.7cm] (8, 4);
\end{scope}

\begin{scope}[decoration = {markings, mark = at position 1 with {\arrow{>}}}]
\draw[postaction = {decorate}] (3.5, 10) -- (3.5, 7) -- (6, 7);
\draw[postaction = {decorate}] (10, 7) -- (13, 4.6);
\draw[postaction = {decorate}] (13, 4.4) -- (10, 2);
\end{scope}

\end{tikzpicture}
\end{center}
\vspace{-30pt}
\end{wrapfigure}

Первое что нужно -- \texttt{accept()}. Для этого обычно в отдельном потоке запускают блокирущий \texttt{ServerSocketChannel}. Для каждого нового подключения создается \texttt{SocketChannel} и отправляется в селектор, крутящийся в другом потоке и следящий за чтением. Поскольку в неблокирующем режиме мы можем не прочитать всё сразу, для каждого канала хранятся еще буферы чтения и записи. Каждый раз, когда какой-то поток готов к чтению, данные из него читаются и дописываются в соответствующий буфер чтения. Когда в каком-то буфере накапливается полностью сообщение, оно отправляется на обработку в тредпул. После этого результат записывается в буфер записи, канал соответствующего клиента регистрируется в селектор для записи, и ответ постепенно отправляется. Когда ответ полностью отправлен, канал удаляется из селектора. Получилось |\texttt{ThreadPool}| + 3 потока.

Здесь можно сэкономить поток или даже два. Во-первых, можно селекторы объединить в один поток и вызывать их в цикле друг за другом. Во-вторых, можно \texttt{ServerSocketChannel} сделать неблокирующим и поместить в селектор для чтения. В этом случае тредпулу можно будет выделить больше потоков, чтобы они эффективно распараллеливались на всех имеющихся ядрах.

\href{http://tutorials.jenkov.com/java-nio/non-blocking-server.html}{По этой ссылке} можно почитать чуть больше про \texttt{nio} и неблокирующую архитектуру.

\Subsection{Сравнение архитектур}

Обработка данных в обеих архитектурах происходит в тредпуле. Разница в том, что в неблокирующей для общения с клиентами используются 2 потока с селекторами, а не 2n, как в блокирующей.

Узкое место неблокирующей архитектуры -- селекторы. Когда есть много клиентов, которые часто шлют запросы, селектор, работающий в одном потоке, работает крайне медленно. Для блокирующей архитектуры эта ситуация тоже плохая, потому что создается куча потоков и происходит частое переключение контекста. Это решается увеличением ядер и процессоров.

Итого, неблокирующая архитектура выигрывает, когда клиентов много, но они не очень активно пишут. Если клиентов мало, но пишут они часто, блокирующая архитектура тормозит из-за частого переключения контекста, а неблокирующая -- когда как, зависит от пропускной способности селектора.

Пример, когда блокирующая лучше -- если запросы от клиентов большие. Блокирующий \texttt{read()} их будет быстро считывать, сразу отправлять на обработку и засыпать. А неблокирующий будет долго крутиться в цикле, пока не считает всё.

На практике чаще всего выбирают блокирующую архитектуру, потому что её проще реализовывать, и она лучше масштабируется, то есть её можно увеличить, купив еще несколько процессоров.

\Subsection{Асинхронные каналы}

Асинхронные каналы позволяют запустить чтение/запись в отдельном потоке и вернуть \texttt{Future}, в котором потом появится результат, или по завершении вызвать некоторую функцию (\texttt{callback}), в которую результат передастся и сразу обработается. При этом сам метод \texttt{read()}/\texttt{write()} выполнится мгновенно. Если делать через \texttt{Future}, операцию можно будет отменить, вызвав у \texttt{Future} метод \texttt{cancel()}. 

\texttt{Callback} передается методу \texttt{read()}/\texttt{write()} в виде объекта, реализующего интерфейс\\
\texttt{CompletionHandler<V, A>}. Если делать таким образом, то операцию отменить нельзя. У этого интерфейса есть два метода -- \texttt{completed(v, a)} и \texttt{failed(exception, a)}.

\texttt{CompletionHandler<V, A>} параметризуется двумя типами. Тип \texttt{V} -- это результат. Он попадает только в метод \texttt{completed()}. В метод \texttt{failed()} вместо этого передается исключение. Тип \texttt{A} -- это тип прикрепляемого объекта, который чаще всего называют \textbf{контекстом}.

Пример. 

\begin{lstlisting}
socketChannel.read(
  buffer, // буфер, куда читать
  context, // контекст
  new CompletionHandler<Integer, Context>() {
    public void completed(Integer result, Context context) { // если успех
      if (context.process(result)) { // обрабатываем то что считали
        socketChannel.read(buffer, context, this); // читаем еще раз с тем же обработчиком
      }
    }
    public void failed(Throwable e, Context context) { // если неуспех
      context.error(); // обрабатываем ошибку контекстом
      e.printStackTrace();
    }
  }
);
\end{lstlisting}

По сути, такая асинхронная конструкция заменяет бесконечный цикл, который был в примере блокирующей архитектуры. Отличие в том, что все колбэки выполняются в отдельной группе потоков. Таким образом мы экономим потоки, потому что не создаем их для каждого клиента. При этом в колбэках не стоит делать долгие вычисления, потому что вся эта группа потоков будет тормозить. 

% TODO операции синхронные или нет?

Интерфейс \texttt{AsynchronousByteChannel} -- наследник интерфейса \texttt{AsynchronousChannel} -- читает/пишет в буфер. В качестве результата выдает \texttt{Integer} -- количество прочитанных/записанных байтов.

Класс \texttt{AsynchronousSocketChannel} реализует этот интерфейс. 

\begin{lstlisting}
// создаем асинхронный сетевой канал
AsynchronousSocketChannel socketChannel = AsynchronousSocketChannel.open();
// привязываем к локальному адресу
socketChannel.bind(localAddress);
// подключаемся к удаленному адресу
socketChannel.connect(remoteAddress);
// или 
socketChannel.connect(remoteAddress, context, handler);
// читаем/пишем
. . .
// прерываем все запущенные операции чтения
socketChannel.shutdownInput()
// прерываем все запущенные операции записи
socketChannel.shutdownOutput()
\end{lstlisting}

Класс \texttt{AsynchronousServerSocketChannel} принимает входящие подключения

\begin{lstlisting}
// создаем
AsynchronousServerSocketChannel ssc = AsynchronousServerSocketChannel.open();
// привязываем к локальному адресу
ssc.bind(localAddress);
// устанавливаем соединение и получаем AsynchronousSocketChannel
ssc.accept();
// или
ssc.accept(context, handler);
\end{lstlisting}

Класс \texttt{AsynchronousChannelGroup} -- это как раз группа потоков, в которой могут выполняться колбэки асинхронных операций. По умолчанию они все исполняются в одной группе, размер которой недокументирован. Обычно размер либо 1, либо количество ядер - 1.

Такую группу можно создать вручную, но так делают редко.

\begin{lstlisting}
// создаем
channelGroup = AsynchronousChannelGroup.withThreadPool(executor);
// или
channelGroup = AsynchronousChannelGroup.withCachedThreadPool(executor, minThreads);
// или
channelGroup = AsynchronousChannelGroup.withFixedThreadPool(threadsNumber, threadFactory);
// создаем канал с привязкой к нашей группе
AsynchronousSocketChannel socketChannel = AsynchronousSocketChannel.open(channelGroup);
// завершаем работу
channelGroup.shutdown();
// завершаем работу и разрываем соединения
channelGroup.shutdownNow();
// дожидаемся завершения
channelGroup.awaitTermination(timeout, units);
\end{lstlisting}