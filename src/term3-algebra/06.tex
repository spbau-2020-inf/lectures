\begin{definition}
	\slashns
	
	Пусть $V$~--- конечномерное векторное пространство над полем $\R$.
	
	Введем над пространством $V\oplus V$ умножение на комплексный аргумент по следующему правилу: $(\alpha + \beta i)(u, v) = (\alpha u - \beta v, \alpha v + \beta u)$.
	
	Полученное пространство обозначается $V^\CC$ и называется комплексификацией пространства $V$.    
	
	Вектора из пространства $V^\CC$ будем обозначать как $(u, v) = u + iv$.	
\end{definition}

\begin{properties}
	\slashns
	
	1) $v \mapsto (v, 0)$~--- вложение $V \hookrightarrow V^\CC$.
	
	2) Если $a$~--- гомоморфизм пространств $V$ и $U$, то $a^\CC(v_1 + iv_2) := a(v_1) + ia(v_2)$~--- гомоморфизм пространств $U^\CC$ и $V^\CC$
	
	\begin{proof}
		\slashns
		
		1) Обозначим вложение $f$.
		
		$\alpha f(v) + f(u) = \alpha(v, 0) + (u, 0) = (\alpha v + u, 0) = f(\alpha v + u)$.
		
		2) $a^\CC(\alpha(v_1 + iv_2) + (v_1' + iv_2')) = a^\CC((\alpha v_1 + v_1') + i(\alpha v_2 + v_2')) = a(\alpha v_1 + v_1') + ia(\alpha v_2 + v_2') = \alpha(a(v_1) + ia(v_2)) + (a(v_1') + ia(v_2')) = \alpha a^\CC(v_1 + iv_2) + a^\CC(v_1' + v_2')$
	\end{proof}
\end{properties}

%TODO могу попробовать дописать
\begin{remark}
	\slashnss
	
	1) $\dim V^\CC = \dim V$
	
	2) Матрица $a^\CC$ из базиса $U^\CC$ в базис $V^\CC$ совпадает с матрицей $a$ из базиса $U$ в базис $V$.
	
	\begin{proof}
		\slashns
		
		1) Вспомним, что у нас существует естественное вложение $v \mapsto (v, 0)$.
		
		Тогда покажем, что если $e_1, \ldots, e_n$~--- базис $V$, то $(e_1, 0), \ldots, (e_n, 0)$~--- базис $V^{\CC}$.
		
		Линейная независимость:
		
		$\sum\limits_{j=1}^{n} (a_j + i b_j) (e_j, 0) = (0, 0) \implies \left(\sum\limits_{j=1}^{n} a_j e_j, \sum\limits_{j=1}^{n} b_j e_j\right) = (0, 0) \implies a_j, b_j = 0$
		
		Система образующих:
		
		$(v, u) = \left(\sum\limits_{j=1}^{n} a_j e_j, \sum\limits_{j=1}^{n} b_j e_j \right) = \sum\limits_{j=1}^{n} a_j (e_j, 0) + \sum\limits_{j=1}^{n} i b_j (e_j, 0)$
		
		2) В силу первого замечания.
	\end{proof}
\end{remark}

\Section{Операторы в евклидовых и эрмитовых пространствах}{А.Ермилов, А.Киракосян}

\Subsection{Унитарные и ортогональные операторы}

На протяжении всего параграфа $V$ -- евклидово или эрмитово пространство.

\begin{definition}
	\slashnss
	
	Множество автоморфизмов $V$: $\{a \in \Aut(V)\;|\;\angles{a(u), a(v)} = \angles{u, v}\;\forall u, v \in V\}$ называется группой изометрий $V$.
\end{definition}

\begin{remark}
	\slashns
	
	Любая изометрия переводит базис в базис, т.к. является автоморфизмом. 
\end{remark}

\begin{statement}
	\slashnss
	
	Если $V$~--- евклидово или эрмитово пространство, то следующие условия эквивалентны:
	
	\begin{enumerate}
		\item $a$~--- изометрия
		
		\item $\forall v \in V$: $\norm{a(v)} = \norm{v}$
		
		\item Для всякого базиса $e_1,\ldots,e_n$ пространства $V$ выполняется $\overline{A}^TGA = G$, где $A$~--- матрица автоморфизма $a$ в базисе $e$, $G$~--- матрица Грама.
		
		\item $a$ переводит некоторый ортонормированный базис в ортонормированный.
	\end{enumerate}

	\begin{proof}
		\slashnss
		
		\begin{enumerate}
			\item [$1\Rightarrow2$] 
			$\norm{v}^2 = \angles{v, v}$
			
			\item [$2\Rightarrow1$]
			Евклидово: 
			
			$\angles{u, v} = \frac{1}{2}(\norm{u + v}^2 - \norm{u}^2 - \norm{v}^2) = \frac{1}{2}(\norm{a(u + v)}^2 - \norm{a(u)}^2 - \norm{a(v)}^2) = \angles{a(u), a(v)}$
			
			Эрмитово: 
			
			$\angles{u, v} = \Re \angles{u, v} + i\Re\angles{iu, v}$
			
			$\Re \angles{u, v} = \frac{1}{2}(\norm{u +v}^2 - \norm{u}^2 - \norm{v}^2)$
										
			\item [$1\Rightarrow3$]
			$\angles{u, v} = \angles{a(u), a(v)}$.
				
			Тогда, т.к. в матрице Грама записаны скалярные произведения, то матрицы Грама в базисах $e_1,\ldots,e_n$ и $a(e_1), \; \ldots, \; a(e_n)$ совпадают. 
			
			А значит получаем, что $\overline{A}^TGA = G$.
			
			Пояснение: такое домножение является заменой базиса матрицы Грама, а, как мы поняли, в базисах $e_1, \ldots e_n$ и $a(e_1), \ldots a(e_n)$ они совпадают) 
				
			\item [$3\Rightarrow4$]
			Базис ортонормирован тогда и только тогда, когда матрица Грама в нем единичная.
			
			Мы знаем, что матрица Грама не поменялась, а значит и базис остался ортонормированным.  

			\item[$4\Rightarrow1$]
			Пусть наш базис это $e_1, \; \ldots, \; e_n$.
			
			Очевидно, что на базисных векторах скалярное произведение сохранилось (т.к. базисы ортонормированные), а тогда оно сохранилось и для любой пары по линейности скалярного произведения и оператора $a$.
		\end{enumerate}
	\end{proof}
\end{statement}

\begin{remark}
	\slashns 

	Мы заодно доказали, что любой ортонормированный базис переходит в ортонормированный.
\end{remark}

\begin{definition}
	\slashnss
	
	Пусть $a \in \Aut(V)$, $\norm{a(v)} = \norm{v}\;\; \forall v\in V$. Тогда:
	
	1) Если $V$ эрмитово, то оператор $a$ называется унитарным.
	
	2) Если $V$ евклидово, то оператор $a$ называется ортогональным.
\end{definition}

\begin{remark}
	\slashns

	Исходя из предыдущего утверждения мы просто переопределили понятия изометрии для эрмитовых и евклидовых пространств.
\end{remark}

\begin{statement}
	\slashnss
	
	$V$~--- конечномерное векторное пространство над $\R$, $\dim V > 1$, $a \in \End(V)$
	
	Тогда в $V$ существует одномерное или двумерное подпространство $U$ инвариантное относительно $a$. 

	\begin{proof}
		\slashnss
		
		Рассмотрим характеристический многочлен $\chi_a$.
		
		\begin{enumerate}
		\item Если $\chi_a$ имеет вещественный корень $\lambda$, т.e. $\chi_a(\lambda) = 0$.
		
		Тогда рассмотрим $U = \angles{v}$, где $v$~--- собственный вектора для $\lambda$. $U$ будет одномерным и инвариантным.		
		\item Если же $\chi_a$ не имеет вещественных корней то рассмотрим комплексификацию $a^\CC \in \End(V^\CC)$ Тогда
		
		$\chi_a = \chi_{a^\CC} \in \R[x]$. Рассмотрим корень $z = \alpha + i\beta$ и соответствующий $z$ собственный вектор $u + iv$.
		
		Тогда $a(u) + ia(v) = a^\CC(u + iv) = (\alpha + i\beta)(u + iv) = \alpha u - \beta v + i(\beta u + \alpha v)$
		
		Из полученного равенства следуют равенства вещественной и мнимой части. Т.е. 
		
		$\begin{cases}
		a(u) = \alpha u - \beta v \\
		a(v) = \beta u + \alpha v
		\end{cases}$
		
		Тогда $U = \angles{u, v}$~--- двумерное инвариантное подпространство $V$.
		\end{enumerate} 
	\end{proof}
\end{statement}

\begin{denotation}
	$O(V)$ - группа ортогональных операторов.
\end{denotation}

\begin{statement}
	\slashns
	
	Если $V$~--- одномерное пространство над $\R$, то группа ортогональных операторов состоит их двух элементов: $id, -id$.

	\begin{proof}
		\slashns
		
		$V = \angles{v}$; 
		
		$a(v) = \lambda v$
		
		$\norm{\lambda v} = \norm{a(v)} = \norm{v} \implies |\lambda| = 1$
		
		А это значит, что $a = \pm id$.
	\end{proof}	
\end{statement}

\begin{statement}
	\slashns 
 
	Если $V$~--- двумерное пространство над $\R$, то любой ортогональный оператор имеет вид
	
	$$A = \begin{pmatrix}
		\cos \varphi & -\sin \varphi \\
		\sin \varphi & \cos \varphi
	\end{pmatrix}$$		 

	\begin{proof}
		\slashns 
		
	Рассмотрим ортонормированный базис.
	
	Тогда т.к. $A^TA = E$, то $\det A = \pm 1$. 
	
	Рассмотрим подгруппу $SO(2) \le O(2)$ образованную матрицами, у которых определитель равен $1$.
	
	$[O(2) : SO(2)] = 2$, т.к. существует два класса, а именно те матрицы, у которых определитель равен $1$ и те, у которых определитель $-1$.
	
	Пусть $A \in SO(2) = \begin{pmatrix}
	a & b \\ 
	c & d
	\end{pmatrix}$. Тогда у нас есть два условия:
	
	$$\begin{cases}
	AA^T = E \\
	\det A = 1 \\
	\end{cases} \iff \begin{cases}
	a^2 + b^2 = 1 \\
	c^2 + d^2 = 1 \\
	ac + bd = 0 \\
	ad - bc = 1 \\
	\end{cases} \implies A = \begin{pmatrix}
	\cos \varphi & -\sin \varphi \\
	\sin \varphi & \cos \varphi
	\end{pmatrix}$$
	
	Подробнее про последний переход. Т.к. $a^2 + b^2 = 1$, то можно считать, что $a = \sin \varphi$; $b = \cos \varphi$.
	Аналогично для $c$ и $d$.
	
	Полученная матрица является матрицей поворота на угол $\varphi$, а значит группа $SO(2)$ это группа поворотов.

	Осталось рассмотреть второй класс смежности группы $O(2)$.
	
	$B \in O(2)$
	
	Тогда, если поменять местами строки $B$, то получится матрица $A \in SO(2)$, т.е. $B = A\begin{pmatrix}
	0 & 1 \\
	1 & 0
	\end{pmatrix}$, где $A \in OS(2)$.
	
	Такие матрицы сводятся к матрицам из $SO(2)$ заменой порядка базисных векторов.		
	\end{proof}	
\end{statement} 


\begin{theorem}
	\slashnss
	
	\begin{enumerate}
		\item $V$ - эрмитово, $a \in \End(V)$. Тогда следующие условия равносильны:
		
		\begin{enumerate}
			\item[1)] $a$~--- унитарный.
			\item[2)] $\Spec(a) \subset \{z \in \CC \;:\; |z| = 1\}$, и существует ортонормированный базис в котором матрица $a$ диагональна.
		\end{enumerate}
		
		\item $V$~--- евклидово, $a \in \End(V)$. Тогда следующие условия равносильны:
		
		\begin{enumerate}
			\item[1)] $a$~--- ортогональный.
			\item[2)] существует ортонормированный базис в котором матрица $a$ имеет вид 
			$$\begin{pmatrix}
			A(\varphi_1) & 0 & \cdots & 0 \\
			0 & A(\varphi_2) & \cdots & 0 \\
			\vdots & \vdots & \ddots & \vdots \\
			0 & 0 & \cdots & A(\varphi_n)
			\end{pmatrix}$$
			
			($A$ ~--- матрицы из рассуждений выше)
		\end{enumerate}
		
		\item Собственные векторы оператора $a$, соответствующие различным собственным числам, ортогональны.
	\end{enumerate}

	\begin{proof}
		\slashns
		
		\begin{enumerate}
			\item 
			``$1 \impliedby 2$''
			
			Существует базис в котором матрица имеет вид
		
			$A = \begin{pmatrix}
				\lambda_1 & \cdots & 0 \\
				\vdots & \ddots & \vdots \\
				0 & \cdots & \lambda_n \\
			\end{pmatrix}$
			
			При этом, т.к. все $\lambda_i \in \Spec(a)$, то $|\lambda_i| = 1$. Тогда $A^TA = E$, что и требовалось.
			
			``$1 \implies 2$''
			
			Т.к. $a$~--- унитарный, то $a \in \Aut(V)$.
			
			Пусть $v$~--- собственный вектор для числа $\lambda$. Тогда $\norm{v} = \norm{a(v)} = |\lambda|\norm{v} \implies |\lambda| = 1$.
			
			Теперь индукция по размерности $V$. Если $\dim V = 1$, то матрица оператора уже диагональна.
			
			Теперь пусть $\dim V > 1$, а $\lambda$~--- собственное число. 
			
			Тогда $V = \angles{v} \oplus \angles{v}^\perp$.

			Осталось доказать, что $\angles{v}^\perp$ инвариантен относительно $a$.
			
			$\forall u \in \angles{v}:$ $\angles{a(v), u} = \angles{a(v), a(\frac{1}{\lambda}u)} = \angles{v, \frac{1}{\lambda}u} = \frac{1}{\lambda}\angles{v, u} = 0$
			
			\item 
			``$1 \impliedby 2$''
			
			В каком-то базисе матрица имеет вид:
			$$\begin{pmatrix}
			A(\varphi_1) & 0 & \cdots & 0 \\
			0 & A(\varphi_2) & \cdots & 0 \\
			\vdots & \vdots & \ddots & \vdots \\
			0 & 0 & \cdots & A(\varphi_n)
			\end{pmatrix}$$
			
			Тогда в нем $A^TA = E$, т.к. для каждой клеточки так.
				
			``$1 \implies 2$''
			
			Сразу заметим, что раз $a$~--- ортогональный, то $a \in \Aut(V)$.
			
			Доказываем индукцией по $n$. 
			
			Базу для $n = 1, 2$ мы уже проверили, когда изучали группу ортонормированных операторов. 
			
			Сделаем переход. Мы доказали, что у любого пространства есть инвариантное подпространство размерности $2$ или $1$. Найдем его и назовем $U$.
			
			Тогда $V = U \oplus U^\perp$. 
			
			Чтобы воспользоваться предположением нужно показать, что $U^\perp$ инвариантно относительно нашего оператора.

			Если $\dim U = 1$, то $U = \angles{v}$, а такой случай мы уже проверяли в первом пункте теоремы.
			
			Если же $\dim U = 2$, то рассмотрим $v \in U^\perp$ и $u \in U$.
			
			$\angles{a(v), u} = \angles{a(v), a(a^{-1}u)} = \angles{v, a^{-1}(u)} = 0$ (т.к. $a^{-1}(u) \in U$).			
				
			\item
			Пусть $au = \lambda u;\,\,av = \mu v;\,\, \lambda \neq \mu$.
			
			$\angles{u, v} = \angles{a(u), a(v)} =\angles{\lambda u, \mu v} = \overline{\lambda}\mu\angles{u, v}$
			
			Из полученного равенства следует, что либо $\angles{u, v} = 0$, либо $\overline{\lambda} \mu = 1$.
			
			Но при этом мы знаем, что $\lambda$ лежит на единичной окружности, т.е. $\overline{\lambda}\lambda = 1$, а тогда т.к. \\ $\mu \neq \lambda$, то второй случай отпадает. Значит, действительно, любые два вектора соответствующие различным собственным числам ортогональны. 
		\end{enumerate}
	\end{proof}
\end{theorem}