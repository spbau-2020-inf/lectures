\Section{Aд (или тензоры)}{А. Ермилов, А. Киракосян}

\Subsection{Полилинейная алгебра}

В данном параграфе $V_1, \, \ldots, \, V_n, \, U$~--- векторные пространства над полем $K$.

\begin{definition}
	\slashns
	
	Линейное пространство всех полилинейных функций $f : V_1 \times \ldots \times V_n \to U$ будем называть полилинейным пространством.
	
	Обозначается $\Hom(V_1, \ldots, V_n, U)$.
\end{definition}

\begin{denotation}
	\slashns
	
	Пусть $v_1 \in V_1, \ldots, v_n \in V_n$. 
	
	Функцию $f : V_1 \times \ldots \times V_n \to K$, такую, что $f$ принимает значение $1$ в точке $v_1, \ldots, v_n$ и значение $0$ во всех остальных точках, обозначим $(v_1, \ldots, v_n)$.
\end{denotation}

\begin{remark}
	\slashns
	
	Любая функция, принимающая ненулевое значение в конечном числе точек является конечной суммой функций $(v_1, \ldots, v_n)$ с коэффициентами из поля $K$.
\end{remark}	

\begin{definition}
	\slashns
	
	Введем множество $M$, как множество всех финитных функций (функций, описанных в замечании выше)
	
	Тогда $M := \{\sum\limits\limits_{\substack{\text{конечная} \\ \text{сумма}}} a_{v_1, \ldots, v_n} (v_1, \ldots, v_n) : a_{v_1, \ldots, v_n} \in K\}$.

\end{definition}

\begin{remark}
	\slashns

	Множество $M$ с покоординатным сложением и умножением на скаляр является векторным пространством над полем $K$. 
	
	Базисом будет являться множество всех функций $(v_1, \ldots, v_n)$.
		
\end{remark}

\begin{definition}
	\slashns
	
	Рассмотрим подпространство $M_0 \subseteq M$. 
	
	$M_0 = \angles{(v_1, \dots, v_k' + v_k'', \ldots, v_n) - (v_1, \ldots, v_k', \ldots, v_n) - (v_1, \ldots, v_k'', \ldots, v_n), $
		
	\vspace{-1pt}\hspace{100pt}$(v_1, \ldots, \alpha v_k, \ldots, v_n) - \alpha(v_1, \ldots, v_k, \ldots, v_n) :  \forall k \in [1..n], \; v_i \in V_i, \; v_k, v_k', v_k'' \in V_k}$
	
\end{definition}

\begin{definition}
	\slashns
		
	Определим тензорное произведение пространств $V_1, V_2, \ldots, V_n$ как $M / M_0$.

	Обозначается $V_1 \otimes \ldots \otimes V_n$.	

\end{definition}
	
\begin{remark}	
	\slashns
	
	Важно заметить, что при определении тензорного произведения мы нигде не опирались на базисы изначальных пространств.
\end{remark}	
	
\begin{definition}
	\slashns
		
	Класс эквивалентности функции $(v_1, \ldots, v_n)$ называется разложимым тензором. 
	
	Обозначается $v_1 \otimes \ldots \otimes v_n$.

	$v_1 \otimes \ldots \otimes v_n = (v_1, \ldots, v_n) + M_0$
\end{definition}
	
\begin{remark}	
	\slashns
	
	При этом из-за выбора множества $M_0$ мы сразу получаем различные равенства на разложимые тензоры. К примеру, $v_1 \otimes \ldots \otimes \alpha v_i \otimes \ldots \otimes v_n = \alpha(v_1 \otimes \ldots \otimes v_n)$ (прямо внутри выбора $M_0$ прописано, что классы эквивалентности $\alpha(v_1, \ldots, v_n)$ и $(v_1, \ldots, \alpha v_i, \ldots, v_n)$ совпадают).
\end{remark}	

\begin{lemma}
	\slashns
	
	Разложимые тензоры порождают $V_1 \otimes \ldots \otimes V_n$.
\end{lemma}

\begin{proof}
	\slashns
	
	Действительно, функции $(v_1, \ldots, v_n)$ порождают все пространство $M$, а тогда их классы эквивалентности, т.е. разложимые тензоры, порождают все пространство $V_1 \otimes \ldots \otimes V_n$.
\end{proof}	

\begin{consequence}
	\slashns
	
	Если одно из векторных пространств $V_i$ тривиально, то и всё тензорное произведение тривиально.
\end{consequence}

\begin{proof}
	\slashns
	
	Из доказанной леммы нам достаточно показать, что все разложимые тензоры нулевые.
	
	Действительно, $v_1 \otimes \dots \otimes 0\otimes \dots \otimes v_n = 0\cdot v_1 \otimes \dots \otimes 0\otimes \dots \otimes v_n = 0$
	
	Иначе говоря, класс эквивалентности $(v_1, \ldots, 0, \ldots, v_n)$ совпадает с классом эквивалентности нуля, т.к. $(v_1, \ldots, 0, \ldots, v_n) - 0\cdot(v_1, \ldots, 0, \ldots, v_n) \in M_0$, а $0\cdot(v_1, \ldots, 0, \ldots, v_n) = 0$.
\end{proof}
	
\begin{denotation}	
	\slashns
	
	Введем отображение $t : V_1 \times \ldots \times V_n \to V_1 \otimes \ldots \otimes V_n$:
	
	$(v_1, \ldots, v_n) \mapsto v_1 \otimes \ldots \otimes v_n$
\end{denotation}

\begin{theorem}
	\slashns
	
	1) $t$~--- полилинейно
	
	2) $t$~--- универсально, т.е. $\forall s \in \Hom(V_1, \ldots, V_n, U) \;\; \exists \, !f \in \Hom(V_1 \otimes \ldots \otimes V_n, U)$, т.ч. $s = f \circ t$
	
	\begin{center}
		\begin{tikzpicture}
		\node (1) at (0, 0) {$V_1 \times \ldots \times V_n$};
		\node (2) at (5, 0) {$U$};
		\node (3) at (2.5, -1.5) {$V_1 \otimes \ldots \otimes V_n$};
		\draw (1) edge[->] (3);
		\draw (1) edge[->] (2);
		\draw (3) edge[dashed, ->] (2);
		\node[above] at (3, 0) {\small $s$\text{ --- полилин.}};
		\node[right] at (1.35, -0.6) {\small $t$};
		\node[right] at (3, -0.6) {\small $\exists !f$};
		\end{tikzpicture}
	\end{center}
	
	\begin{proof}
		\slashns
		
		Проверим полилинейность $t$.
		
		$\alpha t((v_1, \ldots, v_n)) = \alpha(v_1\otimes \ldots \otimes v_n) = (v_1\otimes \ldots \otimes \alpha v_i \otimes \ldots v_n) = t((v_1, \ldots, \alpha v_i, \ldots v_n))$

		Аналогично проверяется аддитивность.		
	
		Перейдем к универсальности.
		
		Единственность:
		
		Пусть $f_1 \circ t = s = f_2 \circ t$
		 
		Тогда $f_1\circ t(v_1, \ldots, v_n) = f_2\circ t(v_1, \ldots, v_n) \implies f_1(v_1 \otimes \ldots \otimes v_n) = f_2(v_1 \otimes \ldots \otimes v_n)$, а значит, раз $v_1 \otimes \ldots \otimes v_n$ образуют все пространство, то $f_1 = f_2$.
		
		Существование:
		
		Определим линейное отображение $g : M \to U$ на функциях $(v_1, \ldots, v_n)$.
		
		$g(v_1, \ldots, v_n) = s(v_1, \ldots, v_n)$.
		
		Тогда по линейности, $g$ единственным образом продолжается на все суммы.
		
		$g\Big(\sum a_{v_1, \ldots, v_n} (v_1, \ldots, v_n)\Big) = \sum a_{v_1, \ldots, v_n} s(v_1, \ldots, v_n)$
		
		Тогда можем определить $f : M / M_0 \to U$, а именно $X \mapsto g(x)$, где $x \in X$~--- элемент из соответствующего класса эквивалентности. 
		
		Для обоснования корректности нужно показать, что $g(x) = g(y)$, если $x$ и $y$ лежат в одном классе эквивалентности. 
		
		Т.е. нужно проверить, что $g(x - y) = 0$. Мы знаем, что $x - y \in M_0$, а значит достаточно показать, что $M_0 \subseteq \Ker g$. 
		
		Это очевидно т.к. $g$ обнуляется на каждом образующем векторе $M_0$ по линейности.
		
		Таким образом, получили искомое отображение $f$.
	\end{proof}
\end{theorem}

\begin{remark}\label{remark:universal-hom}
	\slashns

	Благодаря этой теореме мы теперь можем определять гомоморфизм лишь на разложимых тензорах.

	Т.е. если мы хотим задать гомоморфизм $\sigma : V_1\otimes\ldots \otimes V_n \to U$, то мы можем задать лишь полилинейное отображение $V_1\times \ldots \times V_n \to U$ (эквивалентно определению $\sigma$ на разложимых векторах), по которому из теоремы единственным образом определяется $\sigma$.
\end{remark}

\begin{consequence}
	\slashns
	
	\begin{enumerate}
		\item
		Существует \textbf{канонический} изоморфизм векторных пространств:
		
		$\Hom(V_1, \ldots, V_n, U) \cong \Hom(V_1 \otimes \ldots \otimes V_n, U)$
		
		\item 
		$\Hom(V_1, \ldots, V_n, K) \cong (V_1 \otimes \ldots \otimes V_n)^*$
		
		\item 
		$\dim(V_1 \otimes \ldots \otimes V_n) = \dim V_1 \cdot \ldots \cdot \dim V_n$
		
		\item 
		Пусть $e_1^{(i)}, e_2^{(i)}, \ldots, e_{k_i}^{(i)}$~--- базис $V_i$.
		
		Тогда $e_{j_1}^{(1)} \otimes e_{j_2}^{(2)} \otimes \ldots \otimes e_{j_n}^{(n)}$~--- базис $V_1 \otimes \ldots \otimes V_n$, $1 \le j_l \le k_l$
	\end{enumerate}
	
	\begin{proof}
		\slashns
		
		\begin{enumerate}
			\item 
			
			Рассмотрим отображение $\varphi$, сопоставляющее элементу $s \in \Hom(V_1, \ldots, V_n, U)$ элемент $f \in \Hom(V_1 \otimes\ldots\otimes V_n, U)$, такой, что $s = f \circ t$. Проверим, что $\varphi$ изоморфизмом. 
						
			Гомоморфизм: 
			
			$\varphi(s_1 + s_2) = \varphi(f_1 \circ t + f_2 \circ t) = \varphi((f_1 + f_2) \circ t) = f_1 + f_2 = \varphi(s_1) + \varphi(s_2)$.
			
			$\varphi(\alpha s) = \varphi(\alpha (f \circ t)) = \varphi(\alpha f \circ t) = \alpha f = \alpha \varphi (s)$
			
			Сюръективность: Пусть $f \in \Hom(V_1 \otimes\ldots\otimes V_n, U)$. Тогда $\varphi(f\circ t) = f$. Осталось заметить, что $f \circ t \in \Hom(V_1, \ldots, V_n, U)$, т.к. $f$~--- гомоморфизм, а $t$~--- полилинейна.
			
			Инъективность: $f = \varphi(s) = 0 \implies s = f \circ t \implies s = 0$
			
			\item
			Следует из первого пункта, если подставить $U = K$ 
			
			($\Hom(V, K) = V^*$, если $V$~--- векторное пространство над $K$).
			
			\item 
			$\dim(V_1 \otimes \ldots \otimes V_n) = \dim(V_1 \otimes \ldots \otimes V_n)^* = \dim \Big(\Hom(V_1, \ldots, V_n, K)\Big) = \dim V_1 \cdot \ldots \cdot \dim V_n$
			
			\item 
			Уже показали, что размерность $V_1 \otimes \ldots \otimes V_n$ совпадает с количеством элементов в предъявленном базисе, значит, осталось показать, что он является системой образующих.
			
			Покажем, что каждый разложимый вектор $V_1 \otimes \ldots \otimes V_n$ представим в выбранном базисе. Из этого последует, что любой элемент $V_1\otimes\ldots\otimes V_n$ представим, т.к. разложимые вектора являются системой образующих. 
			
			Рассмотрим разложимый вектор $v_1\otimes\ldots\otimes v_n$. Разложим каждое из $v_i$ по базису пространства $V_i$, а затем по полилинейности получим представление в описанном базисе (раскроем скобки), что и требовалось.
		\end{enumerate}
	\end{proof}
\end{consequence}

\vspace{-30pt}
\begin{example}(Комплексификация)
	\slashns
	
	Начнем с известного нам примера комплексификации.
	
	Пусть $V$~--- векторное пространство над $\R$ с базисом $e_1, \, \ldots, \, e_n$.

	Рассмотрим пространство $\CC\otimes V$ как векторное пространство над $\CC$. Для этого нужно определить домножение на комплексный аргумент. Определим его как $\alpha(z \otimes v) = \alpha z\otimes v$.

	Базисом $\CC\otimes V$ будет множество $1 \otimes e_j$, т.к. остальные базисные вектора (как пространства над $\R$) выражаются через домножение на $i$.
		
	Покажем, что есть изоморфизм векторных пространств $\CC\otimes V$ и $V^\CC$.
	
	Зададим отображение $1 \otimes e_j \mapsto (e_j, 0)$. Оно переводит базис в базис.
	
\end{example}	
	
\begin{remark}	
	\slashns
	
	При определении умножения над полем $\CC$ мы определяем умножение лишь на разложимых тензорах.
	
	Мы имеем на это право согласно теореме про универсальность отображения $V_1\times \ldots \times V_n \to V_1 \otimes \ldots \otimes V_n$ (см. \hyperref[remark:universal-hom]{\text{замечание}}). 
\end{remark}	
	
\begin{example}(Расширение скаляров)
	\slashns
	
	
	Попробуем теперь обобщить пример выше.
	
	Пусть $L, \, K$~--- поля, $K \subseteq L$ (к примеру, ситуация с $\R$ и $\CC$).
	
	$V$~--- векторное пространство над $K$.
		
	Заметим, что $L$ и $V$~--- векторные пространства над полем $K$, а значит мы можем рассмотреть $L \otimes V$ как векторное пространство над $K$.
	
	Теперь мы хотим ввести структуру линейного пространства $L\otimes V$ над полем $L$. Нужно определить домножние на скаляр разложимых векторов $L\otimes V$.
	
	Определим как $\alpha(\beta \otimes v) = (\alpha\beta) \otimes v$, где $\alpha$, $\beta$~--- элементы поля $L$, $v$~--- вектор $V$.
	
	Получили корректную структуру (далее все определяется по линейности).
\end{example}

\begin{remark}
	\slashns
	
	Аналогичное комплексификации.
\end{remark}	

\begin{lemma}
	\slashns
	
	1) Существует \textbf{канонический} изоморфизм $V_1 \otimes V_2 \cong V_2 \otimes V_1$.

	2) Существует \textbf{канонический} изоморфизм $(V_1 \otimes V_2) \otimes V_3 \cong V_1 \otimes (V_2 \otimes V_3)$.
	
	3) Существует \textbf{канонический} изоморфизм $V_1^* \otimes \ldots \otimes V_n^* \cong \left(V_1 \otimes \ldots \otimes V_n\right)^*$.
	
	4) Существует \textbf{канонический} изоморфизм $U^* \otimes V \cong Hom(U, V)$.
\end{lemma}

\begin{proof}
	\slashns

	Нам всегда достаточно определять отображение лишь на разложимых векторах.

	1) $ v_1\otimes v_2 \mapsto v_2 \otimes v_1$
	
		Такое отображение, очевидно является изоморфизмом, т.к. для любого выбранного базиса пространства $V_1\otimes V_2$ он перейдет в базис $V_2 \otimes V_1$.
	
	2) Покажем, что существует канонический изоморфизм $V_1 \otimes V_2 \otimes V_3 \cong V_1 \otimes (V_2 \otimes V_3)$.
	
	$(v_1\otimes v_2\otimes v_3) \mapsto v_1\otimes(v_2 \otimes v_3)$~--- является требуемым изоморфизмом, т.к. любой базис перейдет в базис.

	3) Мы знаем, что существует канонический изоморфизм $\Hom(V_1, \ldots, V_n, K) \cong (V_1 \otimes \ldots \otimes V_n)^*$. 

	Осталось найти канонический изоморфизм $V_1^* \otimes \ldots \otimes V_n^* \cong \Hom(V_1, \ldots, V_n, K)$.
	
	$f_1\otimes \ldots \otimes f_n \mapsto ((v_1,\ldots, v_n) \mapsto f_1(v_1)\ldots f_n(v_n))$.
	
	Проверим инъективность. Пусть результирующее отображение нулевое. Тогда одно из $f_i = 0$, а тогда и $f_1\otimes \ldots \otimes f_n = 0$, что и требовалось. 
	
	Осталось заметить, что размерности пространств совпадают.
	
	4) Рассмотрим отображение $f \otimes v \mapsto (u \mapsto f(u)v)$.
	
	Покажем, что оно изоморфизм.

	$e_1, \ldots, e_n$~--- базис $U$
		
	$e^1, \ldots, e^n$~--- двойственный базис $U^*$
		
	$f_1, \ldots, f_m$~--- базис $V$
	
	Нужно проверить, что базис переходит в базис:
		
	$e^i \otimes f_j \mapsto \left(u \mapsto u^if_j \right)$
		
	Рассмотрим, как действует отображение $u \mapsto u^if_j$ на базисные вектора: $e_k \mapsto \delta_k^i f_j$, где $\delta_k^i$~--- символ Кронекера. Это и есть базисный элемент $Hom(U, V)$.

\end{proof}