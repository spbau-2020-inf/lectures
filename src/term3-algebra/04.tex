\Section{Евклидовы и эрмитовы пространства}{А.Ермилов, А.Киракосян}
\Subsection{Введение}

\begin{definition}
	\slashns
	
	Конечномерное векторное пространство $V$ над $\R$ с симметричной билинейной формой $\alpha$ называется евклидовым, если ассоциированная с ней квадратичная форма положительно определена. 
		
	Форму $\alpha$ в таком случае будем называть скалярным произведением и обозначать $\angles{x, y}$. 
\end{definition}

\begin{definition}
	\slashns
	
	$V$~--- векторное пространство над $\CC$.
	
	Полуторалинейная форма $\alpha$ называется эрмитовой, если $\alpha(x, y) = \overline{\alpha(y, x)}$
\end{definition}

\begin{properties}
	\slashns
	
	\begin{enumerate}
		\item $\alpha$~--- эрмитова $\iff \overline{A}^T = A$ ($A$~--- матрица $\alpha$, $\overline{A}$~--- матрица из сопряжённых элементов.) 
	
		\item $\alpha(x, x) \in \R$.
	\end{enumerate}
	
	\begin{proof}
		\slashns
	
		\begin{enumerate}
			\item $\overline{\alpha(e_i, e_j)} = \alpha(e_j, e_i)$
			
			\item $\alpha(x, x) = \overline{\alpha(x, x)} \implies \alpha(x, x) \in \R$
		\end{enumerate}
	\end{proof}
\end{properties}

\begin{definition}
	\slashns
	
	Конечномерное векторное пространство $V$ над $\CC$ с эрмитовой формой $\alpha$ называется эрмитовым, если для любого ненулевого вектора $\alpha(x, x) > 0$.
	 	
	Форма же $\alpha$ называется эрмитовым скалярным произведением. 
\end{definition}

\begin{definition}
	\slashns
	
	$f : U \to V$ называется гомоморфизмом евклидовых (эрмитовых) пространств, если \\ $f$~--- гомоморфизм векторных пространств, такой что $\langle f(x), \, f(y) \rangle = \langle x, \, y \rangle \;\; \forall \, x, \, y \in U$.

	Т.е., если $f$ сохраняет скалярное произведение.
\end{definition}

\begin{example}
	\slashns
	
	1) $\R^2$ со стандартным скалярным произведением.
	
	2) $C([a, b])$ со скалярным произведением $\int[a][b] f(x)g(x) \, dx$.
\end{example}

\begin{definition}
	\slashns
	
	$\langle \cdot, \, \cdot \rangle$~--- скалярное произведение (евклидово, эрмитово), $v_1, \, \ldots, \, v_k \in V$.
	
	$G(v_1, \, \ldots, \, v_k) = \begin{pmatrix}
	\langle v_1, v_1 \rangle & \cdots & \langle v_1, v_k \rangle\\
	\vdots & \ddots & \vdots\\
	\langle v_k, v_1 \rangle & \cdots & \langle v_k, v_k \rangle\\
	\end{pmatrix}$~--- матрица Грама системы векторов $v_1, \, \ldots, \, v_k$
\end{definition}

\begin{remark}
	\slashns 
	
	Если вектора $v_1, \ldots, v_n$ образуют базис, то матрица Грама совпадает с матрицей билинейной формы скалярного произведения в базисе $v_1, \ldots, v_n$. 	
\end{remark}

\Subsection{Евклидовы пространства}

\begin{lemma}
	\slashns
	
	$V$~--- евклидово пространство.
	
	$v_1, \, \ldots, \, v_k \in V$
	
	Тогда $\det G(v_1, \, \ldots, \, v_k) \ge 0$, причём равенство достигается тогда и только тогда, когда $v_1, \, \ldots, \, v_k$~--- линейно зависимы.
	
	\begin{proof}
		\slashns
		
		1) $v_1, \, \ldots, \, v_k$~--- линейно независимы, а значит являются базисом пространства $\angles{v_1, \ldots, v_k}$.
		
		Тогда $G$ является матрицей квадратичной формой, которая в этом базисе положительно определена (т.к. скалярное произведение).
		
		Следовательно, по критерию Сильвестра $\det G > 0$.
		
		2) $v_1, \, \ldots, \, v_k$~--- линейно зависимы $\implies \exists \lambda_j :\; \sum\limits_{j=1}^{k} \lambda_j v_j = 0$
		
		Докажем, что столбцы матрицы Грама, домноженные на $\lambda_j$ дают в сумме $0$.
		
		Рассмотрим строку $i$.
			
		$\sum\limits_{j=1}^{k} \lambda_j \angles{v_i, v_j} = \angles{v_i, \sum\limits_{j=1}^k\lambda_j v_j} = \angles{v_i, 0} = 0$.
		
		А значит, столбцы линейно зависимы, а тогда определитель $0$.
	\end{proof}
\end{lemma}

\begin{definition}
	\slashns
	
	Длина вектора $\norm{v} = \sqrt{\angles{v, v}}$.
\end{definition}

\begin{lemma}[\textbf{Неравенство Коши-Буняковского-Шварца}]
	\slashnss
	
	$\angles{x, y}^2 \le \norm{x}^2\cdot\norm{y}^2$
\end{lemma}

\begin{proof}
	\slashnss
	
	$$G(x, y) = \begin{pmatrix}
	\angles{x, x} & \angles{x, y}) \\
	\angles{y, x} & \angles{y, y} \\
	\end{pmatrix}$$
	
	Мы знаем, что $\det G \ge 0$, а значит $\angles{x, x} \cdot \angles{y, y} \ge \angles{x, y}\cdot\angles{y, x} \Rightarrow \norm{x}^2\cdot\norm{y}^2 \ge \angles{x, y}^2$
	
\end{proof}

\begin{theorem}
	\slashns
	
	Длина является нормой.

	\begin{proof}
		\slashns
		
		\begin{enumerate}
		\item $\sqrt{\angles{v, v}} \ge 0\;\; \forall v$, при этом по определению скалярного произведения $\angles{v, v} > 0 \;\; \forall v \neq 0$.
		\item 
		$\norm{\lambda v} = \abs{\lambda} \cdot \norm{v}$
		\item $\norm{x + y} \le \norm{x} + \norm{y}$
		
		Возводим в квадрат.
		
		$\angles{x + y, x + y} \le \norm{x}^2 + \norm{y}^2 + 2\norm{x}\cdot\norm{y}$ 
		
		$\norm{x}^2 + \norm{y}^2 + 2\angles{x, y} \le \norm{x}^2 + \norm{y}^2 + 2\norm{x}\cdot\norm{y}$ 
		
		$\angles{x, y} \le \norm{x}\cdot\norm{y}$
		
		А это неравенство Коши-Буняковского-Шварца.
		\end{enumerate}
	\end{proof}
\end{theorem}

\begin{consequence}
	\slashns
	
	1) $\rho(x, y) = \norm{x - y}$
	
	2) $-1 \le \frac{\langle x, \, y \rangle}{\norm{x} \norm{y}} \le 1 \;\; \forall \, x, \, y \in V \setminus \{0\}$
	
	Тогда можем определить угол:
	
	$\cos \phi = \frac{\langle x, \, y \rangle}{\norm{x} \norm{y}} \implies 0 \le \phi \le \pi$
\end{consequence}

\begin{definition}
	\slashns
	
	Базис $e_1, \, \ldots, \, e_n$ называется ортогональным, если $\langle e_i, \, e_j \rangle = 0$ для всех $i \ne j$
\end{definition}

\begin{statement}
	\slashns
	
	Ненулевые, попарно ортогональные вектора линейно независимы.
	
	\begin{proof}
		\slashns
		
		От противного. Пусть $\alpha_1 e_1 + \ldots + \alpha_k e_k = 0$
		
		При этом $\forall i:\; 0 = \angles{0, e_i} = \langle \alpha_1 e_1 + \ldots + \alpha_k e_k, \, e_i \rangle = \sum\limits_{j = 0}^k\alpha_j\angles{e_j, e_i} = \alpha_i\angles{e_i, e_i}$
		
		$\alpha_i \langle e_i, \, e_i \rangle = 0, \; \langle e_i, \, e_i \rangle > 0 \implies \alpha_i = 0$
	\end{proof}
\end{statement}

\begin{algorithm}[\textbf{ортогонализации}]
	\slashns
	
	Рассмотрим набор векторов $e_1, \, \ldots, \, e_n \in V, \; e_1 \ne 0$.
	
	Построим $f_1, \, \ldots, \, f_n$ по таким правилам:

	1) $f_1 = e_1$.
	
	2)$ f_{k+1} = e_{k+1} - \sum\limits_{i=1}^{k} \frac{\langle e_{k+1}, \, f_i \rangle}{\langle f_i, \, f_i \rangle} f_i$
\end{algorithm}

\begin{theorem}
	\slashns
	
	$e_1, \ldots, e_n$~--- набор векторов, $f_1, \ldots, f_n$~--- построенный по ним набор.
	
	$i, \, j, \, k \in \{1, \, \ldots, \, n\}, \;\; i \ne j$
	
	Тогда:
	
	1) $\langle f_i, \, f_j \rangle = 0$
	
	2) $\langle e_1, \, \ldots, \, e_n \rangle = \langle f_1, \, \ldots, \, f_n \rangle$
	
	3) Если $e_1, \, \ldots, \, e_n$~--- линейно независимы, то $f_1, \, \ldots, \, f_n$~--- линейно независимы. В частности, $f_i \ne 0$.
	
	4) Если $e_k \in \langle e_1, \, \ldots, \, e_{k-1} \rangle$, то $f_k = 0$
	
	5) Если $e_1, \, \ldots, \, e_n$~--- система образующих, то ненулевые $f_1, \, \ldots, \, f_n$ образуют базис.
	
	6) Если $e_1, \, \ldots, \, e_n$~--- базис $V$, то $f_1, \, \ldots, \, f_n$~--- ортогональный базис.
	
	\begin{proof}
		\slashns
		
		Модифицируем алгоритм ортогонализации Грама-Шмидта. А именно, при построении очередного $f_i$ будем рассматривать только такие $f_j$ для $j < i$, что $f_j \ne 0$. И покажем, что $f_i$ будет ортогонален всем предыдущим.
		
		Доказательство этого, аналогично доказательству корректности ортогонализации Грама-Шмидта, будет осуществляться по индукции.
		
		Пусть мы на очередном шаге имеем $f_i = e_i - \sum\limits_{j < i} \frac{\angles{e_i, f_j}}{\angles{f_j, f_j}} f_j$, причём векторы $f_1, \ldots, f_{i-1}$~--- ортогональны.
		
		Тогда получаем, что $\angles{f_i, f_j} = \angles{e_i, f_j} - \frac{\angles{e_i, f_j}}{\angles{f_j, f_j}} \cdot \angles{f_j, f_j} = 0$.
		
		А значит, очередной $f_i$ или равен нулю (если $e_i \in \angles{e_1, \ldots, e_{i-1}}$), или отличен от нуля и, соответственно, ортогонален всем предыдущим векторам (если $e_i \in \angles{e_1, \ldots, e_{i-1}}^{\perp}$).
		
		Отсюда очевидным образом следуют все 6 пунктов данной теоремы.
	\end{proof}
\end{theorem}

\Subsection{Ортогональные матрицы}

\begin{definition}
	\slashns
	
	Заметим, что в евклидовом пространстве очень просто перейти от ортогонального базиса к ортонормированному.
	
	Пусть $e_1, \, \ldots, \, e_n$~--- ортонормированный базис.
	
	Тогда $f_1, \, \ldots, \, f_n$ является ортонормированным базисом тогда и только тогда, когда $C^TEC = C^TC = E$.
	
	Матрицы $C$, обладающие таким свойством, называются ортогональными.
\end{definition}

\begin{remark}
	\slashns
	
	Если $C$~--- ортогональная, то $\det C = \pm 1$.
	
	\begin{proof}
		\slashns
		
		$\det E = \det (C^TC) = \det^2 C \iff \det C = \pm 1$
	\end{proof}		
\end{remark}