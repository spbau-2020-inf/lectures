\Subsection{Сопряжённые операторы}

\begin{definition}
	\slashns
	
	Линейные операторы $a$ и $a^*$ над евклидовым или эрмитовым пространство $V$ называются сопряжёнными, если
	
	$\forall v, w \in V: \angles{a^*(v), w} = \angles{v, a(w)}$
\end{definition}
 
\begin{property}  
	 \slashns
	 
	 Если $A$~--- матрица $a$, то $A^* = G^{-1}\overline{A^T}G$, где $A^*$~--- матрица сопряженного $a$ оператора $a^*$ в том же самом базисе. 
	 
	 \begin{proof}
		\slashns
		
		Из определения: $\overline{A^*}^TG = G A \implies \overline{A^*} = (GAG^{-1})^T \implies A^* = G^{-1}\overline{A^T}G$
		
		В последним переходе пользуемся свойствами матрицы Грама.
	 \end{proof}
\end{property} 
 
\begin{remark}
	\slashnss
		
	В частности, получили, что сопряженный оператор всегда существует и единственный.
\end{remark}

\begin{fact}
	\slashns
	
	Рассмотрим отображение $r : V \to V^*$, такой что $v \mapsto \angles{v, \cdot}$.
	
	Тогда $r$~--- изоморфизм.
	
	\begin{proof}
		\slashns 
		
		Покажем, что $r$~--- инъекция. Т.е, что $\Ker r = \{0\}$.		
	
		$\Ker r = \{v \in V : \forall u \in V \;\; \angles{v, u} = 0 \}$, в частности, $\angles{v, v} = 0 \implies v = 0$.
	\end{proof}	
\end{fact}

\begin{remark}
	\slashnss
	
	\begin{enumerate}
		\item
		Введем скалярное произведение в двойственном пространстве:
		
		$\angles{\angles{v, \cdot}, \angles{u, \cdot}} = \angles{v, u}$
		
		\item
		Пусть $e_1, \ldots, e_n$~--- ортонормированный базис $V$.
		
		Тогда $r(e_1), \ldots, r(e_n)$~--- двойственный базис в пространстве $V^*$, а точнее $r(e_j)(e_i) = \delta_{i, j}$
		
		\item 
		Если $V$~--- эрмитово, а $z \in \CC$, то $r(zv) = \overline{z}r(v)$.
		 
		Отображение, обладающие таким свойством называется антилинейным.
		
		\item 		
		Рассмотрим оператор $a$. 
		
		$a^*: V \to V$, $a': V^* \to V^*$ (сопряженный оператор из прошлого года)
		
		Заметим, что $r(a^*(x)) = \angles{a^*(x), \cdot} = \angles{x, a(\cdot)} = a'(\angles{x, \cdot}) = a'(r(x)) \;\; \forall x \in V$
		
		А значит, $a^* = r^{-1}a'r$.
	
		% $a'(\angles{x, \cdot})(y) = \angles{x, \cdot}(a(y)) = \angles{x, a(y)}$
	\end{enumerate}
\end{remark}

\Subsection{Самосопряженные операторы}

\begin{definition}
	\slashnss
	
	Пусть $V$~--- евклидово или эрмитово, $a \in \End(V)$.
	
	Оператор $a$ называется самосопряженным, если $a = a^*$.	
\end{definition}

\begin{remark}
	\slashnss
	
	В терминах матриц получаем равенство $A = G^{-1}\overline{A}^TG$, а в ортонормированном базисе получаем $A = \overline{A}^T$
\end{remark}

\begin{theorem}
	\slashnss
	
	Пусть $V$~--- евклидово или эрмитово, а $a \in \End(V)$. Тогда:
	
	\begin{enumerate}
		\item
		$a$~--- самосопряжен
		$\iff \begin{cases}
			\Spec(a) \subset \R \\
			\exists e_1, \ldots, e_n$~--- ортонормированный базис, т.ч. $a_e$~--- диагональная
		$\end{cases}$
		
		\item
		Собственные вектора, соответствующие различным собственным числам, ортогональны.
	\end{enumerate}
	
	\begin{proof}
		\slashnss
		
		\begin{enumerate}
			\item
			
			``$\impliedby$''
			
			Рассмотрим базис, в котором $A$ диагональна.
			
			Тогда $\overline{A} = A$, и $A^T = A \implies A = \overline{A}^T$, тогда, т.к. базис ортонормированный, то $A$~--- самосопряжен.
			
			``$\implies$''
			
			Покажем, что все собственные числа вещественны.
				
			$\forall \lambda \in \Spec(V): \; av = \lambda v$
			
			Пусть $V$~--- эрмитово. Тогда:
			
			$\lambda\angles{v, v} = \angles{v, \lambda v} = \angles{v, av} = \angles{av, v} = \angles{\lambda v, v} = \overline{\lambda}(v, v)$
			
			А так как $\angles{v, v} \ne 0$, то $\lambda = \overline{\lambda} \implies \lambda \in \R$
			
			Если же $V$~--- евклидово, то рассмотрим $V^\CC$ со скалярным произведением: 
			
			$\angles{u_1 + iv_1, u_2 + iv_2} := \angles{u_1, u_2} + i\angles{u_1, v_2} - i\angles{v_1, u_2} + \angles{v_1, v_2}$ 
		
			Таким образом, получили эрмитово пространство $V^\CC$. Покажем, что $a^\CC$~--- самосопряженный.
			
			$\angles{a^\CC(u_1 + iv_1), u_2 + iv_1} = \angles{a(u_1) + ia(v_1), u_2 + iv_2} = \angles{a(u_1), u_2} + i\angles{a(u_1), v_2} - i\angles{a(v_1), u_2} + \angles{a(v_1), v_2} = \angles{u_1, a(u_2)} + i\angles{u_1, a(v_2)} - i\angles{v_1, a(u_2)} + \angles{v_1, a(v_2)} = \angles{u_1 + iv_1, a(u_2) + ia(v_2)} = \angles{u_1 + iv_1, a^\CC(u_2 + iv_1)}$
						
			Но для самосопряжённых операторов над эрмитовым пространством мы знаем, что их собственные числа~--- вещественные.
			
			А поскольку $\chi_a^\CC = \chi_a$, то получаем, что собственные числа оператора $a$ также будут вещественными.
			
			Теперь предъявим ортонормированный базис, в котором матрица диагональна.

			Индукция по размерности $n$.

			База при $n = 1$~--- подойдет любой базис. 
			
			Переход:

			Рассмотрим $\lambda \in \Spec(a)$, и $v$~--- соответствующий собственный вектор.
			
			Тогда $V = \angles{v} \oplus \angles{v}^{\perp}$ (мы уже доказывали, что все такие суммы прямые).
			
			Нужно показать, что $\angles{v}$ и $\angles{v}^{\perp}$~--- инвариантны относительно $a$.
			
			Инвариантность $\angles{v}$ очевидна. Покажем инвариантность $\angles{v}^{\perp}$.
			
			Рассмотрим $v \in \angles{v}^\perp$. Хотим проверить, что $\angles{u, av} = 0 \; \forall u \in \angles{v}$
			
			$\angles{u, av} = \angles{au, v} = \angles{\lambda u, v} = \lambda\angles{u, v} = 0$
			
			\item
			$au = \lambda u, \;\; av = \mu v$
			
			$\overline{\lambda}\angles{u, v} = \angles{\lambda u, v} = \angles{au, v} = \angles{u, av} = \angles{u, \mu v} = \mu\angles{u, v} \implies \angles{u, v} = 0$, т.к. $\lambda \ne \mu$
		\end{enumerate}
	\end{proof}
	
	\begin{consequence}
		\slashns
		
		Пусть $A \in \R^{n \times n}$ или $A \in \CC^{n \times n}$, при этом $\overline{A}^T = A$ (т.е. это матрица самосопряженного оператора в ортонормированном базисе). Тогда:
		
		\begin{enumerate}
			\item
			Все корни $\chi_A$~--- вещественные
			
			\item
			Существует ортогональная (или унитарная) матрица $C$, такая что $C^{-1}AC = \overline{C}^TAC$~--- диагональная.
		\end{enumerate} 
		
		\begin{proof}
			\slashns
			
			Рассмотрим пространство в котором задана наша матрица ($\R^{n \times n}$ или $\CC^{n \times n}$).
			
			Тогда наша матрица задает самосопряженный оператор, а значит все собственные числа вещественны, т.е. характеристический многочлен имеет $n$ вещественных корней.
			
			При этом, существует ортонормированный базис, в котором матрица будет диагональна, а значит $C$~--- соответствующая матрица перехода.
		\end{proof}
	\end{consequence}
\end{theorem}