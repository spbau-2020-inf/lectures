\subsection{Симметричные тензоры}

В параграфе $V$~--- конечномерное векторное пространство над полем $K$ нулевой характеристики.

\begin{denotation}
	\slashns
		
	Группа перестановок $S_q$ действует на $T^q_0(V)$, а именно:
	
	Если $\sigma \in S_q$, тогда отображение $f_{\sigma}$ действует как $f_{\sigma}(v_1\otimes \ldots \otimes v_q) = v_{\sigma(1)} \otimes \ldots \otimes v_{\sigma(q)}$.
	
\end{denotation}	

\begin{definition}
	\slashns 
	
	Пространство симметричных тензоров:
	
	$S^q(V) = \{T \in T_0^q(V) \, | \, f_\sigma T = T \;\; \forall \sigma \in S_q\}$, т.е. пространство тензоров, не меняющихся под действием всех элементов группы $S_q$.
\end{definition}	

\begin{remark}
	\slashns
	
	Это подпространство в $T_0^q(V)$.
\end{remark}	

\begin{definition}
	\slashns
	
	$S := \frac{1}{q!}\sum\limits_{\sigma \in S_q} f_{\sigma}$~--- оператор симметризации.
\end{definition}	

\begin{remark}
	\slashns

	Если $T \in T_0^q(V)$, то $S(T) \in S^q(V)$, т.к. определение симметрично относительно всех перестановок составляющих тензора.
\end{remark}

\begin{statement}
	\slashns
	
	$T_1 = f_\sigma T_2 \implies S(T_1) = S(T_2)$.
	
	\begin{proof}
		\slashns
		
		$S(T_1) = \frac{1}{q!}\sum\limits_{\tau \in S_q} f_{\tau} T_1 = \frac{1}{q!}\sum\limits_{\tau \in S_q} f_\tau f_\sigma T_2 = \frac{1}{q!} \sum\limits_{\tau \in S_q} f_{\tau\sigma}T_2 = S(T_2)$
	\end{proof}
\end{statement}	

\begin{statement}
	\slashns

	$S^2 = S$
	
	\begin{proof}
		\slashns 
		
		$S(S(T)) = \frac{1}{q!} \sum\limits_{\sigma \in S_q} f_\sigma S(T) = \frac{1}{q!} \sum\limits_{\sigma \in S_q} f_{\sigma}\left(\frac{1}{q!} \sum\limits_{\tau \in S_q} f_{\tau} T\right) = \\$
		
		$\hspace{225pt}= \frac{1}{q!} \sum\limits_{\sigma \in S_q} \left(\frac{1}{q!} \sum\limits_{\tau \in S_q} f_{\sigma \tau} T\right) = \frac{1}{q!} \sum\limits_{\sigma \in S_q} S(T) = S(T)$
	\end{proof}	
\end{statement}

\begin{statement}
	\slashns
	
	$\Im S = S^q(V)$
	
	\begin{proof}
		\slashns
		
		$\Im S \subset S^q(V)$ (из определения $S$).	
		
		Докажем, что для любого $T \in S^q(V): \;\; S(T) = T$.

		$S(T) = \frac{1}{q!}\sum\limits_{\sigma \in S_q} f_{\sigma}(T) = T$.
				
		Значит, любой элемент из $S^q(V)$ лежит в $\Im S$, что и требовалось.
	\end{proof}
\end{statement}

\begin{statement}
	\slashns
	
	Положим $e_1, \, \ldots, \, e_n$~--- базис $V$. Тогда 

	\vspace{-10pt}
	\begin{enumerate}
		\item 
		Обозначим $S(e_{i_1} \otimes \ldots \otimes e_{i_q})$ как $e_{i_1} \ldots e_{i_q}$.
		
		Тогда $\{ e_{i_1} \ldots e_{i_q} \}$~--- базис $S^q(V)$.
		
		\item
		$S^q(V) \cong \{f \in K[x_1, \ldots, x_n] \, | \,$ все мономы $f$ имеют степень $q \}$
		
		\item 
		$\dim S^q(V) = \binom{n + q - 1}{q}$
	\end{enumerate}

	\begin{proof}
		\slashns
		
		\begin{enumerate}
			\item 
			Заметим, что $\{e_{i_1} \ldots e_{i_q}\}$ порождают $S^q(V)$, т.к. являются образами базисов при эпиморфизме $S: T_0^q(V) \to S^q(V)$.
			
			Заметим, что значение $e_{i_1} \ldots e_{i_q}$ не зависит от порядка $e_{i_k}$ ($e_{i_1} \ldots e_{i_q} = e_{\sigma (i_1)} \ldots e_{\sigma (i_q)}$ для любой перестановки $\sigma$). 
			
			Будем записывать в более краткой форме: $e_1^{k_1} e_2^{k_2} \ldots e_n^{k_n}$, где $k_1 + \ldots k_n = q$ и $k_i \ge 0$.
			
			Покажем, что они линейно независимы.
			
			$\sum\limits_{k_1 + \ldots + k_n = q} a_{k_1, \ldots, k_n} e_1^{k_1} \ldots e_n^{k_n} = 0$.
			
			$S(\sum a_{k_1,\ldots,k_n} e_1^{\otimes k_1} \otimes \ldots \otimes e_n^{\otimes k_n}) = 0$
			
			$\frac{1}{q!} \sum\limits_{\sigma \in S_q} \left(\sum\limits_{k_1 + \ldots + k_n = q} a_{k_1,\ldots,k_n} f_{\sigma}(e_1^{\otimes k_1} \otimes \ldots \otimes e_n^{\otimes k_n})\right) = 0$
			
			$\frac{1}{q!} \sum\limits_{k_1 + \ldots + k_n = q} a_{k_1,\ldots,k_n} \left(\sum\limits_{\sigma \in S_q} f_{\sigma}(e_1^{\otimes k_1} \otimes \ldots \otimes e_n^{\otimes k_n})\right) = 0$
			
			Но при ненулевых коэффициентах такая сумма не может занулиться, т.к. внутри скобок находятся \textbf{различные} базисные вектора $T^q_0(V)$. А значит, выражение равно нулю только тогда, когда все $a_{k_1, \ldots, k_n}$ равны нулю.
			
			\item 
			$T \in S^q(V)$
			
			$T = \sum a_{k_1\ldots k_n} e_1^{k_1}\ldots e_n^{k_n} \mapsto \sum a_{k_1\ldots k_n} x_1^{k_1}\ldots x_n^{k_n}$	
			
			\item 
			Следует из первого утверждения, совпадает с количеством способов представить $q$ в виде суммы $k_1, \, \ldots, \, k_n$.
		\end{enumerate}
	\end{proof}
\end{statement}

\vspace{-20pt}
\begin{definition}
	\slashns
	
	$S(V) = \bigoplus\limits^\infty_{q = 0} S^q(V)$~--- симметричная алгебра.
	
	Введем умножение.
	
	Пусть $T_1 \in S^q(V)$, $T_2 \in S^p(V)$. Тогда $T_1T_2 = S(T_1 \otimes T_2)$. 
\end{definition}	

\begin{statement}
	\slashns
	
	\begin{enumerate}
		\item 
		$S(V)$~--- коммутативная ассоциативная алгебра.
		
		\item 
		$\left(e_1^{k_1}\ldots e_n^{k_n}\right)\left(e_1^{l_1}\ldots e_n^{l_n}\right) = e_1^{k_1 + l_1}\ldots e_n^{k_n + l_n}$
	\end{enumerate}
\end{statement}	

\begin{proof}
	\slashns
	
	\begin{enumerate}
		\item
		Докажем, что $S(S(T_1)\otimes T_2) = S(T_1 \otimes S(T_2)) = S(T_1 \otimes T_2)$.
		
		$S(T_1) \otimes T_2 = \frac{1}{p!}\sum\limits_{\sigma \in S_p} f_\sigma (T_1) \otimes T_2$
		
		$S\Big(S(T_1) \otimes T_2\Big) = S\left(\frac{1}{p!} \sum\limits_{\sigma \in S_p} f_{\sigma} (T_1)T_2\right) = \frac{1}{p!}\sum\limits_{\sigma \in S_p}S\Big(f_\sigma(T_1) \otimes T_2\Big) = \frac{1}{p!}\sum\limits_{\sigma \in S_p}S(T_1 \otimes T_2) = S(T_1 \otimes T_2)$.
		
		Аналогично~--- для $S(T_1 \otimes S(T_2))$
		
		Осталось показать ассоциативность и коммутативность.
		
		Ассоциативность.
		
		$(T_1T_2)T_3 = S(S(T_1 \otimes T_2)\otimes T_3) = S(T_1 \otimes T_2 \otimes T_3) = S(T_1 \otimes S(T_2 \otimes T_3)) = T_1(T_2T_3)$.
		
		Коммутативность.
		
		Достаточно показать для разложимых тензоров.
		
		В таком случае равенство $S(T_1 \otimes T_2) = S(T_2 \otimes T_1)$ очевидно, а значит по линейности оно выполняется для всех тензоров.
		
		\item 
		$\left(e_1^{k_1}\ldots e_n^{k_n}\right)\left(e_1^{l_1}\ldots e_n^{l_n}\right) = S(S(e_1^{\otimes k_1}\otimes \ldots \otimes e_n^{\otimes k_n}) \otimes S(e_1^{\otimes l_1}\otimes \ldots \otimes e_n^{\otimes l_n})) = e^{k_1 + l_1}\ldots e_n^{k_n + l_n}$
	\end{enumerate}
\end{proof}	

\Subsection{Внешняя алгебра или алгебра Грассмана}

\begin{remark}
	\slashns
	
	В этом параграфе по-прежнему считаем, что $\Char K = 0$.
\end{remark}

\begin{definition}
	\slashns
	
	$\Lambda^q(V) = \{ T \in T_0^q(V) \, | \, f_\sigma(T) = \sign(\sigma) T \;\; \forall \sigma \in S_q \}$~--- пространство антисимметричных тензоров.
\end{definition}	

\begin{definition}
	\slashns
	
	$A = \frac{1}{q!} \sum\limits_{\sigma \in S_q} \sign(\sigma)f_\sigma$~--- оператор альтернирования.
\end{definition}

\begin{statement}\label{statement:grassman-perm}
	\slashns
	
	$T_1 = f_\sigma T_2 \implies A(T_1) = \sign(\sigma) A(T_2)$
	
	\begin{proof}
		\slashns
		
		$A(T_1) = \frac{1}{q!} \sum\limits_{\tau \in S_q} \sign(\tau) f_{\tau}(T_1) = \frac{1}{q!}\sum\limits_{\tau \in S_q} \sign(\tau)f_{\tau\sigma}(T_2) = \sign(\sigma) \cdot \frac{1}{q!}\sum\limits_{\tau \in S_q}sign(\tau\sigma)f_{\tau\sigma}(T_2) = \sign(\sigma)A(T_2)$
	\end{proof}
\end{statement}	

\begin{consequence}
	\slashns
	
	$T = f_\sigma T, \, \sign(\sigma) = -1 \implies AT = -AT \implies AT = 0$
\end{consequence}

\begin{statement}
	\slashns
	
	$A^2 = A$
	
	\begin{proof}
		\slashns
		
		$A^2 = \frac{1}{(q!)^2}\sum\limits_{\sigma \in S_q} \sum\limits_{\tau \in S_q} \sign(\sigma)\sign(\tau) (f_\sigma \circ f_\tau) = \frac{1}{(q!)^2} \sum\limits_{\sigma \in S_q} \sum\limits_{\tau \in S_q} \sign(\sigma \tau)f_{\sigma \tau} = \frac{1}{q!}\sum\limits_{\omega \in S_q} \sign(\omega)f_\omega = A$.
	\end{proof}	
\end{statement}

\begin{statement}
	\slashns
	
	$\Im A = \Lambda^q(V)$
	
	\begin{proof}
		\slashns
		
		Проверим, что $\Lambda^q(V) \in \Im A$
		
		$f_{\sigma} (A(T)) = f_{\sigma} \left(\frac{1}{q!} \sum\limits_{\tau \in S_q} \sign(\tau) f_{\tau}(T)\right) = \sign(\sigma) \left(\frac{1}{q!} \sum\limits_{\tau \in S_q} \sign(\sigma \tau) f_{\sigma \tau}(T)\right) = \sign(\sigma) A(T)$
		
		С другой стороны, проверим, что $\forall T \in \Lambda^q(V): A(T) = T$, т.е. что $T \in \Im A$.
		
		$\forall \sigma \in S_q: f_{\sigma}(T) = \sign(\sigma)T \implies A(T) = \frac{1}{q!} \sum\limits_{\sigma \in S_q} \sign(\sigma)f_\sigma(T) = \frac{1}{q!} \sum\limits_{\sigma \in S_q} \sign^2(\sigma)T = T$
		
		Показали включение в обе стороны, значит, $\Im A = \Lambda^q(V)$. 
	\end{proof}
\end{statement}

\begin{statement}
	\slashns
	
	Пусть $e_1, \ldots, e_n$~--- базис $V$.

	Тогда $e_{i_1} \wedge \ldots \wedge e_{i_q} := A(e_{i_1}\otimes \ldots \otimes e_{i_q})$ порождают $\Lambda^q(V)$.
	
	\begin{proof}
		\slashns
		
		Следует из того, что $e_{i_1} \wedge \ldots \wedge e_{i_q}$~--- образы базисных элементов.
	\end{proof}
\end{statement}	

\begin{statement}
	\slashns
	
	$e_{i_1} \wedge \ldots \wedge e_{i_q} = 0$, если $i_l = i_s$ для некоторых $i_l = i_s$.
	
	\begin{proof}
		\slashns
		
		Нужно лишь заметить, что $f_{\sigma}(e_{i_1} \wedge \ldots \wedge e_{i_q}) = \sign(\sigma) (e_{i_1} \wedge \ldots \wedge e_{i_q})$. А значит, при транспозиции значение не изменяется только тогда, когда $e_{i_1} \wedge \ldots \wedge e_{i_q} = 0$.
	\end{proof}
\end{statement}

\begin{statement}
	\slashns
	
	\vspace{-10pt}
	\begin{enumerate}
		\item 
		$q \le n \implies e_{i_1} \wedge \ldots \wedge e_{i_q}$~--- базис $\Lambda^q(V)$, $1 \le i_1 < \ldots < i_q \le n$
		
		\item
		$q > n \implies \Lambda^q(V) = 0$
		
		\item 
		$\dim \Lambda^q(V) = \binom{n}{q}$
	\end{enumerate}

	\begin{proof}
		\slashns
		
		\begin{enumerate}
			\item
			Заметим, что если $i_l > i_s$ при $l < s$, то можем поменять эти два элемента в тензоре местами.
			
			Уже знаем, что система образующих.
			
			Покажем, что тензоры $e_{i_1} \wedge \ldots \wedge e_{i_q}$~--- линейно независимы.
			
			Пусть $\sum\limits_{i_1 < \ldots < i_q} a_{i_1, \ldots, i_q} e_{i_1} \wedge \ldots \wedge e_{i_q} = 0$.
			
			Но тогда $A(a_{i_1, \ldots, i_q} e_{i_1} \otimes \ldots \otimes e_{i_q}) = 0$, а это возможно только если $a_{i_1, \ldots, i_q} = 0$ (аналогично утверждению про симметричные тензоры).
			
			\item
			Следует из того, что $e_{i_1} \wedge \ldots \wedge e_{i_q} = 0$, если $i_l = i_s$ при некоторых $l, s$.
			
			\item
			Следует из первого пункта, равно количеству способов выбрать $q$ элементов из $n$.
		\end{enumerate}
	\end{proof}	
\end{statement}	

%Потом будет:
%$\dim \bigoplus_{q = 0}^n\bigwedge^q(V) = 2^n$.