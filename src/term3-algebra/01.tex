\Section{Формы над линейными пространствами}{А.Ермилов, А.Киракосян}
\Subsection{Билинейные и полуторалинейные формы}

\begin{definition}
	\slashns
	
	$V$ ~--- векторное пространство над $K$. 
	
	Отображение $\alpha : V \times V \to K$ называется билинейной формой если $\alpha$ удовлетворяет следующим условиям:
	\begin{enumerate}
	\item $\alpha(x_1 + x_2, y) = \alpha(x_1, y) + \alpha(x_2, y)$
	\item $\alpha(\lambda x, y) = \lambda \alpha(x, y)$
	\item $\alpha(x, y_1 + y_2) = \alpha(x, y_1) + \alpha(x, y_2)$
	\item $\alpha(x, \lambda y) = \lambda \alpha(x, y)$
	\end{enumerate}
\end{definition}

\begin{example}
	\slashns
	
	\begin{enumerate}
	\item [1)] $V = \R^n$, $\alpha(x, y) = \angles{x, y}$.
	
	\item [2)] $V = C[a, b]$, $\alpha(f, g) = \int[a][b] f(x)g(x) \, dx$
	
	\item [3)] $V = K^2$, $\alpha(x, y) = \det\big((x,y)\big)$ 
	
	\item [4)] $V = K^{n \times n}$, $\alpha(X, Y) = \tr(X \cdot Y)$
	\end{enumerate}
\end{example}

\begin{denotation}
	\slashns
	
	$K$~--- поле.
	  	
	Рассмотрим автоморфизм $i : K \to K$, такое что, $i^2 = \text{id}$. Тогда над таким полем можно ввести операцию сопряжения, а именно $\overline{\lambda} = i(\lambda)$.
\end{denotation}

\begin{remark}
	\slashns
	
	Сопряжение сохраняет $1$ и $0$ поля $K$.
\end{remark}

\begin{definition}
	\slashns
	
	Отображение $\alpha : V \times V \to K$ называется полуторалинейной формой, если $\alpha$ удовлетворяет следующим свойствам:
	\begin{enumerate}
	\item $\alpha(x_1 + x_2, y) = \alpha(x_1, y) + \alpha(x_2, y)$
	\item $\alpha(\lambda x, y) = \overline{\lambda} \alpha(x, y)$
	\item $\alpha(x, y_1 + y_2) = \alpha(x, y_1) + \alpha(x, y_2)$
	\item $\alpha(x, \lambda y) = \lambda \alpha(x, y)$
	\end{enumerate}
\end{definition}

%\begin{remark}
%	Видимо, полуторалинейная форма определена для пространств над полями, с операцией сопряжения. В частности, в дальнейшем мы почти всегда будем считать, что полем является поле комплексных чисел $(\CC)$.  
%\end{remark}

\begin{definition}
	\slashns

	Пусть $V$~--- конечномерное векторное пространство над полем $K$ с выбранным базисом $e_1, \, e_2 \, \ldots \, e_n$, а $\alpha$~--- билинейная или полуторалинейная форма.	

	Матрицу $A$ имеющую вид:
	
	$$A = \begin{pmatrix}
	\alpha(e_1, e_1) & \alpha(e_1, e_2) & \cdots & \alpha(e_1, e_n)\\
	\alpha(e_2, e_1) & \alpha(e_2, e_2) & \cdots & \alpha(e_2, e_n)\\
	\vdots & \vdots & \ddots & \vdots\\
	\alpha(e_n, e_1) & \alpha(e_n, e_2) & \cdots & \alpha(e_n, e_n)
	\end{pmatrix}$$
	 
	будем называть матрицей формы $\alpha$ в базисе $e$. 
\end{definition}

\begin{statement}
	\slashns
	
	Пусть $V$~--- конечномерное пространство над $K$, $\alpha$ билинейная форма, а $A$~--- ее матрица в базисе $e_1, \, e_2 \, \ldots \, e_n$.
	
	Пусть $x = \sum x_ie_i$, $y = \sum y_ie_i$~--- вектора из пространства $V$. Тогда $\alpha(x, y) = x^TAy$.

	\begin{proof}
		\slashns
		
		$\alpha(x, y) = \alpha(\sum x_ie_i, \sum y_ie_i) = \sum\limits_{i, j} x_iy_j\alpha(e_i, e_j) = x^TAy$
	\end{proof}
\end{statement}

\begin{remark}
	\slashns
	
	Для полуторалинейной формы $\alpha(x, y) = \overline{x}^TAy$.
\end{remark}

\begin{statement}
	\slashns
	
	Пусть $V$~--- конечномерное векторное пространство над $K$, $C$~--- матрица перехода от базиса $e$ к базису $f$.
	
	Тогда, если $A_e$~--- матрица формы $\alpha$ в базисе $e$, а $A_f$~--- матрица формы $\alpha$ в базисе $f$, то $A_f = \overline{C}^TA_eC$.

	\begin{proof}
		\slashns
		
		Пусть $e = (e_1, \, e_2, \, \ldots, \, e_n),\; f = (f_1, \, f_2, \, \ldots, \, f_n)$
	
		Рассмотрим любые $x, \, y \in V$. Т.к. $C$~--- матрица перехода, то $x_e = Cx_f, \, y_e = Cy_f$.
			
		$\alpha(x, y) = \overline{x}_e^TA_ey_e = (\overline{C} \, \overline{x}_f)^T A_e(Cy_f) = \overline{x}_f^T \, \overline{C}^T A_eCy_f$
		
		С другой стороны, если расписывать форму через матрицу в базисе $f$:
		
		$\alpha(x, y) = \overline{x}_f^TA_fy_f = \overline{x}_f^T \, \overline{C}^TA_eCy_f \implies A_f = \overline{C}^TA_eC$
		
		Последний переход верен. т.к. равенство было получено для любых $x, y \in V$.
	\end{proof}	 
\end{statement}

\begin{remark}
	Утверждение для случая, когда $\alpha$ билинейна получается удалением всех операций сопряжения, т.е. $A_f = C^TA_eC$.
\end{remark}

\Subsection{Ядро билинейной формы и ортогональное дополнение}

\begin{definition}
	\slashns

	$V$~--- векторное пространство над полем $K$, а $\alpha$~--- билинейная или полуторалинейная форма. 
	
	Множество $\{y \in V : \alpha(x, y) = 0 \;\; \forall \, x \in V\}$ называется ядром формы $\alpha$ и обозначается $\Ker \alpha$.
	
	Форма $\alpha$ называется невырожденной, если $\Ker \alpha = \{0\}$ %$, \; \alpha(\cdot, y) : V \to K$
\end{definition}

\begin{definition}
	\slashns
	
	$V$~--- векторное пространство над полем $K$, $\Char K \neq 2$. $\alpha$~--- билинейная форма.
	
	$\alpha$ называется симметричной билинейной формой, если $\forall x, y \in V:\;\alpha(x, y) = \alpha(y, x)$
	
	$\alpha$ называется антисимметричной билинейной формой, если $\forall x, y \in V:\; \alpha(x, y) = - \alpha(y, x)$
\end{definition}

\begin{definition}
	\slashns
	
	$\alpha$~--- билинейная (анти-)симметричная форма
	
	Вектора $x, y \in V, \; x, y$ называются ортогональными ($x \perp y$), если $\alpha(x, y) = 0$
\end{definition}

\begin{definition}
	\slashns
	
	$U$~--- подпространство $V$
	
	Множество $U^{\perp} = \{v \in V : v \perp u \;\; \forall u \in U \}$ называется ортогональным дополнением подпространства $U$.
\end{definition}

\begin{properties}
	\slashns
	
	\begin{enumerate}
	\item $U^{\perp}$~--- подпространство $V$
	\item $V^{\perp} = \Ker \alpha$
	\item Если $e_1, \, e_2, \, \ldots, \, e_m$~--- базис $U$, то $v \in U^{\perp} \iff \underset{i = 1 .. m}{v \perp e_i}$
	\end{enumerate}
\end{properties}

\begin{proof}
	\slashns
	
	\begin{enumerate}
		\item
		$0 \in U^\perp$
		
		$u, v \in U^\perp \implies u + \lambda v \in U^{\perp}$, т.к. $\alpha(u + \lambda v, x) = \alpha(u, x) + \lambda\alpha(v, x) = 0$
	
		\item Ровно по определению ядра пространства.
		\item Стрелка вправо очевидна (т.к. если $v \in U^\perp$, то, в частности, $x \perp e_i$).
		
		Стрелка влево. Если $x = \sum\beta_ie_i \in U$, то $\alpha(v, x) = \alpha(v, \sum \beta_ie_i) = \sum \beta_i\alpha(v, e_i) = 0$
	\end{enumerate}
\end{proof}

\begin{remark}
	\slashns
	
	$\Ker \alpha$~--- подпространство $V$ (т.к. $\Ker \alpha = V^\perp$~--- подпространство $V$). 
\end{remark}

\begin{definition}
	\slashns
	
	$\alpha$~--- симметричная билинейная форма.
	
	$\rank \alpha = \rank A$, где $A$~--- матрица формы $\alpha$ в базисе $e_1, \, \ldots, \, e_n$.
\end{definition}

\begin{statement}
	\slashns
	
	$\dim \Ker \alpha = \dim V - \rank A$, где $A$~--- матрица формы $\alpha$ в каком-то базисе.
	\begin{proof}
		\slashns

			Пусть $A$ была записана в базисе $e_1, \ldots, e_n$.
			
			$v \in \Ker \alpha \iff v \in V^{\perp} \iff \underset{i = 1 .. m}{v \perp e_i}$, т.е. $\alpha(e_i, v) = 0$ для всех $i$.
			
			Распишем $\alpha(e_i, v)$.
			
			$0 = \alpha(e_i, v) = \overline{e_i}^TAv = \begin{pmatrix}
			0, \ldots, 1, \ldots, 0
			\end{pmatrix}\begin{pmatrix}
			\alpha(e_1, e_1)  & \cdots & \alpha(e_1, e_n)\\
			\alpha(e_2, e_1)  & \cdots & \alpha(e_2, e_n)\\
			\vdots & \ddots & \vdots\\
			\alpha(e_n, e_1)  & \cdots & \alpha(e_n, e_n)
			\end{pmatrix}v = 
			\begin{pmatrix}
				\alpha(e_i, e_1), \ldots, \alpha(e_i, e_n)
			\end{pmatrix}v$

			Значит, мы получили, что любая строчка матрицы $A$, домноженная на $v$ равна $0$. Это означает, что $Av = 0 \iff v \in \Ker A$.
			
			Т.е. $V^{\perp} = \Ker A$. При этом мы знаем, что $\dim\Ker A = \dim V - \rank A$, т.е. получили требуемое.
	\end{proof}

\end{statement}

\begin{consequence}
	\slashn
	
	\begin{enumerate}
		\item Ранг матрицы формы $\alpha$ не зависит от выбора базиса, в частности, определение $\rank \alpha$ корректно.
		\item $\alpha$~--- невырождена $\iff$ строки $A$ линейно независимы.
	\end{enumerate}

	\begin{proof}
		\slashn
		
		\begin{enumerate}
			\item $\rank A = \dim V - \dim \Ker \alpha$~--- не зависит от базиса.
			\item $\rank A = \dim V - \dim \Ker \alpha = \dim V$, а значит строки $A$ линейно независимы. 
		\end{enumerate}
	\end{proof}
	
\end{consequence}

\begin{remark}
	\slashns
	
	Получаем утверждение $\dim \Ker \alpha = \dim V - \rank \alpha$.
\end{remark}

\begin{lemma}
	\slashns
	
	$V$~--- конечномерное векторное пространство над полем $K$, $U$~--- подпространство $V$, a $\alpha$~--- симметричная или антисимметричная билинейная форма.
	
	Тогда если $\alpha$~--- невырождена, то $\dim U^{\perp} = \dim V - \dim U$ и $U^{\perp \perp} = U$.
	
	\begin{proof}
		\slashns
	
		Пусть $e_1, \, \ldots, \, e_m$~--- базис $U$.
		
		Дополним его до базиса пространства $V$: $e_1, \, \ldots, \, e_m, \, e_{m+1}, \, \ldots, \, e_n$.

		Пусть матрица $A$~--- матрица формы $\alpha$. Введем матрицу $B$ состоящую из первых $m$ строк матрицы $A$. Размер матрицы $B$ будет $m\cross n$. Докажем, что $U^{\perp} = \Ker B$.
		
		$v \in U^{\perp} \iff \forall i \in [1..m]:\; \alpha(e_i, v) = 0$. 
		
		Рассмотрим $\alpha(e_i, v)$.
		
		$0 = \alpha(e_i, v) = \overline{e}_i^TAv = \begin{pmatrix}
		0, \ldots, 1, \ldots, 0
		\end{pmatrix}\begin{pmatrix}
		\alpha(e_1, e_1)  & \cdots & \alpha(e_1, e_n)\\
		\alpha(e_2, e_1)  & \cdots & \alpha(e_2, e_n)\\
		\vdots & \ddots & \vdots\\
		\alpha(e_n, e_1)  & \cdots & \alpha(e_n, e_n)
		\end{pmatrix}v = 
		\begin{pmatrix}
		\alpha(e_i, e_1), \ldots, \alpha(e_i, e_n)
		\end{pmatrix}v$
			
		(Эта строка аналогична строке из доказательства утверждения 1.3)
			
		Таким образом, мы получили, что первые $m$ строк матрицы $A$ после домножения на $v$ дают $0$. Это означает, что $Bv = 0 \iff v \in \Ker B$, что мы и хотели.
		
		Далее, т.к. $U^\perp = \Ker B$, то $\dim U^\perp = \dim \Ker B = n - \rank B$.
		
		Осталось доказать, что $\rank B = m$, т.е. все строки $B$ линейно независимы. Это очевидно следует из того, что все строки $A$ линейно независимы (т.к. $\alpha$ невырожденная).   			

		Теперь докажем, что $U^{\perp \perp} = U$.
				
		$\dim U^{\perp \perp} = n - \dim U^\perp = n - (n - m) = m = \dim U$
		
		Осталось заметить, что $U \subseteq U^{\perp \perp}$.

		Действительно, пусть $x \in U$, тогда нам нужно проверить, что $\forall y \in U^\perp:\;\alpha(x, y) = 0$, т.е., что $x$ ортогонален всем векторам из $U^\perp$. 
		
		$\alpha(x, y) = 0$ т.к. $x \in U,\; y \in U^\perp$.
	\end{proof}
\end{lemma}